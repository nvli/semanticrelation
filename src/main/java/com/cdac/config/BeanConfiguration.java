package com.cdac.config;

import com.cdac.form.validator.DomainFormValidator;
import com.cdac.form.validator.DomainOntologyFormValidator;
import com.cdac.form.validator.DomainOntologyProcessFormValidator;
import java.util.Locale;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * Class act as configuration.
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
@Configuration
public class BeanConfiguration {

    /**
     * method to get DomainFormValidator.
     *
     * @return DomainFormValidator
     */
    @Bean
    public DomainFormValidator getDomainFormValidator() {
        return new DomainFormValidator();
    }

    /**
     * method to get DomainOntologyFormValidator.
     *
     * @return DomainOntologyProcessFormValidator
     */
    @Bean
    public DomainOntologyFormValidator getDomainOntologyFormValidator() {
        return new DomainOntologyFormValidator();
    }

    /**
     * Method to get Message Source
     *
     * @return ReloadableResourceBundleMessageSource
     */
    @Bean(name = "messageSource")
    public MessageSource getMessageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setCacheSeconds(2);
        messageSource.setBasename("/WEB-INF/etc/locale/messages");
        return messageSource;
    }

    /**
     * Method to get Locale Resolver
     *
     * @return SessionLocaleResolver
     */
    @Bean(name = "localeResolver")
    public LocaleResolver getLocalResolver() {
        SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        localeResolver.setDefaultLocale(Locale.ENGLISH);
        return localeResolver;
    }

    /**
     * Method to get MultipartResolver
     *
     * @return CommonsMultipartResolver
     */
    @Bean(name = "multipartResolver")
    public MultipartResolver getMultipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        return multipartResolver;
    }

    /**
     * method to get DomainOntologyProcessFormValidator.
     *
     * @return DomainOntologyProcessFormValidator
     */
    @Bean
    public DomainOntologyProcessFormValidator getDomainOntologyProcessFormValodator() {
        return new DomainOntologyProcessFormValidator();
    }
}
