/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.processor;

import com.cdac.generated.FactoryOntology;
import com.cdac.generated.NvliRecord;
import com.cdac.generated.ResourceType;
import com.cdac.model.RecordOAI;
import com.cdac.utils.HcdcStringUtils;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

/**
 *
 * @author Gajraj
 */
public abstract class AbstractOntProcessor implements OntProcessor {

    private final Logger LOGGER = Logger.getLogger(AbstractOntProcessor.class);

    public static final String NAME_SEPERATOR = "&\\|&";

    public static final double SEMANTIC_MATCH_THRESHOLD = 0.5;
    public static final double PERCENTAGE_MATCH_THRESHOLD = 0.5;
    public static final double PERCENTAGE_MATCH_THRESHOLD_FOR_DESCRIPTION = 0.8;

    protected OWLOntology targetOntology;
    protected OWLOntologyManager manager;
    protected FactoryOntology targetFactoryOntology;
    protected Set<OWLClass> referenceOntClasses;
    private String resourceType;
    private String resourceCode;

    /**
     *
     * @param targetOntFile
     * @param resourceType
     * @param resourceCode
     * @throws OWLOntologyCreationException
     */
    public AbstractOntProcessor(File targetOntFile, String resourceType, String resourceCode) throws OWLOntologyCreationException {
        manager = OWLManager.createOWLOntologyManager();
        targetOntology = manager.loadOntologyFromOntologyDocument(targetOntFile);
        targetFactoryOntology = new FactoryOntology(targetOntology);
        this.resourceType = resourceType;
        this.resourceCode = resourceCode;
    }

    /**
     *
     * @param resourceTypeRef
     * @param recordOAI
     */
    @Override
    public void addRecordStaticInformation(Optional<ResourceType> resourceTypeRef, RecordOAI recordOAI) {
        NvliRecord nvliRecord = targetFactoryOntology.createNvliRecord(recordOAI.getIdentifier().get(0));
        nvliRecord.addIsInstanceOf(resourceTypeRef.get());
        nvliRecord.addHasRecordName(recordOAI.getRecordName());
        recordOAI.getCreater().forEach(creater -> {
            nvliRecord.addHasAuthor(targetFactoryOntology.createAuthor(HcdcStringUtils.getSpaceSeparetedToUnderScore(creater.trim()).trim()));
        });
        recordOAI.getPublisher().forEach(publisher -> {
            nvliRecord.addHasPublisher(targetFactoryOntology.createPublisher(HcdcStringUtils.getSpaceSeparetedToUnderScore(publisher.trim()).trim()));
        });
    }

    /**
     *
     * @return
     */
    @Override
    public boolean saveOntology() {
        try {
            manager.saveOntology(targetOntology);
        } catch (OWLOntologyStorageException ex) {
            LOGGER.error("Error in saving ontology file : " + ex.getLocalizedMessage());
            return false;
        }
        return true;
    }

    /**
     * invoke the method according to the resourceType . either we have to pass
     * the same type as defined in the NVLI Ontology file or we can create a
     * mapping file which map the nvli resouceType to ontology bubble
     *
     * @return
     */
    public Optional<ResourceType> getResourceTypeRefFor() {
        try {
            Method method = targetFactoryOntology.getClass().getMethod("create" + resourceType.trim().replace("\\s+", " ").replace(" ", "_"), String.class);
            Optional<ResourceType> resourceTypeRef = Optional.ofNullable((ResourceType) method.invoke(targetFactoryOntology, resourceCode));
            return resourceTypeRef;
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            LOGGER.error("Either resourceType in incorrect or method doesnt exist");
            return Optional.empty();
        }

    }

    @Override
    abstract public List<RecordOAI> getRecordsFromSource(File csvFile);

    /**
     *
     * @param record
     */
    @Override
    abstract public void processRecords(RecordOAI record);

    /**
     *
     * @param recordStream
     */
    @Override
    public void processRecordsFromStream(Stream<RecordOAI> recordStream) {
        recordStream.forEach(record -> processRecords(record));
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

}
