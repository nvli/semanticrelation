/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.processor;

import com.cdac.entity.OntDomainClasses;
import com.cdac.entity.dao.IOntDomainClassesDao;
import com.cdac.generated.NvliRecord;
import com.cdac.model.RecordOAI;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author Gajraj
 */
//@Service
public class LuceneBasedRecordProcessor extends AbstractOntProcessor {

    @Autowired
    private IOntDomainClassesDao ontDomainClassesDao;

    public final static Logger LOGGER = Logger.getLogger(LuceneBasedRecordProcessor.class);
    //  @Autowired
    private final IOntDomainClassesDao domainClassesDao;

    public LuceneBasedRecordProcessor(File targetOntFile, String resourceType, String resourceCode, IOntDomainClassesDao domainClassesDao) throws OWLOntologyCreationException {
        super(targetOntFile, resourceType, resourceCode);
        this.domainClassesDao = domainClassesDao;
    }

    /*
    public void processWithRefOntology(NvliRecord nvliRecord, String[]... data) {
        if (data.length > 0) {
            String[] titles = data[0];
            Set<OntDomainClasses> classForTitles = new HashSet<>();
            for (String title : titles) {
                classForTitles.addAll(domainClassesDao.searchDomainClasses(title));
            }
            classForTitles.forEach(cls -> nvliRecord.addHasDomainClassInTitle(cls.getQualifiedClassName()));
        }

        if (data.length > 1) {
            String[] subjects = data[1];
            Set<OntDomainClasses> classForSubjects = new HashSet<>();
            for (String subject : subjects) {
                classForSubjects.addAll(domainClassesDao.searchDomainClasses(subject));
            }
            classForSubjects.forEach(cls -> nvliRecord.addHasDomainClassInSubject(cls.getQualifiedClassName()));
        }

    }
     */
    /**
     *
     * @param csvData
     * @return
     */
    @Override
    public List<RecordOAI> getRecordsFromSource(File csvData) {
        Function<String, List<String>> string2List = x
                -> {
            if (x.equalsIgnoreCase("null") || x.trim().isEmpty()) {
                return Collections.EMPTY_LIST;
            } else {
                return Arrays.asList(stripNonValidXMLCharacters(x).split(NAME_SEPERATOR));
            }
        };
        List<RecordOAI> listRecords = new ArrayList<>();
        try {
            CSVParser parser = CSVParser.parse(csvData, Charset.defaultCharset(), CSVFormat.DEFAULT);
            for (CSVRecord csvRecord : parser) {
                if (csvRecord.size() == 6) {
                    RecordOAI roai = new RecordOAI();
                    roai.setIdentifier(Arrays.asList(stripNonValidXMLCharacters(csvRecord.get(0)).split(NAME_SEPERATOR)));
                    roai.setTitle(Arrays.asList(stripNonValidXMLCharacters(csvRecord.get(1)).split(NAME_SEPERATOR)));
                    roai.setSubject(Arrays.asList(stripNonValidXMLCharacters(csvRecord.get(2)).split(NAME_SEPERATOR)));
                    roai.setCreater(string2List.apply(csvRecord.get(3)));
                    roai.setPublisher(Arrays.asList(stripNonValidXMLCharacters(csvRecord.get(5)).split(NAME_SEPERATOR)));
                    listRecords.add(roai);
                }
            }
            return listRecords;
        } catch (IOException ex) {
            LOGGER.error("Error in opening the data file " + ex.getLocalizedMessage());
        }
        return listRecords;
    }

    @Override
    public void processRecords(RecordOAI record) {
        NvliRecord nvliRecord = targetFactoryOntology.createNvliRecord(record.getIdentifier().get(0));
        addRecordStaticInformation(getResourceTypeRefFor(), record);
        LOGGER.info("new Record created : " + record.getIdentifier().get(0));
        record.getTitle().forEach(title -> {
            if (!title.trim().isEmpty()) {
                LOGGER.info("Title : " + title);
                List<OntDomainClasses> classesRef = domainClassesDao.searchDomainClasses(title);
                LOGGER.info("Ref class count : " + classesRef.size());
                classesRef.forEach(cls -> {
                    if (cls.getIsUpdatedRecordCount()) {
                        cls.setIsUpdatedRecordCount(Boolean.FALSE);
                        ontDomainClassesDao.saveOrUpdate(cls);
                    }
                    LOGGER.info("For title : " + cls.getQualifiedClassName());
                    nvliRecord.addHasDomainClassInTitle(cls.getQualifiedClassName());
                });
            }
        });
        record.getSubject().forEach(subject -> {
            if (!subject.trim().isEmpty()) {
                LOGGER.info("Subject : " + subject);
                List<OntDomainClasses> subList = domainClassesDao.searchDomainClasses(subject);
                LOGGER.info("Ref class count : " + subList.size());
                subList.forEach(cls -> {
                    if (cls.getIsUpdatedRecordCount()) {
                        cls.setIsUpdatedRecordCount(Boolean.FALSE);
                        ontDomainClassesDao.saveOrUpdate(cls);
                    }
                    LOGGER.info("For subjects : " + cls.getQualifiedClassName());
                    nvliRecord.addHasDomainClassInSubject(cls.getQualifiedClassName());
                });
            }
        });
    }

    public static void main(String[] args) {

    }

    @Override
    public boolean processMetadataWithOWL(File xmlFile) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void processMetadataWithOWL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * This function is used to escape Junk characters from input string.
     *
     * @param in
     * @return
     */
    public static String stripNonValidXMLCharacters(String in) {
        StringBuilder out = new StringBuilder(); // Used to hold the output.
        char current; // Used to reference the current character.

        if (in == null || ("".equals(in))) {
            return ""; // vacancy test.
        }
        for (int i = 0; i < in.length(); i++) {
            current = in.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
            if ((current == 0x9) || (current == 0xA) || (current == 0xD) || ((current >= 0x20) && (current <= 0xD7FF))
                    || ((current >= 0xE000) && (current <= 0xFFFD)) || ((current >= 0x10000) && (current <= 0x10FFFF))) {
                out.append(current);
            }
        }
        return out.toString();
    }

}
