/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.processor;

import au.com.bytecode.opencsv.CSVReader;
import com.cdac.generated.FactoryOntology;
import com.cdac.generated.GovernementOrganisation;
import com.cdac.generated.Open_Repository;
import com.cdac.model.RecordOAI;
import com.cdac.utils.HcdcStringUtils;
import com.cdac.utils.ParseOAIxmlRecord;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

/**
 *
 * @author Gajraj
 */
public class ProcessMetadataInOWL {

    private static Logger logger = Logger.getLogger(ProcessMetadataInOWL.class);
    private static String NAME_SEPERATOR = "&\\|&";
    private static final int WORD_LENGTH_THRESHOLD = 3;

    private static final double SEMANTIC_MATCH_THRESHOLD = 0.5;
    private static final double PERCENTAGE_MATCH_THRESHOLD = 0.5;
    private static final double PERCENTAGE_MATCH_THRESHOLD_FOR_DESCRIPTION = 0.8;

    private OWLOntology referenceOntology;
    private OWLOntology targetOntology;
    private OWLOntologyManager manager;
    private FactoryOntology targetFactoryOntology;
    private Set<OWLClass> referenceOntClasses;

    private File targetOntologyFile;

    public ProcessMetadataInOWL(File targetOntFile, File refOntFile) throws OWLOntologyCreationException {
        manager = OWLManager.createOWLOntologyManager();
        referenceOntology = manager.loadOntologyFromOntologyDocument(refOntFile);
        targetOntology = manager.loadOntologyFromOntologyDocument(targetOntFile);
        targetFactoryOntology = new FactoryOntology(targetOntology);
        referenceOntClasses = referenceOntology.getClassesInSignature();
    }

    public ProcessMetadataInOWL(OWLOntology targetOntology) {
        this.targetOntology = targetOntology;
    }

    public ProcessMetadataInOWL(OWLOntology referenceOntology, OWLOntology targetOntology) {
        this.referenceOntology = referenceOntology;
        this.targetOntology = targetOntology;
    }

    public OWLOntology getReferenceOntology() {
        return referenceOntology;
    }

    public void setReferenceOntology(OWLOntology referenceOntology) {
        this.referenceOntology = referenceOntology;
    }

    public OWLOntology getTargetOntology() {
        return targetOntology;
    }

    public void setTargetOntology(OWLOntology targetOntology) {
        this.targetOntology = targetOntology;
    }

    public OWLOntologyManager getManager() {
        return manager;
    }

    public void setManager(OWLOntologyManager manager) {
        this.manager = manager;
    }

    /**
     * parse the input input oai_dc format xml. generate instance of classes in
     * referenced ontology and make triple for the matched class instances
     *
     * @param oaiXml
     * @param organisationName
     * @throws IOException
     */
    public void processInputDCfile(File oaiXml, String organisationName) throws IOException {
        logger.info("  File : " + oaiXml.getAbsolutePath());
        com.cdac.generated.GovernementOrganisation iitb = targetFactoryOntology.createGovernementOrganisation(organisationName);
        if (iitb == null) {
            logger.info("Organisation doesnt exist");
        }

        RecordOAI oai = ParseOAIxmlRecord.parse(oaiXml);
        if (oai != null) {
            logger.info("Now parsing the oai Record");
            Open_Repository or = null;
//            for (String identifier : oai.getIdentifier()) {
//                article = targetFactoryOntology.createArticle(HcdcStringUtils.toCamelCaseWithUnderScore(identifier));
//                logger.info("New article created : " + article.getOwlIndividual().getIRI());
//                break;
//            }
            if (!oai.getIdentifier().isEmpty()) {
                or = targetFactoryOntology.createOpen_Repository(HcdcStringUtils.toCamelCaseWithUnderScore(oaiXml.getName()));

                //article = targetFactoryOntology.createArticle(HcdcStringUtils.toCamelCaseWithUnderScore(oai.getIdentifier().get(0)));
                logger.info("New open repository created : " + or.getOwlIndividual().getIRI());
            }

            if (or == null) {
                return;
            }
            or.addHasOrganisation(iitb);
            for (String creater : oai.getCreater()) {
                or.addHasAuthor(targetFactoryOntology.createAuthor(HcdcStringUtils.toCamelCaseWithUnderScore(creater).trim()));
            }

            for (String publisher : oai.getPublisher()) {
                or.addHasPublisher(targetFactoryOntology.createPublisher(HcdcStringUtils.toCamelCaseWithUnderScore(publisher).trim()));
            }

            for (OWLClass oWLClass : referenceOntClasses) {
                final String owlClassIRI = oWLClass.getIRI().toString();
                final String localClasname = owlClassIRI.substring(owlClassIRI.indexOf('#') + 1);

                /*
                check  similarity between title and the classes from
                refrenced domain ontology. here we have re
                referenced physics ontology which has apporx 700 classe
                 */
                final String name = HcdcStringUtils.splitCamelCase(localClasname);

                for (String title : oai.getTitle()) {

                    double score = HcdcStringUtils.percentageMatchOfWords(name, title, WORD_LENGTH_THRESHOLD);
                    if (score >= PERCENTAGE_MATCH_THRESHOLD) {
                        logger.info("Owl Class IRI : " + owlClassIRI);
                        logger.info(" Title : " + title);
                        logger.info(" Class Name : " + name);
                        logger.info(" percentage match Score title : " + score);
                        or.addHasDomainClassInTitle(owlClassIRI);
                    }
                }

                logger.debug("************Now subject*********");

                for (String subject : oai.getSubject()) {
                    double score = HcdcStringUtils.percentageMatchOfWords(name, subject, WORD_LENGTH_THRESHOLD);
                    if (score >= PERCENTAGE_MATCH_THRESHOLD) {
                        logger.info("Owl Class IRI : " + owlClassIRI);
                        logger.info(" subject : " + subject);
                        logger.info(" Class Name " + name);
                        logger.info(" percentage match Score subject : " + score);
                        or.addHasDomainClassInSubject(owlClassIRI);
                    }
                }

            }
        }
    }

    /**
     * parse the input input oai_dc format xml. generate instance of classes in
     * referenced ontology and make triple for the matched class instances
     *
     * @param recordDCdata
     * @throws IOException
     */
    public void processRecordsFromCSV(File csvFile) throws IOException, OWLOntologyStorageException {
        CSVReader reader = new CSVReader(new FileReader(csvFile));

        String[] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            System.out.println("No of fields : " + nextLine.length);
            GovernementOrganisation org = targetFactoryOntology.createGovernementOrganisation(nextLine[50]);
            if (org == null) {
                logger.info("Organisation doesnt exist");
            }
//            for (String identifier : oai.getIdentifier()) {
//                article = targetFactoryOntology.createArticle(HcdcStringUtils.toCamelCaseWithUnderScore(identifier));
//                logger.info("New article created : " + article.getOwlIndividual().getIRI());
//                break;
//            }

            Open_Repository or = targetFactoryOntology.createOpen_Repository(nextLine[40]);
            //  Article article = targetFactoryOntology.createArticle(HcdcStringUtils.toCamelCaseWithUnderScore(nextLine[40]));

            //article = targetFactoryOntology.createArticle(HcdcStringUtils.toCamelCaseWithUnderScore(oai.getIdentifier().get(0)));
            logger.info("New open repository record created : " + or.getOwlIndividual().getIRI());

            if (or == null) {
                return;
            }

            or.addHasOrganisation(org);
            String creater1 = nextLine[6];
            if (!creater1.isEmpty() && !creater1.equalsIgnoreCase("null")) {
                String[] createrList = creater1.split(NAME_SEPERATOR);
                for (String creater : createrList) {
                    or.addHasAuthor(targetFactoryOntology.createAuthor(HcdcStringUtils.toCamelCaseWithUnderScore(creater).trim()));
                }
            }
            String pub = nextLine[36];
            if (!pub.isEmpty()) {
                for (String publisher : pub.split(NAME_SEPERATOR)) {
                    if (!publisher.equalsIgnoreCase("null")) {
                        or.addHasPublisher(targetFactoryOntology.createPublisher(HcdcStringUtils.toCamelCaseWithUnderScore(publisher).trim()));
                    }
                }
            }

            for (OWLClass oWLClass : referenceOntClasses) {
                final String owlClassIRI = oWLClass.getIRI().toString();
                final String localClasname = owlClassIRI.substring(owlClassIRI.indexOf('#') + 1);

                /*
                check  similarity between title and the classes from
                refrenced domain ontology. here we have re
                referenced physics ontology which has apporx 700 classe
                 */
                final String name = HcdcStringUtils.splitCamelCase(localClasname).toLowerCase();
                if (nextLine.length > 52) {
                    String tit = nextLine[52];
                    if (!tit.isEmpty() && !tit.equalsIgnoreCase("null")) {
                        for (String title : tit.split(NAME_SEPERATOR)) {
                            double score = HcdcStringUtils.percentageMatchOfWords(name, title, WORD_LENGTH_THRESHOLD);
                            if (score >= PERCENTAGE_MATCH_THRESHOLD) {
                                logger.info("Owl Class IRI : " + owlClassIRI);
                                logger.info(" Title : " + title);
                                logger.info(" percentage match Score title : " + score);
                                // article.addHasPhysicsClasssInTitle(owlClassIRI);
                                or.addHasDomainClassInTitle(owlClassIRI);
                            }
                        }
                    }
                }
                logger.debug("************Now subject*********");
                if (nextLine.length > 51) {
                    String sub = nextLine[51];
                    if (!sub.isEmpty() && !sub.equalsIgnoreCase("null")) {
                        for (String subject : sub.split(NAME_SEPERATOR)) {
                            double score = HcdcStringUtils.percentageMatchOfWords(name, subject, WORD_LENGTH_THRESHOLD);
                            if (score >= PERCENTAGE_MATCH_THRESHOLD) {
                                logger.info("Owl Class IRI : " + owlClassIRI);
                                logger.info(" subject : " + subject);
                                logger.info(" percentage match Score subject : " + score);
                                // article.addHasPhysicsClasssInSubject(owlClassIRI);
                                or.addHasDomainClassInSubject(owlClassIRI);
                            }
                        }
                    }
                }
                /*  logger.debug("************Now Abstract*********");
                String desc = nextLine[12];
                if (!desc.isEmpty() && !desc.equalsIgnoreCase("null")) {
                    for (String description : desc.split(NAME_SEPERATOR)) {
                        //double score = HcdcStringUtils.percentageMatchOfWords(name, description);
                        if (description.toLowerCase().contains(name)) {
                            logger.info("Owl Class IRI : " + owlClassIRI);
                            logger.info(" Description : " + description);
                            logger.info("Exacat march found");
                            //  logger.info(" percentage match Score Description : " + score);
                            //  article.addHasPhysicsClasssInAbstract(owlClassIRI);
                            or.addHasDomainClassInDescription(owlClassIRI);
                        }
                    }
                }
                 */
                // nextLine[] is an array of values from the line
                // System.out.println(nextLine[50] + "\t" + nextLine[40] + "\t" + nextLine[51] + "\t" + nextLine[52]);
            }
            // manager.saveOntology(targetOntology);
        }
        manager.saveOntology(targetOntology);
    }

    /**
     * process all the record in the directory
     *
     * @param dir
     * @param organisationName
     * @throws IOException
     * @throws OWLOntologyStorageException
     */
    public void processRecordsInDirectory(File dir, String organisationName) throws IOException, OWLOntologyStorageException {
        File[] listFiles = dir.listFiles();
        for (File file : listFiles) {
            processInputDCfile(file, organisationName);
        }
        manager.saveOntology(targetOntology);

    }

    public Map<String, Integer> getFrequencyMap() {
        Map<String, Integer> map = new HashMap<>();
        System.out.println("No of classes : " + referenceOntClasses.size());
        for (OWLClass oWLClass : referenceOntClasses) {
            final String owlClassIRI = oWLClass.getIRI().toString();
            final String localClasname = owlClassIRI.substring(owlClassIRI.indexOf('#') + 1);
            final String name = HcdcStringUtils.splitCamelCase(localClasname).toLowerCase();
            String[] words = name.split(" ");
            for (String word : words) {
                if (map.containsKey(word)) {
                    map.put(word, map.get(word) + 1);
                } else {
                    map.put(word, 1);
                }
            }
        }

        map = HcdcStringUtils.sortByValue(map);
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            String key = entry.getKey().toString();
            Integer value = entry.getValue();
            System.out.println(key + "\t" + value);
        }
        return map;
    }

    /**
     * parse the input input oai_dc format xml. generate instance of classes in
     * referenced ontology and make triple for the matched class instances
     *
     * @param recordDCdata
     * @throws IOException
     */
    public void processRecordsFromCSVWithWeight(File csvFile, String OrganisationName, File jsonFile) throws IOException, OWLOntologyStorageException {

        WordWeightProcessor wwp = new WordWeightProcessor(null, 0, 0);
        wwp.loadFromJson(jsonFile);

        CSVReader reader = new CSVReader(new FileReader(csvFile));

        String[] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            System.out.println("No of fields : " + nextLine.length);
            GovernementOrganisation org = targetFactoryOntology.createGovernementOrganisation(OrganisationName);
            if (org == null) {
                logger.info("Organisation doesnt exist");
            }
//            for (String identifier : oai.getIdentifier()) {
//                article = targetFactoryOntology.createArticle(HcdcStringUtils.toCamelCase(identifier));
//                logger.info("New article created : " + article.getOwlIndividual().getIRI());
//                break;
//            }

            Open_Repository or = targetFactoryOntology.createOpen_Repository(nextLine[0]);
            //  Article article = targetFactoryOntology.createArticle(HcdcStringUtils.toCamelCase(nextLine[40]));

            //article = targetFactoryOntology.createArticle(HcdcStringUtils.toCamelCase(oai.getIdentifier().get(0)));
            logger.info("New open repository record created : " + or.getOwlIndividual().getIRI());

            if (or == null) {
                return;
            }

            or.addHasOrganisation(org);
            String creater1 = nextLine[3];
            if (!creater1.isEmpty() && !creater1.equalsIgnoreCase("null")) {
                String[] createrList = creater1.split(NAME_SEPERATOR);
                for (String creater : createrList) {
                    or.addHasAuthor(targetFactoryOntology.createAuthor(HcdcStringUtils.toCamelCase(creater).trim()));
                }
            }
            String pub = nextLine[5];
            if (!pub.isEmpty()) {
                for (String publisher : pub.split(NAME_SEPERATOR)) {
                    if (!publisher.equalsIgnoreCase("null")) {
                        or.addHasPublisher(targetFactoryOntology.createPublisher(HcdcStringUtils.toCamelCase(publisher).trim()));
                    }
                }
            }

            for (OWLClass oWLClass : referenceOntClasses) {
                final String owlClassIRI = oWLClass.getIRI().toString();
                final String localClasname = owlClassIRI.substring(owlClassIRI.indexOf('#') + 1);

                /*
                check  similarity between title and the classes from
                refrenced domain ontology. here we have re
                referenced physics ontology which has apporx 700 classe
                 */
                final String name = HcdcStringUtils.splitCamelCase(localClasname).toLowerCase();
                logger.debug("************Now subject*********");
                String tit = nextLine[1];
                if (!tit.isEmpty() && !tit.equalsIgnoreCase("null")) {
                    for (String title : tit.split(NAME_SEPERATOR)) {
                        boolean match = HcdcStringUtils.isMatchStrongWord(name, title, wwp.getDomainWordWeight());
                        if (match) {
                            logger.info("Owl Class IRI : " + owlClassIRI);
                            logger.info(" Title : " + title);
                            // article.addHasPhysicsClasssInTitle(owlClassIRI);
                            or.addHasDomainClassInTitle(owlClassIRI);
                        }
                    }
                }
                logger.debug("************Now subject*********");
                String sub = nextLine[2];
                if (!sub.isEmpty() && !sub.equalsIgnoreCase("null")) {
                    for (String subject : sub.split(NAME_SEPERATOR)) {
                        boolean match = HcdcStringUtils.isMatchStrongWord(name, subject, wwp.getDomainWordWeight());
                        if (match) {
                            logger.info("Owl Class IRI : " + owlClassIRI);
                            logger.info(" subject : " + subject);
                            // article.addHasPhysicsClasssInSubject(owlClassIRI);
                            or.addHasDomainClassInSubject(owlClassIRI);
                        }
                    }
                }
                /*  logger.debug("************Now Abstract*********");
                String desc = nextLine[12];
                if (!desc.isEmpty() && !desc.equalsIgnoreCase("null")) {
                    for (String description : desc.split(NAME_SEPERATOR)) {
                        //double score = HcdcStringUtils.percentageMatchOfWords(name, description);
                        if (description.toLowerCase().contains(name)) {
                            logger.info("Owl Class IRI : " + owlClassIRI);
                            logger.info(" Description : " + description);
                            logger.info("Exacat march found");
                            //  logger.info(" percentage match Score Description : " + score);
                            //  article.addHasPhysicsClasssInAbstract(owlClassIRI);
                            or.addHasDomainClassInDescription(owlClassIRI);
                        }
                    }
                }
                 */
                // nextLine[] is an array of values from the line
                // System.out.println(nextLine[50] + "\t" + nextLine[40] + "\t" + nextLine[51] + "\t" + nextLine[52]);
            }
            // manager.saveOntology(targetOntology);
        }
        manager.saveOntology(targetOntology);
    }
}
