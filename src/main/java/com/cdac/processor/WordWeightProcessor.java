/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.processor;

import com.cdac.model.DomainWordWeight;
import com.cdac.service.OntologyService;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import weka.core.Stopwords;

/**
 *
 * @author Gajraj
 */
public class WordWeightProcessor {

    private final Logger LOOGER = Logger.getLogger(WordWeightProcessor.class);

    private DomainWordWeight domainWordWeight;
    private final OntologyService ontologyService;
    private float thresholdWordLength;
    private int domainOntID;
    // private Map<String, Long> wordWeightMap;

    public WordWeightProcessor(OntologyService ontologyService, int domainOntID, float thresholdWordLength) {
        this.ontologyService = ontologyService;
        this.domainOntID = domainOntID;
        this.thresholdWordLength = thresholdWordLength;

    }

    public DomainWordWeight getDomainWordWeight() {
        return domainWordWeight;
    }

    public void setDomainWordWeight(DomainWordWeight domainWordWeight) {
        this.domainWordWeight = domainWordWeight;
    }

    public float getThreshold() {
        return thresholdWordLength;
    }

    public void setThreshold(int threshold) {
        this.thresholdWordLength = threshold;
    }

    /**
     *
     * @param outputJsonFile
     * @throws IOException
     */
    public void saveWeightedWord(File outputJsonFile) throws IOException, Exception {
        domainWordWeight = buildDomainWordWeightMap();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(outputJsonFile, domainWordWeight);
    }

    /**
     *
     * @return @throws java.lang.Exception
     */
    public DomainWordWeight buildDomainWordWeightMap() throws Exception {
        DomainWordWeight dww = new DomainWordWeight();
        Stopwords sw = new Stopwords();
        Resource resource = new ClassPathResource("stopwords.txt");
        final File stopwordFile = resource.getFile();
        sw.read(stopwordFile);
        Set<String> classes = ontologyService.getClassesForOnt(domainOntID);
        List<String> wordList = new ArrayList<>();
        wordList.addAll(classes);
        String joined = wordList.parallelStream().collect(Collectors.joining(" "));
        String[] words = joined.split(" ");
        Map<String, Long> wordWeightMap = Arrays.asList(words).parallelStream().
                filter((String s) -> {
                    return s.length() >= thresholdWordLength && !sw.is(s);
                }).collect(groupingBy(Function.identity(), counting()));
        dww.setwordWeightMap(wordWeightMap);
        return dww;
    }

    /**
     *
     * @param jsonFile
     * @return
     * @throws IOException
     */
    public static DomainWordWeight loadFromJson(final File jsonFile) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsonFile, DomainWordWeight.class);
    }

    /**
     *
     * @param jsontext
     * @return
     * @throws IOException
     */
    public static DomainWordWeight loadFromJsonText(final String jsontext) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(jsontext, DomainWordWeight.class);
    }

    public static void mainH(String[] args) {
        int l1 = 1;
        int r1 = 3;
        int l2 = 1;
        int r2 = 3;

        int ans = 0;
        for (int i = l1; i <= r1; i++) {
            for (int j = l2; j <= r2; j++) {
                ans = ans ^ (i ^ j);
            }
        }
        System.out.println(ans);
    }

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println(getXORForN(5));
        System.out.println(getXORForN(51));
        System.out.println(getXORForN(256));
        System.out.println(getXORForN(16));
        System.out.println(getXORForN(20));
        System.out.println(getXORForN(18));
        System.out.println("====");
        System.out.println(16 ^ getXORForRange(17, 19));

        System.out.println(getXORForRange(1, 19));

        System.out.println("hemant->" + getXOR2(1, 1, 1, 2));
        System.out.println("gajraj->" + getXOR(1, 1, 1, 2));

        System.out.println("hemant->" + getXOR2(1, 3, 1, 3));
        System.out.println("gajraj->" + getXOR(1, 3, 1, 3));
    }

    public static int getXOR(int l1, int r1, int l2, int r2) {
        int ans = 0;

        for (int j = l2; j <= r2; j++) {
            ans = ans ^ j;
        }
        int jrange = r2 - l2 + 1;
        for (int i = l1; i <= r1; i++) {
            if (jrange % 2 == 1) {

                ans = ans ^ i;
            }
        }
        return ans;
    }

    public static int getXOR2(int l1, int r1, int l2, int r2) {

        int ans = 0;
        for (int i = l1; i <= r1; i++) {
            for (int j = l2; j <= r2; j++) {
                ans = ans ^ (j ^ i);
            }
        }
        return ans;
    }

    public static int getXORForRange(int l, int r) {
        int t = 0;
        for (int j = l; j <= r; j++) {
            t = t ^ j;
        }
        return t;
    }

    public static int getXORForN(int n) {
        int t = 0;
        for (int i = 1; i <= n; i++) {
            t = t ^ i;
        }
        return t;
    }
}
