/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.processor;

import com.cdac.generated.ResourceType;
import com.cdac.model.RecordOAI;
import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 *
 * @author Gajraj
 */
public interface OntProcessor {

    public boolean processMetadataWithOWL(File xmlFile);

    public void processMetadataWithOWL();

    public void addRecordStaticInformation(Optional<ResourceType> resourceTypeRef, RecordOAI recordOAI);

    public boolean saveOntology();

    public List<RecordOAI> getRecordsFromSource(File csvFile);

    public void processRecords(RecordOAI record);

    public void processRecordsFromStream(Stream<RecordOAI> recordStream);
}
