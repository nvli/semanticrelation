/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.processor;

import au.com.bytecode.opencsv.CSVReader;
import com.cdac.entity.OntDomainClasses;
import com.cdac.entity.ResourceProcess;
import com.cdac.generated.NvliRecord;
import com.cdac.generated.ResourceType;
import com.cdac.model.DomainWordWeight;
import com.cdac.model.RecordOAI;
import com.cdac.service.OntologyService;
import com.cdac.utils.HcdcStringUtils;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

/**
 *
 * @author Gajraj
 */
public class OntProcessorForCSV extends AbstractOntProcessor {

    private final Logger LOGGER = Logger.getLogger(OntProcessorForCSV.class);

    private final Map<String, Integer> fieldIndexMap;
    private final String resourceCode;
    private final long refOntId;
    private final OntologyService ontologyService;
    private final String resourceType;

    /**
     *
     * @param targetOntFile
     * @param ontologyService
     * @param fieldIndexMap
     * @param resourceProcess
     * @throws OWLOntologyCreationException
     */
    public OntProcessorForCSV(File targetOntFile, OntologyService ontologyService, Map<String, Integer> fieldIndexMap, ResourceProcess resourceProcess) throws OWLOntologyCreationException {
        super(targetOntFile, null, null);
        this.fieldIndexMap = fieldIndexMap;
        this.resourceCode = resourceProcess.getResourceCode();
        this.refOntId = resourceProcess.getReferenceOnt().getId();
        this.ontologyService = ontologyService;
        this.resourceType = resourceProcess.getResourceType();
    }

    /**
     * Process the records of repsitory of given file in full capacity. If
     * Exception is occured return true else returns false
     *
     * @param csvFile
     * @return
     */
    public boolean processMetadataWithOWL(File csvFile) {
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(csvFile));
            String[] nextLine;
            //TODO change for each resourcetype use reflexdtion or whatever comes in your mind
            ResourceType resource = getResourceType1();
            if (resource == null) {
                LOGGER.info("resource doesnt exist");
                return false;
            }
            while ((nextLine = reader.readNext()) != null) {
                String recordname = null;
                LOGGER.info("No of fields : " + nextLine.length);
                if (nextLine.length < fieldIndexMap.size()) {
                    LOGGER.error("CSV file is not suitable . Please provide the reqjuired no of fields as defined in the field map");
                    LOGGER.error(nextLine + "\n record has fewer columns than required");
                    continue;
                    //return false;
                }

                String recordIdentifier = nextLine[fieldIndexMap.get("identifier")];
                NvliRecord nvliRecord = targetFactoryOntology.createNvliRecord(recordIdentifier);
                nvliRecord.addIsInstanceOf(resource);
                LOGGER.info("New record created : " + nvliRecord.getOwlIndividual().getIRI());
                if (nvliRecord == null) {
                    return false;
                }

                String creater = nextLine[fieldIndexMap.get("creater")];
                if (!creater.isEmpty() && !creater.equalsIgnoreCase("null")) {
                    String[] createrList = creater.split(NAME_SEPERATOR);
                    for (String c : createrList) {
                        nvliRecord.addHasAuthor(targetFactoryOntology.createAuthor(HcdcStringUtils.getSpaceSeparetedToUnderScore(c)));
                    }
                }

                String publisher = nextLine[fieldIndexMap.get("publisher")];
                String[] pubNames = {};
                if (!publisher.isEmpty() && !publisher.equalsIgnoreCase("null")) {
                    pubNames = publisher.split(NAME_SEPERATOR);
                }

                for (String pub : pubNames) {
                    if (!pub.equalsIgnoreCase("null")) {
                        nvliRecord.addHasPublisher(targetFactoryOntology.createPublisher(HcdcStringUtils.getSpaceSeparetedToUnderScore(publisher)));
                    }
                }

                /**
                 * TODO user hibernate search
                 */
                String tit = nextLine[fieldIndexMap.get("title")];
                String[] titNames = {};
                if (!tit.isEmpty() && !tit.equalsIgnoreCase("null")) {
                    titNames = tit.split(NAME_SEPERATOR);
                    recordname = titNames[0];
                }

                String sub = nextLine[fieldIndexMap.get("subject")];
                String[] subNames = {};
                if (!sub.isEmpty() && !sub.equalsIgnoreCase("null")) {
                    subNames = sub.split(NAME_SEPERATOR);
                    if (recordname == null) {
                        recordname = subNames[0];
                    }
                }
//                String publisher = nextLine[fieldIndexMap.get("publisher")];
//                String[] pubNames = {};
                if (!publisher.isEmpty() && !publisher.equalsIgnoreCase("null")) {
                    pubNames = publisher.split(NAME_SEPERATOR);
                }
                if (recordname == null && pubNames.length > 0) {
                    recordname = pubNames[0];
                }
                if (recordname == null) {
                    recordname = recordIdentifier;
                }
                nvliRecord.addHasRecordName(recordname);
                processWithRefOnt(nvliRecord, titNames, subNames);
            }
            /**
             * save ontology file
             */
            try {
                manager.saveOntology(super.targetOntology);
            } catch (OWLOntologyStorageException ex) {
                LOGGER.error("Error in storing the owl file", ex);
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("CSV File not found", ex);
            return false;
        }

        return true;
    }

    /**
     * Find the ref ontology class for title and subject fields. Add the new
     * information in the nvlirecord object.
     *
     * @param nvliRecord
     * @param titNames
     * @param subNames
     */
    public void processWithRefOnt(NvliRecord nvliRecord, String[] titNames, String[] subNames) {
        List<OntDomainClasses> classSet = ontologyService.getReferenceOntClasses(refOntId);
        for (OntDomainClasses oWLClass : classSet) {
            final String localClasname = oWLClass.getClassname();
            /**
             * check similarity between title and the classes from referenced
             * domain ontology.
             */
            String name = HcdcStringUtils.splitCamelCase(localClasname).toLowerCase().replaceAll("\\s", " ");
            BiConsumer<String[], String> biConsumer = (fieldValueList, fieldName) -> {
                for (String fieldValue : fieldValueList) {
                    double score = HcdcStringUtils.percentageMatchOfWords(name, fieldValue, 3);
                    if (score >= PERCENTAGE_MATCH_THRESHOLD) {
                        LOGGER.info("Class : " + name);
                        LOGGER.info(fieldName + ": " + fieldValue);
                        LOGGER.info("Score : " + score);
                        try {
                            Method method = NvliRecord.class.getMethod("addHasDomainClassIn" + fieldName, Object.class);
                            method.invoke(nvliRecord, oWLClass.getQualifiedClassName());
                        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
                            LOGGER.error("Either resourceType in incorrect or method doesnt exist" + ex.getLocalizedMessage());
                        }
                    }
                }
            };
            biConsumer.accept(titNames, "Title");
            biConsumer.accept(subNames, "Subject");
        }
    }

    public void processMetadataWithOWL() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * invoke the method according to the resourceType . either we have to pass
     * the same type as defined in the NVLI Ontology file or we can create a
     * mapping file which map the nvli resouceType to ontology bubble
     *
     * @return
     */
    public ResourceType getResourceType1() {
        ResourceType resourceTypeRef = null;
        try {
            //  resourceTypeRef = targetFactoryOntology.createOpen_Repository(resourceCode);
            Method method = targetFactoryOntology.getClass().getMethod("create" + resourceType.trim().replace("\\s+", " ").replace(" ", "_"), String.class);
            resourceTypeRef = (ResourceType) method.invoke(targetFactoryOntology, resourceCode);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException ex) {
            LOGGER.error("Either resourceType in incorrect or method doesnt exist");
        }
        return resourceTypeRef;
    }

    /**
     *
     * @param csvFile
     * @param domainWordWeight
     * @return
     */
    public boolean processMetadataWithOWL(File csvFile, DomainWordWeight domainWordWeight) {
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(csvFile));
            String[] nextLine;
            //TODO change for each resourcetype use reflexdtion or whatever comes in your mind
            ResourceType resource = getResourceType1();
            if (resource == null) {
                LOGGER.info("resource doesnt exist");
                return false;
            }
            while ((nextLine = reader.readNext()) != null) {
                String recordname = null;
                LOGGER.info("No of fields : " + nextLine.length);
                if (nextLine.length < fieldIndexMap.size()) {
                    LOGGER.error("CSV file is not suitable . Please provide the reqjuired no of fields as defined in the field map");
                    LOGGER.error(nextLine + "\n record has fewer columns than required");
                    continue;
                    //return false;
                }

                String recordIdentifier = nextLine[fieldIndexMap.get("identifier")];
                NvliRecord nvliRecord = targetFactoryOntology.createNvliRecord(recordIdentifier);
                nvliRecord.addIsInstanceOf(resource);
                LOGGER.info("New record created : " + nvliRecord.getOwlIndividual().getIRI());
                if (nvliRecord == null) {
                    return false;
                }
                String creater = nextLine[fieldIndexMap.get("creater")];
                if (!creater.isEmpty() && !creater.equalsIgnoreCase("null")) {
                    String[] createrList = creater.split(NAME_SEPERATOR);
                    for (String c : createrList) {
                        nvliRecord.addHasAuthor(targetFactoryOntology.createAuthor(HcdcStringUtils.getSpaceSeparetedToUnderScore(c)));
                    }
                }

                String publisher = nextLine[fieldIndexMap.get("publisher")];
                String[] pubNames = {};
                if (!publisher.isEmpty() && !publisher.equalsIgnoreCase("null")) {
                    pubNames = publisher.split(NAME_SEPERATOR);
                }

                for (String pub : pubNames) {
                    if (!pub.equalsIgnoreCase("null")) {
                        nvliRecord.addHasPublisher(targetFactoryOntology.createPublisher(HcdcStringUtils.getSpaceSeparetedToUnderScore(publisher)));
                    }
                }

                List<OntDomainClasses> classSet = ontologyService.getReferenceOntClasses(refOntId);
                /*
                TODO user hibernate search
                 */

                String tit = nextLine[fieldIndexMap.get("title")];
                String[] titNames = {};
                if (!tit.isEmpty() && !tit.equalsIgnoreCase("null")) {
                    titNames = tit.split(NAME_SEPERATOR);
                    recordname = titNames[0];
                }

                String sub = nextLine[fieldIndexMap.get("subject")];
                String[] subNames = {};
                if (!sub.isEmpty() && !sub.equalsIgnoreCase("null")) {
                    subNames = sub.split(NAME_SEPERATOR);
                    if (recordname == null) {
                        recordname = subNames[0];
                    }
                }

                String desc = nextLine[fieldIndexMap.get("description")];
                String[] descNames = {};
                if (!desc.isEmpty() && !desc.equalsIgnoreCase("null")) {
                    descNames = desc.split(NAME_SEPERATOR);
                }

                if (recordname == null && pubNames.length > 0) {
                    recordname = pubNames[0];
                }
                if (recordname == null) {
                    recordname = recordIdentifier;
                }
                nvliRecord.addHasRecordName(recordname);
                for (OntDomainClasses oWLClass : classSet) {
                    final String localClasname = oWLClass.getClassname();
                    /**
                     * check similarity between title and the classes from
                     * referenced domain ontology.
                     */

                    String name = HcdcStringUtils.getSpaceSeparetedToUnderScore(localClasname).toLowerCase();

                    for (String title : titNames) {
                        boolean score = HcdcStringUtils.isMatchStrongWord(name, title, domainWordWeight);
                        if (score) {
                            System.out.println("Owl Class Name : " + name);
                            System.out.println(" Title : " + title);
                            System.out.println(" percentage match Score title : " + score);
                            // article.addHasPhysicsClasssInTitle(owlClassIRI);
                            nvliRecord.addHasDomainClassInTitle(oWLClass.getQualifiedClassName());
                        }
                    }

//                    LOGGER.debug("************Now subject*********");
                    for (String subject : subNames) {
                        boolean score = HcdcStringUtils.isMatchStrongWord(name, subject, domainWordWeight);
                        if (score) {
                            System.out.println("Owl Class Name : " + name);
                            System.out.println(" subject : " + subject);
                            System.out.println(" percentage match Score subject : " + score);
                            nvliRecord.addHasDomainClassInSubject(oWLClass.getQualifiedClassName());
                        }
                    }

                }

            }
            try {
                manager.saveOntology(super.targetOntology);
            } catch (OWLOntologyStorageException ex) {
                LOGGER.error("Error in storing the owl file", ex);
                return false;
            }
        } catch (Exception ex) {
            LOGGER.error("CSV File not found", ex);
            return false;
        }

        return true;
    }

    /**
     * processing metadata except reference onotlogy processing
     *
     * If Exception is occured return true else returns false
     *
     * @param csvFile
     * @return
     */
    public boolean processMetadataExceptRefOntProcessing(File csvFile) {
        CSVReader reader = null;
        try {
            reader = new CSVReader(new FileReader(csvFile));
            String[] nextLine;
            //TODO change for each resourcetype use reflexdtion or whatever comes in your mind
            ResourceType resource = getResourceType1();
            if (resource == null) {
                LOGGER.info("resource doesnt exist");
                return false;
            }
            while ((nextLine = reader.readNext()) != null) {
                String recordname = null;
                LOGGER.info("No of fields : " + nextLine.length);
                if (nextLine.length < fieldIndexMap.size()) {
                    LOGGER.error("CSV file is not suitable . Please provide the reqjuired no of fields as defined in the field map");
                    LOGGER.error(nextLine + "\n record has fewer columns than required");
                    continue;
                    //return false;
                }

                String recordIdentifier = nextLine[fieldIndexMap.get("identifier")];
                NvliRecord nvliRecord = targetFactoryOntology.createNvliRecord(recordIdentifier);
                nvliRecord.addIsInstanceOf(resource);
                LOGGER.info("New record created : " + nvliRecord.getOwlIndividual().getIRI());
                if (null == nvliRecord) {
                    return false;
                }

                String creater = nextLine[fieldIndexMap.get("creater")];
                if (!creater.isEmpty() && !creater.equalsIgnoreCase("null")) {
                    String[] createrList = creater.split(NAME_SEPERATOR);
                    for (String c : createrList) {
                        nvliRecord.addHasAuthor(targetFactoryOntology.createAuthor(HcdcStringUtils.getSpaceSeparetedToUnderScore(c)));
                    }
                }

                String publisher = nextLine[fieldIndexMap.get("publisher")];
                String[] pubNames = {};
                if (!publisher.isEmpty() && !publisher.equalsIgnoreCase("null")) {
                    pubNames = publisher.split(NAME_SEPERATOR);
                    for (String pub : pubNames) {
                        if (!pub.equalsIgnoreCase("null")) {
                            nvliRecord.addHasPublisher(targetFactoryOntology.createPublisher(HcdcStringUtils.getSpaceSeparetedToUnderScore(publisher)));
                        }
                    }
                }

                String tit = nextLine[fieldIndexMap.get("title")];
                String[] titNames = {};
                if (!tit.isEmpty() && !tit.equalsIgnoreCase("null")) {
                    titNames = tit.split(NAME_SEPERATOR);
                    recordname = titNames[0];
                }

                String sub = nextLine[fieldIndexMap.get("subject")];
                String[] subNames = {};
                if (!sub.isEmpty() && !sub.equalsIgnoreCase("null")) {
                    subNames = sub.split(NAME_SEPERATOR);
                    if (recordname == null) {
                        recordname = subNames[0];
                    }
                }

                if (recordname == null && pubNames.length > 0) {
                    recordname = pubNames[0];
                }
                if (recordname == null) {
                    recordname = recordIdentifier;
                }
                nvliRecord.addHasRecordName(recordname);

            }
            try {
                manager.saveOntology(super.targetOntology);
            } catch (OWLOntologyStorageException ex) {
                LOGGER.error("Error in storing the owl file", ex);
                return false;
            }
        } catch (IOException ex) {
            LOGGER.error("CSV File not found", ex);
            return false;
        }

        return true;
    }

    @Override
    public List<RecordOAI> getRecordsFromSource(File csvFile) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void processRecords(RecordOAI record) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
