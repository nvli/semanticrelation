/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.context.listner;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author Gajraj
 */
public class ContextLoaderListener implements ServletContextListener {

    private final Logger LOGGER = Logger.getLogger(ContextLoaderListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
        SessionFactory sessionFactory = (SessionFactory) wac.getBean("sessionFactory");
        buildIndex(sessionFactory);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * build the index
     *
     */
    private void buildIndex(SessionFactory sessionFactory) {
        LOGGER.info("Started building index");
        try {
            Session session = sessionFactory.openSession();
            FullTextSession fts = Search.getFullTextSession(session);
            fts.createIndexer().purgeAllOnStart(true).startAndWait();
            session.close();
        } catch (InterruptedException ex) {
            LOGGER.error(ex.getCause(), ex);
        }
        LOGGER.info("index built successfully");
    }
}
