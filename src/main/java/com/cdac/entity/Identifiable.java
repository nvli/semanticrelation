package com.cdac.entity;

/**
 * Interface for id
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
public interface Identifiable {

    /**
     * give value of id property
     *
     * @return
     */
    public Long getId();

    /**
     * set value of id property
     *
     * @param id
     */
    public void setId(Long id);
}
