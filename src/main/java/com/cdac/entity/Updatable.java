package com.cdac.entity;

import java.util.Date;

/**
 * Interface for updatedOn, updatedBy.
 *
 * @author Hemant Anjana
 * @version 1
 * @since 1
 */
public interface Updatable {

    /**
     * give value of updatedBy property
     *
     * @return
     */
    public Long getUpdatedBy();

    /**
     * set value of updatedBy property
     *
     * @param updatedBy
     */
    public void setUpdatedBy(Long updatedBy);

    /**
     * give value of updatedOn property
     *
     * @return
     */
    public Date getUpdatedOn();

    /**
     * set value of updatedOn property
     *
     * @param updatedOn
     */
    public void setUpdatedOn(Date updatedOn);
}
