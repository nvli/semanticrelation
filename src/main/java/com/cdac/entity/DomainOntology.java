package com.cdac.entity;

import com.cdac.utils.Status;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author Gajraj
 */
@Entity
@Table(name = "domain_ontology")
public class DomainOntology extends BaseEntity implements Serializable {

    @Column(name = "namespace_uri")
    private String namespaceURI;
    @Column
    private String decsription;
    @Lob
    @Column(columnDefinition = "MEDIUMBLOB")
    private byte[] byteStream;
    @Column
    private byte[] checkSum;

    //User to see List of Ontology
    @Column(name = "display_name")
    private String displayName;
    
    @ManyToOne
    @JoinColumn(name = "domain_id")
    private Domain domain;

    @Column(name = "filename")
    private String fileName;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "varchar(255) default 'NEW'")
    private Status processingStatus;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "domainOntology")
    private Set<OntDomainClasses> domainClasses;

    @Column(name = "word_requency_map", columnDefinition = "TEXT")
    private String wordFrequencyMap;

    public DomainOntology() {
    }

    public String getNamespaceURI() {
        return namespaceURI;
    }

    public void setNamespaceURI(String namespaceURI) {
        this.namespaceURI = namespaceURI;
    }

    public String getDecsription() {
        return decsription;
    }

    public void setDecsription(String decsription) {
        this.decsription = decsription;
    }

    public byte[] getByteStream() {
        return byteStream;
    }

    public void setByteStream(byte[] byteStream) {
        this.byteStream = byteStream;
    }

    public byte[] getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(byte[] checkSum) {
        this.checkSum = checkSum;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public Set<OntDomainClasses> getDomainClasses() {
        return domainClasses;
    }

    public void setDomainClasses(Set<OntDomainClasses> domainClasses) {
        this.domainClasses = domainClasses;
    }

    public String getFileName() {
        return fileName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Status getProcessingStatus() {
        return processingStatus;
    }

    public void setProcessingStatus(Status processingStatus) {
        this.processingStatus = processingStatus;
    }

    public String getWordFrequencyMap() {
        return wordFrequencyMap;
    }

    public void setWordFrequencyMap(String wordFrequencyMap) {
        this.wordFrequencyMap = wordFrequencyMap;
    }

}
