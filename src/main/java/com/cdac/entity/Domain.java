package com.cdac.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.search.annotations.Indexed;

/**
 *
 * @author Gajraj
 */
@Entity
@Table(name = "domain")
@Indexed
public class Domain extends BaseEntity implements Serializable {

    @Column(name = "domain_name")
    private String domainName;
    @Column(name = "description")
    private String description;

    @OneToMany(mappedBy = "domain", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<DomainOntology> domainsOntology;

    /**
     * Self reference
     */
    @OneToMany(mappedBy = "broaderDomain", fetch = FetchType.LAZY)
    private Set<Domain> domains;
    /**
     * Self reference
     */
    @ManyToOne
    @JoinColumn(name = "broader_domain_id", referencedColumnName = "id")
    private Domain broaderDomain;

    public Domain() {
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<DomainOntology> getDomainsOntology() {
        return domainsOntology;
    }

    public void setDomainsOntology(Set<DomainOntology> domainsOntology) {
        this.domainsOntology = domainsOntology;
    }

    public Set<Domain> getDomains() {
        return domains;
    }

    public void setDomains(Set<Domain> domains) {
        this.domains = domains;
    }

    public Domain getBroaderDomain() {
        return broaderDomain;
    }

    public void setBroaderDomain(Domain broaderDomain) {
        this.broaderDomain = broaderDomain;
    }

}
