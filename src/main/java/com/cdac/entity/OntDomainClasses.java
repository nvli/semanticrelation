package com.cdac.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.IndexedEmbedded;
import org.hibernate.search.annotations.Store;

/**
 *
 * @author Gajraj
 */
@Entity
@Table(name = "ontology_domain_classes", uniqueConstraints = @UniqueConstraint(columnNames = {"q_claasname", "domain_ontoid"}))
@Indexed
public class OntDomainClasses extends BaseEntity implements Serializable {

    @Field(store = Store.YES)
    @Column(name = "classname")
    private String classname;

    @Field(store = Store.YES)
    @Column(name = "json_class_id")
    private Long jsonClassID;

    @Field(store = Store.YES)
    @Column(name = "q_claasname")
    private String qualifiedClassName;
    @Column
    private int strength;
    @Column
    private String description;

    @IndexedEmbedded
    @ManyToOne
    @JoinColumn(name = "domain_ontoid")
    private DomainOntology domainOntology;

    @OneToMany(mappedBy = "parentClass", fetch = FetchType.LAZY)
    private Set<OntDomainClasses> childClasses;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_class_id", referencedColumnName = "id")
    private OntDomainClasses parentClass;

    @Field(store = Store.YES)
    @Column(name = "record_count", nullable = false, columnDefinition = "int default 0")
    private Long recordCount;

    @Field(store = Store.YES)
    @Column(name = "is_updated_record_count", nullable = false, columnDefinition = "boolean default false")
    private Boolean isUpdatedRecordCount;

    public Boolean getIsUpdatedRecordCount() {
        return isUpdatedRecordCount;
    }

    public void setIsUpdatedRecordCount(Boolean isUpdatedRecordCount) {
        this.isUpdatedRecordCount = isUpdatedRecordCount;
    }

    public Long getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(Long recordCount) {
        this.recordCount = recordCount;
    }

    public OntDomainClasses() {
    }

    public String getClassname() {
        return classname;
    }

    public void setClassname(String classname) {
        this.classname = classname;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DomainOntology getDomainOntology() {
        return domainOntology;
    }

    public void setDomainOntology(DomainOntology domainOntology) {
        this.domainOntology = domainOntology;
    }

    public Set<OntDomainClasses> getChildClasses() {
        return childClasses;
    }

    public void setChildClasses(Set<OntDomainClasses> childClasses) {
        this.childClasses = childClasses;
    }

    public OntDomainClasses getParentClass() {
        return parentClass;
    }

    public void setParentClass(OntDomainClasses parentClass) {
        this.parentClass = parentClass;
    }

    public String getQualifiedClassName() {
        return qualifiedClassName;
    }

    public void setQualifiedClassName(String qualifiedClassName) {
        this.qualifiedClassName = qualifiedClassName;
    }

    public Long getJsonClassID() {
        return jsonClassID;
    }

    public void setJsonClassID(Long jsonClassID) {
        this.jsonClassID = jsonClassID;
    }

    //@Override
    //public String toString() {
    //    return "";
    //return "OntDomainClasses{" + "classname=" + classname + ", qualifiedClassName=" + qualifiedClassName + ", strength=" + strength + ", description=" + description + ", domainOntology=" + domainOntology + ", childClasses=" + childClasses + ", parentClass=" + parentClass + '}';
    //}
}
