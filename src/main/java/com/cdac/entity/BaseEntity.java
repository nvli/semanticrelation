package com.cdac.entity;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Generic Entity for auto increment id creation
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
@MappedSuperclass
@SuppressWarnings("serial")
public class BaseEntity implements Identifiable, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ontSeqGenerator")
    @TableGenerator(name = "ontSeqGenerator", allocationSize = 1, initialValue = 1, table = "ont_sequence")
    private Long id;

    /**
     * give value of id property
     *
     * @return
     */
    @Override
    public Long getId() {
        return id;
    }

    /**
     * set value of id property
     *
     * @param id
     */
    @Override
    public void setId(Long id) {
        this.id = id;
    }
}
