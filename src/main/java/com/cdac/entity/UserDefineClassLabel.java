/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.entity;

import com.cdac.form.UserDefinedCustomLabelDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Gajraj
 */
@Entity
@Table(name = "user_defeind_class_label", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"createdBy", "class_id", "class_label"})})
public class UserDefineClassLabel extends AuditableEntity {

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "class_id", unique = false)
    private OntDomainClasses ontDomainClass;

    @Column(name = "class_label")
    private String classLabel;

    @Column(name = "lang_code")
    private String languageCode;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name = "custom_tag_bookmark", joinColumns = @JoinColumn(name = "custom_annotaoin_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "bookmark_id", referencedColumnName = "id"),
            uniqueConstraints = {
                @UniqueConstraint(columnNames = {"custom_annotaoin_id", "bookmark_id"})})
    private Set<UserCustomBookmarkedRecord> bookmarkedRecords;

    public UserDefineClassLabel() {
    }

    public UserDefineClassLabel(OntDomainClasses ontDomainClass, String classlabel, String languageCode) {
        this.ontDomainClass = ontDomainClass;
        this.classLabel = classlabel;
        this.languageCode = languageCode;
    }

    public OntDomainClasses getOntDomainClass() {
        return ontDomainClass;
    }

    public void setOntDomainClass(OntDomainClasses ontDomainClass) {
        this.ontDomainClass = ontDomainClass;
    }

    public String getClassLabel() {
        return classLabel;
    }

    public void setClassLabel(String classLabel) {
        this.classLabel = classLabel;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Set<UserCustomBookmarkedRecord> getBookmarkedRecords() {
        return bookmarkedRecords;
    }

    public void setBookmarkedRecords(Set<UserCustomBookmarkedRecord> bookmarkedRecords) {
        this.bookmarkedRecords = bookmarkedRecords;
    }

    public UserDefinedCustomLabelDto convertToDto() {
        UserDefinedCustomLabelDto udclDto = new UserDefinedCustomLabelDto();
        udclDto.setCustomLabel(getClassLabel());
        udclDto.setUserId(getCreatedBy());
        udclDto.setCustomLabelId(getId());
        udclDto.setRefOntClassId(getOntDomainClass().getId());
        return udclDto;
    }

}
