/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.entity;

import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author Gajraj
 */
@Entity
@Table(name = "user_custome_bookmarked_record", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"createdBy", "record_id", "custom_class_id"})})
public class UserCustomBookmarkedRecord extends AuditableEntity {

    @Column(name = "record_id")
    private String recordIdentifier;
    @Column(name = "custom_class_id")
    private long customClassId;
    @ManyToMany(mappedBy = "bookmarkedRecords", fetch = FetchType.LAZY)
    Set<UserDefineClassLabel> customeLableByUsers;

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public long getCustomClassId() {
        return customClassId;
    }

    public void setCustomClassId(long customClassId) {
        this.customClassId = customClassId;
    }

}
