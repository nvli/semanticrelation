package com.cdac.entity;

import com.cdac.utils.Helper;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;

/**
 * Creatable entity for createdOn and CreatedBy
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
@MappedSuperclass
@SuppressWarnings("serial")
public class CreatableEntity extends BaseEntity implements Creatable, Serializable {

    /**
     * Who created.
     */
    private Long createdBy;
    /**
     * When Created.
     */
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdOn;

    /**
     * give value of createdBy property
     *
     * @return
     */
    @Override
    public Long getCreatedBy() {
        return createdBy;
    }

    /**
     * give value of createdOn property
     *
     * @return
     */
    @Override
    public Date getCreatedOn() {
        /**
         * Returning a reference to a mutable object value stored in one of the
         * object's fields exposes the internal representation of the object. 
         * If instances are accessed by untrusted code, and unchecked changes to
         * the mutable object would compromise security or other important
         * properties, you will need to do something different Returning a new
         * copy of the object is better approach in many situations.
         */
        return createdOn;
    }

    /**
     * set value of createdBy property
     *
     * @param createdBy
     */
    @Override
    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * set value of createdOn property
     *
     * @param createdOn
     */
    @Override
    public void setCreatedOn(Date createdOn) {
        if (createdOn == null) {
            this.createdOn = Helper.rightNow();
        } else {
            this.createdOn = createdOn;
        }
    }
}
