package com.cdac.entity;

import com.cdac.utils.Status;
import java.io.Serializable;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Suhas
 */
@Entity
@AttributeOverrides({
    @AttributeOverride(name = "id", column
            = @Column(name = "id"))
    ,
    @AttributeOverride(name = "createdBy", column
            = @Column(name = "created_by"))
    ,
    @AttributeOverride(name = "updatedBy", column
            = @Column(name = "updated_by"))
    ,
    @AttributeOverride(name = "updatedOn", column
            = @Column(name = "updated_on"))
    ,
    @AttributeOverride(name = "createdOn", column
            = @Column(name = "created_on"))
})
@Table(name = "resource_process")
public class ResourceProcess extends AuditableEntity implements Serializable {

    @Column(name = "resource_type")
    private String resourceType;

    @Column(name = "resource_code")
    private String resourceCode;

    @Column(name = "resource_name")
    private String resourceName;

    @ManyToOne
    @JoinColumn(name = "reference_file_id")
    private DomainOntology referenceOnt;

    @Column(name = "record_process_count", columnDefinition = "bigint(20) default 0")
    private Long recordProcessCount;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, columnDefinition = "varchar(255) default 'NEW'")
    private Status status;

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public DomainOntology getReferenceOnt() {
        return referenceOnt;
    }

    public void setReferenceOnt(DomainOntology referenceOnt) {
        this.referenceOnt = referenceOnt;
    }

    public Long getRecordProcessCount() {
        return recordProcessCount;
    }

    public void setRecordProcessCount(Long recordProcessCount) {
        this.recordProcessCount = recordProcessCount;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
    }

}
