package com.cdac.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 * Create AuditableEntity for id, updatedBy,updatedOn,createdBy and createdOn;
 *
 * @author Hemant Anjana
 * @version 1
 * @since 1
 */
@MappedSuperclass
@SuppressWarnings("serial")
public class AuditableEntity extends BaseEntity implements Auditable, Serializable {

    /**
     * who created
     */
    private Long createdBy;
    /**
     * When Created
     */
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdOn;
    /**
     * who updated
     */
    private Long updatedBy;
    /**
     * When updated
     */
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date updatedOn;

    /**
     * give value of createdBy property
     *
     * @return
     */
    @Override
    public Long getCreatedBy() {
        return createdBy;
    }

    /**
     * give value of createdOn property
     *
     * @return
     */
    @Override
    public Date getCreatedOn() {
        /**
         * Returning a reference to a mutable object value stored in one of the
         * object's fields exposes the internal representation of the object. 
         * If instances are accessed by untrusted code, and unchecked changes to
         * the mutable object would compromise security or other important
         * properties, you will need to do something different. Returning a new
         * copy of the object is better approach in many situations.
         */
        return createdOn;
    }

    /**
     * set value of createdBy property
     *
     * @param createdBy
     */
    @Override
    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    /**
     * set value of createdOn property
     *
     * @param createdOn
     */
    @Override
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * give value of updatedBy property
     *
     * @return
     */
    @Override
    public Long getUpdatedBy() {
        /**
         * Returning a reference to a mutable object value stored in one of the
         * object's fields exposes the internal representation of the object. 
         * If instances are accessed by untrusted code, and unchecked changes to
         * the mutable object would compromise security or other important
         * properties, you will need to do something different. Returning a new
         * copy of the object is better approach in many situations.
         */
        return updatedBy;
    }

    /**
     * set value of updatedBy property
     *
     * @param updatedBy
     */
    @Override
    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    /**
     * give value of updatedOn property
     *
     * @return
     */
    @Override
    public Date getUpdatedOn() {
        /**
         * Returning a reference to a mutable object value stored in one of the
         * object's fields exposes the internal representation of the object. 
         * If instances are accessed by untrusted code, and unchecked changes to
         * the mutable object would compromise security or other important
         * properties, you will need to do something different. Returning a new
         * copy of the object is better approach in many situations.
         */
        return updatedOn;
    }

    /**
     * set value of updatedOn property
     *
     * @param updatedOn
     */
    @Override
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

}
