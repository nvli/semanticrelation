package com.cdac.entity;

/**
 * Interface for updatedBy,updatedOn,createdBy and createdOn
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
public interface Auditable extends Creatable, Updatable, Identifiable {
}
