package com.cdac.entity;

import java.util.Date;

/**
 * Interface for createdON, createdBy
 *
 * @author Hemant Anjana
 * @since 1
 * @version 1
 */
public interface Creatable {

    /**
     * give value of createdBy property
     *
     * @return
     */
    public Long getCreatedBy();

    /**
     * set value of createdBy property
     *
     * @param createdBy
     */
    public void setCreatedBy(Long createdBy);

    /**
     * give value of createdOn property
     *
     * @return
     */
    public Date getCreatedOn();

    /**
     * set value of createdBy property
     *
     * @param createdBy
     */
    /**
     * set value of createdOn property
     *
     * @param createdOn
     */
    public void setCreatedOn(Date createdOn);
}
