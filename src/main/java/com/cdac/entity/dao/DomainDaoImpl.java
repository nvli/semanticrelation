/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.entity.dao;

import com.cdac.entity.Domain;
import com.cdac.utils.ResultHolder;
import java.util.List;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gajraj
 */
@Repository("domainDao")
public class DomainDaoImpl extends GenericDAOImpl<Domain, Long> implements IDomainDAO {

    /**
     *
     * @param sessionFactory
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    /**
     *
     * @param dname
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Domain getDomainByName(String dname) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Domain.class).add(Restrictions.eq("domainName", dname));
        Domain dom = (Domain) criteria.uniqueResult();
        return dom;
    }

    /**
     *
     * @param parentDomainId
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<Domain> getSubDomains(int parentDomainId) {
        Session session = sessionFactory.getCurrentSession();
        List<Domain> domainList = session.createCriteria(Domain.class)
                .setProjection(Projections.projectionList()
                        .add(Projections.property("id"))
                        .add(Projections.property("domainName"))
                )
                .add(Restrictions.eq("broaderDomain.id", (long) parentDomainId)).list();
        //Domain d = (Domain) session.get(Domain.class, (long)parentDomainId);
        return domainList;
    }

    /**
     *
     * @param dname
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Set<Domain> getSubDomains(String dname) {
        Domain dom = getDomainByName(dname);
        return dom.getDomains();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<String> getAllDomains() {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Domain.class);
        criteria.setProjection(Projections.property("domainName"));
        return criteria.list();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<Domain> getBaseDomains() {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Domain.class);
        criteria.add(Restrictions.isNull("broaderDomain.id"));
        //criteria.setProjection(Projections.projectionList()
        //.add(Projections.property("id"))
        //.add(Projections.property("domainName"))
        //);
        return criteria.list();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public ResultHolder getListDomains(int pageNum, int pageWin) {
        Session session = sessionFactory.getCurrentSession();
        List<Domain> list = session.createCriteria(Domain.class)
                //                .setProjection(Projections.projectionList()
                //                        .add(Projections.property("domainName"))
                //                        .add(Projections.property("domainsOntology")))
                .setFirstResult((pageNum - 1) * pageWin)
                //                .setFetchMode("domains", FetchMode.JOIN)
                //                .setFetchMode("domainsOntology", FetchMode.JOIN)
                .setMaxResults(pageWin).list();

        Long totalRecords = (Long) session.createCriteria(Domain.class)
                .setProjection(org.hibernate.criterion.Projections.count("id"))
                .uniqueResult();

        ResultHolder resultHolder = new ResultHolder(list, totalRecords);
        return resultHolder;
    }

}
