package com.cdac.entity.dao;

import com.cdac.entity.ResourceProcess;
import com.cdac.utils.ResultHolder;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Suhas
 */
@Repository("resourceProcessDAO")
public class ResourceProcessDAOImpl extends GenericDAOImpl<ResourceProcess, Long> implements IResourceProcessDAO {

    /**
     *
     * @param sessionFactory
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public ResultHolder getListOntProcess(int pageNum, int pageWin) {
        Session session = sessionFactory.getCurrentSession();
        List<ResourceProcess> list = session.createCriteria(ResourceProcess.class)
                //                .setProjection(Projections.projectionList()
                //                        .add(Projections.property("domainName"))
                //                        .add(Projections.property("domainsOntology")))
                .setFirstResult((pageNum - 1) * pageWin)
                //                .setFetchMode("domains", FetchMode.JOIN)
                //                .setFetchMode("domainsOntology", FetchMode.JOIN)
                .setMaxResults(pageWin).list();

        Long totalRecords = (Long) session.createCriteria(ResourceProcess.class)
                .setProjection(org.hibernate.criterion.Projections.count("id"))
                .uniqueResult();
        ResultHolder resultHolder = new ResultHolder(list, totalRecords);
        return resultHolder;
    }

}
