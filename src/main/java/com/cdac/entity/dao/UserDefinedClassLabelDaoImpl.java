/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.entity.dao;

import com.cdac.entity.OntDomainClasses;
import com.cdac.entity.UserDefineClassLabel;
import com.cdac.form.UserDefinedCustomLabelDto;
import java.util.List;
import java.util.stream.Collectors;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gajraj
 */
@Repository("UserDefinedClassLabelDao")
public class UserDefinedClassLabelDaoImpl extends GenericDAOImpl<UserDefineClassLabel, Long> implements IUserDefineClassLabelDao {

    @Autowired
    IOntDomainClassesDao domainClassesDao;

    /**
     *
     * @param sessionFactory
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserDefinedCustomLabelDto> listDto() {
        return list().stream().map(x -> x.convertToDto()).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserDefineClassLabel> getCustomLabelForClass(long createdBy, long classId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserDefineClassLabel.class).add(Restrictions.eq("createdBy", createdBy)).add(Restrictions.eq("ontDomainClass.id", classId));
        return criteria.list();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserDefinedCustomLabelDto> getCustomLabelDtoForClass(long createdBy, long classId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserDefineClassLabel.class).add(Restrictions.eq("createdBy", createdBy)).add(Restrictions.eq("ontDomainClass.id", classId));
        List<UserDefineClassLabel> y = criteria.list();
        return y.stream().map(x -> x.convertToDto()).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public List<UserDefineClassLabel> getAllCustomLabelForUser(long createdBy, long domainId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserDefineClassLabel.class)
                .add(Restrictions.eq("createdBy", createdBy))
                .createAlias("ontDomainClass", "od")
                .createAlias("od.domainOntology", "do")
                //.createAlias("do.domain", "d")
                .add(Restrictions.eq("do.id", domainId))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<UserDefineClassLabel> classLabels = (List<UserDefineClassLabel>) criteria.list();

        //to resque from org.hibernate.LazyInitializationException: could not initialize proxy - no Session in Hibernate Exception
        classLabels.forEach(classLabel -> {
            if (classLabel.getOntDomainClass() != null) {
                Hibernate.initialize(classLabel.getOntDomainClass());
            }
        });
        return classLabels;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRED)
    public List<UserDefinedCustomLabelDto> getAllCustomLabelDtoForUser(long createdBy) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserDefineClassLabel.class).add(Restrictions.eq("createdBy", createdBy)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<UserDefineClassLabel> y = criteria.list();
        return y.stream().map(UserDefineClassLabel::convertToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserDefineClassLabel> getAllLabelForClass(long refclassId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserDefineClassLabel.class).add(Restrictions.eq("ontDomainClass.id", refclassId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserDefinedCustomLabelDto> getAllLabelDtoForClass(long refclassId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserDefineClassLabel.class).add(Restrictions.eq("ontDomainClass.id", refclassId)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        List<UserDefineClassLabel> y = criteria.list();
        return y.stream().map(UserDefineClassLabel::convertToDto).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserDefineClassLabel> getAllLabelForClassByUser(long refclassId, long createdBy) {
        Session session = currentSession();
        //  Criteria criteria = session.createCriteria(UserDefineClassLabel.class).add(Restrictions.eq("ontDomainClass.id", refclassId)).add(Restrictions.eq("createdBy", createdBy)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        // Criteria criteria = session.createCriteria(UserDefineClassLabel.class).createAlias("ontDomainClass", "odc");
        // criteria.add(Restrictions.eq("odc.id", refclassId)).add(Restrictions.eq("createdBy", createdBy)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        Criteria criteria = session.createCriteria(UserDefineClassLabel.class)
                .add(Restrictions.eq("createdBy", createdBy))
                .add(Restrictions.eq("ontDomainClass.id", refclassId));
        return criteria.list();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserDefinedCustomLabelDto> getAllLabelDtoForClassByUser(long refclassId, long createdBy) {
        Session session = currentSession();
        //  Criteria criteria = session.createCriteria(UserDefineClassLabel.class).add(Restrictions.eq("ontDomainClass.id", refclassId)).add(Restrictions.eq("createdBy", createdBy)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);

        // Criteria criteria = session.createCriteria(UserDefineClassLabel.class).createAlias("ontDomainClass", "odc");
        // criteria.add(Restrictions.eq("odc.id", refclassId)).add(Restrictions.eq("createdBy", createdBy)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        Criteria criteria = session.createCriteria(UserDefineClassLabel.class)
                .add(Restrictions.eq("createdBy", createdBy))
                .add(Restrictions.eq("ontDomainClass.id", refclassId));
        List<UserDefineClassLabel> y = criteria.list();
        return y.stream().map(UserDefineClassLabel::convertToDto).collect(Collectors.toList());
    }

    /**
     * check if user had already defined the same custom label for this class
     *
     * @return
     */
    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public boolean isExist(UserDefinedCustomLabelDto udclDto) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserDefineClassLabel.class).createAlias("ontDomainClass", "ontDomCls")
                .add(Restrictions.eq("createdBy", udclDto.getUserId()))
                .add(Restrictions.eq("ontDomCls.classname", udclDto.getClassName()))
                .add(Restrictions.eq("ontDomCls.domainOntology.id", udclDto.getOntologyFileId()))
                .add(Restrictions.eq("classLabel", udclDto.getCustomLabel()));
        return criteria.uniqueResult() != null;
    }

    /**
     * check if user had already defined the same custom label for this class
     *
     * @param udclDto
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean AddNewLabel(UserDefinedCustomLabelDto udclDto) {
        //OntDomainClasses ontDomClass = domainClassesDao.getClassBasedOnNameAndID(udclDto.getOntologyFileId(), HcdcStringUtils.splitCamelCaseWithSpaceSeperate(udclDto.getClassName()).replaceAll("\\s{2,}", " ").replaceAll(HcdcStringUtils.NAME_TO_IRI, "").trim());
        OntDomainClasses ontDomClass = domainClassesDao.getClassBasedOnNameAndID(udclDto.getOntologyFileId(), udclDto.getClassName());
        // System.out.println(ontDomClass.toString());
        UserDefineClassLabel udcl = new UserDefineClassLabel();
        udcl.setCreatedBy(udclDto.getUserId());
        udcl.setClassLabel(udclDto.getCustomLabel());
        udcl.setOntDomainClass(ontDomClass);
        return createNew(udcl);
    }

    @Override
    @Transactional(readOnly = true, propagation = Propagation.REQUIRES_NEW)
    public List<UserDefinedCustomLabelDto> getAllLabelDtoForClassByUser(long ontFileId, String className, long createdBy) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserDefineClassLabel.class).createAlias("ontDomainClass", "ontDomCls")
                .add(Restrictions.eq("createdBy", createdBy))
                .add(Restrictions.eq("ontDomCls.jsonClassID", Long.parseLong(className)))
                .add(Restrictions.eq("ontDomCls.domainOntology.id", ontFileId));//domainOntology
        List<UserDefineClassLabel> list = criteria.list();
        if (list == null || list.isEmpty()) {
            criteria = session.createCriteria(UserDefineClassLabel.class).createAlias("ontDomainClass", "ontDomCls")
                    .add(Restrictions.eq("createdBy", createdBy))
                    .add(Restrictions.like("ontDomCls.qualifiedClassName", className, MatchMode.END))
                    .add(Restrictions.eq("ontDomCls.domainOntology.id", ontFileId));//domainOntology
            list = criteria.list();
        }
        return list.stream().map(UserDefineClassLabel::convertToDto).collect(Collectors.toList());

    }

}
