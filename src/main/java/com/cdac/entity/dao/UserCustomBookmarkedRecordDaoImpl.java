/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.entity.dao;

import com.cdac.entity.UserCustomBookmarkedRecord;
import com.cdac.form.UserBookmarkDto;
import java.util.List;
import java.util.stream.Collectors;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository("UserCustomBookmarkedRecordDao")
public class UserCustomBookmarkedRecordDaoImpl extends GenericDAOImpl<UserCustomBookmarkedRecord, Long> implements IUserCustomBookmarkedRecordDao {

    /**
     *
     * @param sessionFactory
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    /**
     *
     * @param createdBy
     * @param customClassId
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserCustomBookmarkedRecord> getRecordForCutomClassIdAndUser(long createdBy, long customClassId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserCustomBookmarkedRecord.class).add(Restrictions.eq("createdBy", createdBy)).add(Restrictions.eq("customClassId", customClassId));
        return criteria.list();
    }

    /**
     *
     * @param createdBy
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserCustomBookmarkedRecord> getAllCustomLabeledRecordForUser(long createdBy) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserCustomBookmarkedRecord.class).add(Restrictions.eq("createdBy", createdBy));
        return criteria.list();
    }

    /**
     *
     * @param recordIdentifier
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserCustomBookmarkedRecord> getCustomeLablesForRecord(String recordIdentifier) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserCustomBookmarkedRecord.class).add(Restrictions.eq("recordIdentifier", recordIdentifier));
        return criteria.list();
        //TODO this is not the correct way. change it later
        // blist.forEach(x->);
        // criteria.createAlias("customeLableByUsers", "customeLableByUsersAlias");
        // criteria.add(Restrictions.eq("recordId", recordId)).add(Restrictions.eq("customeLableByUsersAlias.ontDomainClass.id", "classId"));

        // return null;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserCustomBookmarkedRecord> getBookmarksForRecordByUser(String recordIdentifier, long createdBy) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserCustomBookmarkedRecord.class)
                .add(Restrictions.eq("createdBy", createdBy))
                .add(Restrictions.eq("recordIdentifier", recordIdentifier));
        return criteria.list();

    }

    /**
     * check if user already bookmarked given recordIdenttifer with the given
     * custom label
     *
     * @param recordIdentifier
     * @param createdBy
     * @param customLabelId
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean isExist(String recordIdentifier, long createdBy, long customLabelId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserCustomBookmarkedRecord.class)
                .add(Restrictions.eq("recordIdentifier", recordIdentifier))
                .add(Restrictions.eq("createdBy", createdBy))
                .add(Restrictions.eq("customClassId", customLabelId));
        return criteria.uniqueResult() != null;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserBookmarkDto> DtoList() {
        return list().stream().map(x -> new UserBookmarkDto(x.getCreatedBy(), x.getCustomClassId(), x.getRecordIdentifier())).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserBookmarkDto> getRecordDtoForCutomClassIdAndUser(long createdBy, long customClassId) {
        return getRecordForCutomClassIdAndUser(createdBy, customClassId).stream().map(x -> new UserBookmarkDto(x.getCreatedBy(), x.getCustomClassId(), x.getRecordIdentifier())).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserBookmarkDto> getAllCustomLabeledRecordDtoForUser(long createdBy) {
        return getAllCustomLabeledRecordForUser(createdBy).stream().map(x -> new UserBookmarkDto(x.getCreatedBy(), x.getCustomClassId(), x.getRecordIdentifier())).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserCustomBookmarkedRecord> getCustomeLablesDtoForRecord(String recordIdentifier) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserBookmarkDto> getBookmarksDtoForRecordByUser(String recordIdentifier, long createdBy) {
        return getBookmarksForRecordByUser(recordIdentifier, createdBy).stream().map(x -> new UserBookmarkDto(x.getCreatedBy(), x.getCustomClassId(), x.getRecordIdentifier())).collect(Collectors.toList());
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean addBookmark(UserBookmarkDto ubd) {
        if (!isExist(ubd.getRecordIdentifier(), ubd.getUserId(), ubd.getCustomLabelld())) {
            UserCustomBookmarkedRecord ucbr = new UserCustomBookmarkedRecord();
            ucbr.setCreatedBy(ubd.getUserId());
            ucbr.setCustomClassId(ubd.getCustomLabelld());
            ucbr.setRecordIdentifier(ubd.getRecordIdentifier());
            return createNew(ucbr);
        }
        return false;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<UserCustomBookmarkedRecord> getRecordForCutomClassIdAndUser(long createdBy, List<Long> customClassIds, int pageNo, int limit) {
        Session session = currentSession();

        Criteria criteria = session.createCriteria(UserCustomBookmarkedRecord.class)
                .add(Restrictions.eq("createdBy", createdBy))
                .add(Restrictions.in("customClassId", customClassIds))
                .setFirstResult((pageNo * limit) - limit)
                .setMaxResults(limit);

        return criteria.list();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Long getTaggedRecordCountForCustLbl(long createdBy, List<Long> customClassIds) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(UserCustomBookmarkedRecord.class)
                .setProjection(Projections.distinct(Projections.property("recordIdentifier")))
                .add(Restrictions.eq("createdBy", createdBy))
                .add(Restrictions.in("customClassId", customClassIds));
        return Long.valueOf(criteria.list().size());
    }

}
