/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.entity.dao;

import com.cdac.entity.OntDomainClasses;
import java.util.List;

/**
 *
 * @author Gajraj
 */
public interface IOntDomainClassesDao extends IGenericDAO<OntDomainClasses, Long> {

    public List<OntDomainClasses> getAllClassesForDomOnt(long domOntId);

    public List<OntDomainClasses> getClassesBasedOnStrength(long domOntId, int strength);

    public boolean isExist(long domOntId, String className);

    public OntDomainClasses getClassBasedOnNameAndID(long domOntId, String className);

    public OntDomainClasses getClassBasedOnNameIRIAndID(long domOntId, String className);

    public List<OntDomainClasses> searchDomainClasses(String searchTerm);

    public Long getRecordCountForClass(String className, long domOntId);

    public List<OntDomainClasses> getAllClassesForDomOntWithUpdatedFlagFalse(long domOntId);

    public boolean isExist(String classIRI, long domOntId);

    public OntDomainClasses getClassBasedOnDomainIdAndClassIRI(long domOntId, String classIRI);
    
    public OntDomainClasses getClassBasedOnJsonIdAndDomainOntID(long domOntId, String classJSOnId);
    
}
