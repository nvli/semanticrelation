/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.entity.dao;

import com.cdac.entity.UserDefineClassLabel;
import com.cdac.form.UserDefinedCustomLabelDto;
import java.util.List;

/**
 *
 * @author Gajraj
 */
public interface IUserDefineClassLabelDao extends IGenericDAO<UserDefineClassLabel, Long> {

    public List<UserDefinedCustomLabelDto> listDto();

    public List<UserDefineClassLabel> getCustomLabelForClass(long createdBy, long classId);

    public List<UserDefinedCustomLabelDto> getCustomLabelDtoForClass(long createdBy, long classId);

    public List<UserDefineClassLabel> getAllCustomLabelForUser(long createdBy, long domainId);

    public List<UserDefinedCustomLabelDto> getAllCustomLabelDtoForUser(long createdBy);

    public List<UserDefineClassLabel> getAllLabelForClass(long refclassId);

    public List<UserDefinedCustomLabelDto> getAllLabelDtoForClass(long refclassId);

    public List<UserDefineClassLabel> getAllLabelForClassByUser(long refclassId, long createdBy);

    public List<UserDefinedCustomLabelDto> getAllLabelDtoForClassByUser(long refclassId, long createdBy);

    public List<UserDefinedCustomLabelDto> getAllLabelDtoForClassByUser(long ontFileId, String className, long createdBy);

    public boolean isExist(UserDefinedCustomLabelDto udclDto);

    public boolean AddNewLabel(UserDefinedCustomLabelDto udclDto);


}
