package com.cdac.entity.dao;

import com.cdac.entity.DomainOntology;
import com.cdac.utils.ResultHolder;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gajraj
 */
@Repository("domainOntologyDao")
public class DomainOntologyDaoImpl extends GenericDAOImpl<DomainOntology, Long> implements IDomainOntologyDao {

    /**
     *
     * @param sessionFactory
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<DomainOntology> getDomainOntologiesForDomain(long domainId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(DomainOntology.class).createAlias("domain", "dd");
        criteria.setProjection(
                Projections.projectionList()
                        .add(Projections.property("id"), "id")
                        .add(Projections.property("namespaceURI"), "namespaceURI")
                        .add(Projections.property("fileName"), "fileName")
        )
                .add(Restrictions.eq("dd.id", domainId))
                .setResultTransformer(Transformers.aliasToBean(DomainOntology.class));

        return criteria.list();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<DomainOntology> getDomainOntologiesForDomain(String domainName) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(DomainOntology.class).createAlias("domain", "dd");
        return criteria.add(Restrictions.eq("dd.domainName", domainName)).
                setResultTransformer(criteria.DISTINCT_ROOT_ENTITY).
                list();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean isExistChecksumForOWL(byte[] checksum) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(DomainOntology.class);
        criteria.add(Restrictions.eq("checkSum", checksum));
        if (criteria.uniqueResult() != null) {
            return true;
        }
        return false;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public DomainOntology getDomainOntologiesForChecksum(byte[] checksum) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(DomainOntology.class);
        criteria.add(Restrictions.eq("checkSum", checksum));
        return (DomainOntology) criteria.uniqueResult();
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public ResultHolder getListDomainOntologies(int pageNum, int pageWin) {
        Session session = sessionFactory.getCurrentSession();
        List list = session.createCriteria(DomainOntology.class)
                .setFirstResult((pageNum - 1) * pageWin)
                .setMaxResults(pageWin).list();

        Long totalRecords = (Long) session.createCriteria(DomainOntology.class)
                .setProjection(org.hibernate.criterion.Projections.count("id"))
                .uniqueResult();

        ResultHolder resultHolder = new ResultHolder(list, totalRecords);
        return resultHolder;
    }

    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<DomainOntology> getDomainOntologies() {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(DomainOntology.class).createAlias("domain", "ddd");
        criteria.setProjection(
                Projections.projectionList()
                        .add(Projections.property("id"), "id")
                        .add(Projections.property("namespaceURI"), "namespaceURI")
                        .add(Projections.property("displayName"), "displayName")
                        .add(Projections.property("fileName"), "fileName")
        ).addOrder(Order.asc("displayName"))
                .setResultTransformer(Transformers.aliasToBean(DomainOntology.class));

        return criteria.list();

    }
}
