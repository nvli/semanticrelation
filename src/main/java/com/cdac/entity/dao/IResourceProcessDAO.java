package com.cdac.entity.dao;

import com.cdac.entity.ResourceProcess;
import com.cdac.utils.ResultHolder;

/**
 *
 * @author Suhas
 */
public interface IResourceProcessDAO extends IGenericDAO<ResourceProcess, Long> {

    public ResultHolder getListOntProcess(int pageNum, int pageWin);
}
