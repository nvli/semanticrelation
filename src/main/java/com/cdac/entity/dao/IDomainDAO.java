package com.cdac.entity.dao;

import com.cdac.entity.Domain;
import com.cdac.utils.ResultHolder;
import java.util.List;
import java.util.Set;

/**
 *
 * @author Gajraj
 */
public interface IDomainDAO extends IGenericDAO<Domain, Long> {

    public Domain getDomainByName(String dname);

    public List<Domain> getSubDomains(int parentDomainId);

    public Set<Domain> getSubDomains(String dname);

    public List<String> getAllDomains();

    public List<Domain> getBaseDomains();

    public ResultHolder getListDomains(int pageNum, int pageWin);

}
