/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.entity.dao;

import com.cdac.entity.UserCustomBookmarkedRecord;
import com.cdac.form.UserBookmarkDto;
import java.util.List;

/**
 *
 * @author Gajraj
 */
public interface IUserCustomBookmarkedRecordDao extends IGenericDAO<UserCustomBookmarkedRecord, Long> {

    public List<UserBookmarkDto> DtoList();

    public List<UserCustomBookmarkedRecord> getRecordForCutomClassIdAndUser(long createdBy, long customClassId);

    public List<UserCustomBookmarkedRecord> getRecordForCutomClassIdAndUser(long createdBy, List<Long> customClassIds, int pageNo, int limit);

    public List<UserBookmarkDto> getRecordDtoForCutomClassIdAndUser(long createdBy, long customClassId);

    public List<UserCustomBookmarkedRecord> getAllCustomLabeledRecordForUser(long createdBy);

    public List<UserBookmarkDto> getAllCustomLabeledRecordDtoForUser(long createdBy);

    public List<UserCustomBookmarkedRecord> getCustomeLablesForRecord(String recordIdentifier);

    public List<UserCustomBookmarkedRecord> getCustomeLablesDtoForRecord(String recordIdentifier);

    public List<UserCustomBookmarkedRecord> getBookmarksForRecordByUser(String recordIdentifier, long createdBy);

    public List<UserBookmarkDto> getBookmarksDtoForRecordByUser(String recordIdentifier, long createdBy);

    public boolean isExist(String recordIdentifier, long createdBy, long customLabelId);

    public boolean addBookmark(UserBookmarkDto ubd);

    public Long getTaggedRecordCountForCustLbl(long createdBy, List<Long> customClassIds);
}
