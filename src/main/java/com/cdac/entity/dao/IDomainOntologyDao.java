/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.entity.dao;

import com.cdac.entity.DomainOntology;
import com.cdac.utils.ResultHolder;
import java.util.List;

/**
 *
 * @author Gajraj
 */
public interface IDomainOntologyDao extends IGenericDAO<DomainOntology, Long> {

    public List<DomainOntology> getDomainOntologiesForDomain(long domainId);

    public List<DomainOntology> getDomainOntologiesForDomain(String domainName);

    public boolean isExistChecksumForOWL(byte[] checksum);

    public DomainOntology getDomainOntologiesForChecksum(byte[] checksum);

    public ResultHolder getListDomainOntologies(int pageNum, int pageWin);
    
    public List<DomainOntology> getDomainOntologies();

}
