/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.entity.dao;

import com.cdac.entity.OntDomainClasses;
import java.util.Collections;
import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.search.FullTextQuery;
import org.hibernate.search.FullTextSession;
import org.hibernate.search.Search;
import org.hibernate.search.errors.EmptyQueryException;
import org.hibernate.search.query.dsl.QueryBuilder;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Gajraj
 */
@Repository("ontDomainClassesDao")
public class OntDomainClassesDaoImpl extends GenericDAOImpl<OntDomainClasses, Long> implements IOntDomainClassesDao {
    
    public final static Logger LOGGER = Logger.getLogger(OntDomainClassesDaoImpl.class);

    /**
     *
     * @param sessionFactory
     */
    @Autowired
    @Override
    public void setSessionFactory(SessionFactory sessionFactory) {
        super.setSessionFactory(sessionFactory);
    }

    /**
     *
     * @param domOntId
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<OntDomainClasses> getAllClassesForDomOnt(long domOntId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(OntDomainClasses.class).createAlias("domainOntology", "domOnt");
        List<OntDomainClasses> list = criteria.add(Restrictions.eq("domOnt.id", domOntId)).
                list();
        return list;
    }

    /**
     *
     * @param domOntId
     * @param strength
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<OntDomainClasses> getClassesBasedOnStrength(long domOntId, int strength) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(OntDomainClasses.class).createAlias("domainOntology", "domOnt");
        return criteria.add(Restrictions.eq("domOnt.id", domOntId))
                .add(Restrictions.eq("strength", strength)).
                setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).
                list();
    }

    /**
     *
     * @param domOntId
     * @param className
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean isExist(long domOntId, String className) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(OntDomainClasses.class).createAlias("domainOntology", "domOnt");
        criteria.add(Restrictions.eq("domOnt.id", domOntId))
                .add(Restrictions.eq("classname", className));
        OntDomainClasses odc = (OntDomainClasses) criteria.uniqueResult();
        if (odc != null) {
            return true;
        }
        return false;
    }

    /**
     *
     * @param domOntId
     * @param className
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public OntDomainClasses getClassBasedOnNameAndID(long domOntId, String className) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(OntDomainClasses.class);
        criteria.add(Restrictions.eq("domainOntology.id", domOntId))
                .add(Restrictions.eq("jsonClassID", Long.parseLong(className)));
        OntDomainClasses odc = (OntDomainClasses) criteria.uniqueResult();
        return odc;
    }

    /**
     * get the classes for the search term
     *
     * @param searchTerm
     * @return
     */
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<OntDomainClasses> searchDomainClasses(String searchTerm) {
        Session session = currentSession();
        FullTextSession fts = Search.getFullTextSession(session);
        QueryBuilder b = fts.getSearchFactory()
                .buildQueryBuilder().forEntity(OntDomainClasses.class).get();
        org.apache.lucene.search.Query luceneQuery = null;
        try {
            luceneQuery = b.keyword()
                    .onField("classname")
                    .matching(searchTerm)
                    .createQuery();
        } catch (EmptyQueryException eqe) {
            LOGGER.error("Empty Query " + eqe.getLocalizedMessage());
            return Collections.emptyList();
        }
        FullTextQuery fullTextQuery = fts.createFullTextQuery(luceneQuery).
                setProjection("id", "qualifiedClassName", "recordCount", "isUpdatedRecordCount").setResultTransformer(Transformers.aliasToBean(OntDomainClasses.class));
        return fullTextQuery.list();
    }
    
    @Override
    public OntDomainClasses getClassBasedOnNameIRIAndID(long domOntId, String className) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(OntDomainClasses.class);
        criteria.add(Restrictions.eq("domainOntology.id", domOntId))
                .add(Restrictions.like("qualifiedClassName", className.replaceAll("\\s", ""), MatchMode.END));
        OntDomainClasses odc = (OntDomainClasses) criteria.uniqueResult();
        return odc;
    }
    
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public Long getRecordCountForClass(String className, long domOntId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(OntDomainClasses.class);
        criteria.setProjection(Projections.property("recordCount"));
        criteria.add(Restrictions.eq("jsonClassID", Long.parseLong(className)));
        criteria.add(Restrictions.eq("domainOntology.id", domOntId));
        //criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }
    
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public List<OntDomainClasses> getAllClassesForDomOntWithUpdatedFlagFalse(long domOntId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(OntDomainClasses.class).createAlias("domainOntology", "domOnt");
        List<OntDomainClasses> list = criteria.add(Restrictions.eq("domOnt.id", domOntId))
                .add(Restrictions.eq("isUpdatedRecordCount", Boolean.FALSE))
                .list();
        return list;
    }
    
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public boolean isExist(String classIRI, long domOntId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(OntDomainClasses.class).createAlias("domainOntology", "domOnt");
        criteria.add(Restrictions.eq("domOnt.id", domOntId))
                .add(Restrictions.eq("qualifiedClassName", classIRI));
        OntDomainClasses odc = (OntDomainClasses) criteria.uniqueResult();
        return odc != null;
    }
    
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public OntDomainClasses getClassBasedOnDomainIdAndClassIRI(long domOntId, String classIRI) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(OntDomainClasses.class);
        criteria.add(Restrictions.eq("domainOntology.id", domOntId))
                .add(Restrictions.eq("qualifiedClassName", classIRI));
        OntDomainClasses odc = (OntDomainClasses) criteria.uniqueResult();
        return odc;
    }
    
    @Override
    @Transactional(readOnly = false, propagation = Propagation.REQUIRES_NEW)
    public OntDomainClasses getClassBasedOnJsonIdAndDomainOntID(long domOntId, String classJSOnId) {
        Session session = currentSession();
        Criteria criteria = session.createCriteria(OntDomainClasses.class);
        criteria.add(Restrictions.eq("domainOntology.id", domOntId))
                .add(Restrictions.eq("jsonClassID", Long.parseLong(classJSOnId)));
        OntDomainClasses odc = (OntDomainClasses) criteria.uniqueResult();
        return odc;
    }
    
}
