package com.cdac.form;

import java.io.Serializable;

/**
 *
 * @author Hemant Anjana
 */
public class DomainForm implements Serializable {

    private static final long serialVersionUID = 1L;

    private String domainName;
    private String description;
    private String parentDomain;

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getParentDomain() {
        return parentDomain;
    }

    public void setParentDomain(String parentDomain) {
        this.parentDomain = parentDomain;
    }

    @Override
    public String toString() {
        return "DomainForm{" + "domainName=" + domainName + ", description=" + description + ", parentDomain=" + parentDomain + '}';
    }

}
