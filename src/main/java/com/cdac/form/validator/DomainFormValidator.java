package com.cdac.form.validator;

import com.cdac.form.DomainForm;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 *
 * @author Hemant Anjana
 */
public class DomainFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return DomainForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        DomainForm domainForm = (DomainForm) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "domainName", "domain.label.name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "domain.label.description.required");
        //  ValidationUtils.rejectIfEmptyOrWhitespace(errors, "parentDomain", "domain.label.parentDomain.required");
        if (errors.hasErrors()) {
            return;
        }
    }

}
