/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.form.validator;

import com.cdac.form.DomainOntologyForm;
import com.cdac.form.DomainOntologyProcessForm;
import com.cdac.utils.NvliConstants;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *
 * @author Suhas
 */
public class DomainOntologyProcessFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return DomainOntologyForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        DomainOntologyProcessForm domainOntologyProcessForm = (DomainOntologyProcessForm) target;
        if (domainOntologyProcessForm.getResourceType().equalsIgnoreCase(NvliConstants.SELECT)) {
            errors.rejectValue("resourceType", "process.error.resourcetype.required");
        }
        if (domainOntologyProcessForm.getResourceCode().equalsIgnoreCase(NvliConstants.SELECT)) {
            errors.rejectValue("resourceCode", "process.error.organization.required");
        }
        if (domainOntologyProcessForm.getBaseDomain().equalsIgnoreCase(NvliConstants.SELECT)) {
            errors.rejectValue("baseDomain", "process.error.basedomain.required");
        }
        if (domainOntologyProcessForm.getSubDomain().equalsIgnoreCase(NvliConstants.SELECT)) {
            errors.rejectValue("subDomain", "process.error.subdomain.required");
        }
        if (domainOntologyProcessForm.getOwlFile().equalsIgnoreCase(NvliConstants.SELECT)) {
            errors.rejectValue("owlFile", "process.error.owlfile.required");
        }

    }

}
