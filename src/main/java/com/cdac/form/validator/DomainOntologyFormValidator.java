package com.cdac.form.validator;

import com.cdac.form.DomainOntologyForm;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Hemant Anjana
 */
public class DomainOntologyFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return DomainOntologyForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        DomainOntologyForm domainOntologyForm = (DomainOntologyForm) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "displayName", "domain.ontology.label.displayName.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "domain.ontology.label.description.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "domain", "domain.ontology.label.domain.required");

        if (errors.hasErrors()) {
            return;
        }
        MultipartFile file = domainOntologyForm.getFile();
        //TODO
//        if (file == null || file.isEmpty()) {
//            errors.rejectValue("file", "domain.ontology.label.file.required");
//        }

    }

}
