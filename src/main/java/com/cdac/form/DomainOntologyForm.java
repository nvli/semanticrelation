package com.cdac.form;

import java.io.Serializable;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Hemant Anjana
 */
public class DomainOntologyForm implements Serializable {

    private static final long serialVersionUID = 1L;
    private String description;
    private String displayName;
    private String domain;
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public String getDescription() {
        return description;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public String toString() {
        return "DomainOntologyForm{" + "description=" + description + ", domain=" + domain + ", file=" + file + '}';
    }

}
