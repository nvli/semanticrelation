/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.form;

import java.util.List;

/**
 *
 * @author Gajraj
 */
public class MultipleBookmarkForm {

    long userId;
    List<Long> customLabelId;
    List<String> recordIdentifier;

    public MultipleBookmarkForm() {
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public List<Long> getCustomLabelId() {
        return customLabelId;
    }

    public void setCustomLabelId(List<Long> customLabelId) {
        this.customLabelId = customLabelId;
    }

    public List<String> getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(List<String> recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

}
