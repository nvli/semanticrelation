/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.form;

/**
 *
 * @author Gajraj
 */
public class UserBookmarkDto {

    long bookmarkId;
    long userId;
    long customLabelld;
    long refOntClassId;
    String recordIdentifier;

    public UserBookmarkDto() {
    }

    public UserBookmarkDto(long userId, long customClassId, String recordIdentifier) {
        this.userId = userId;
        this.customLabelld = customClassId;
        this.recordIdentifier = recordIdentifier;
    }

    public long getBookmarkId() {
        return bookmarkId;
    }

    public void setBookmarkId(long bookmarkId) {
        this.bookmarkId = bookmarkId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCustomLabelld() {
        return customLabelld;
    }

    public void setCustomLabelld(long customLabelld) {
        this.customLabelld = customLabelld;
    }

    public long getRefOntClassId() {
        return refOntClassId;
    }

    public void setRefOntClassId(long refOntClassId) {
        this.refOntClassId = refOntClassId;
    }

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    @Override
    public String toString() {
        return "UserBookmarkDto{" + "bookmarkId=" + bookmarkId + ", userId=" + userId + ", customLabelld=" + customLabelld + ", refOntClassId=" + refOntClassId + ", recordIdentifier=" + recordIdentifier + '}';
    }

}
