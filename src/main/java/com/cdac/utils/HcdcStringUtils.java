/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.utils;

import com.cdac.model.DomainWordWeight;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;

/**
 *
 * @author Gajraj
 */
public class HcdcStringUtils {

    private static Logger LOGGER = Logger.getLogger(HcdcStringUtils.class);
    public static final String NAME_TO_IRI = "([^A-Za-z0-9_ ])";

    /**
     * convert a String to acceptable in OWL syntax, String can be in camelCase
     * or space can be replaced by underscore or any other special character
     *
     * @param init
     * @return
     */
    public static String toCamelCaseWithUnderScore(final String init) {
        if (init == null) {
            return null;
        }

        final StringBuilder ret = new StringBuilder(init.length());
        String s = init.replaceAll("([^A-Za-z0-9-])", " ");

        for (final String word : s.split(" ")) {
            if (!word.isEmpty()) {
                ret.append(word.substring(0, 1).toUpperCase());
                ret.append(word.substring(1).toLowerCase());
            }
            if (!(ret.length() == init.length())) {
                ret.append("_");
            }
        }

        return ret.toString();
    }

    /**
     * return the the percentage of words of source string which are part of
     * target
     *
     * @param source
     * @param target
     * @return
     */
    public static double percentageMatchOfWords(final String source, final String target, int thresholdWordLength) {
        if (null == source || source.isEmpty() || null == target || target.isEmpty()) {
            return 0.0;
        }
        //TODO use stopping word set
        String[] sourceWords = source.trim().replaceAll("[^a-zA-Z0-9-]", " ").replaceAll("\\s+", " ").toLowerCase().split(" ");
        List<String> words = Arrays.asList(sourceWords).parallelStream().filter((String s) -> s.length() >= thresholdWordLength).collect(Collectors.toList());
        Set<String> targetWordsSet = new HashSet<>();
        String tempTarget = target.toLowerCase().replaceAll("[^a-zA-Z0-9-]", " ");
        Collections.addAll(targetWordsSet, tempTarget.split(" "));
        double matchCount = 0;
        for (String word : words) {
            if (targetWordsSet.contains(word)) {
                matchCount++;
            }
        }
        return matchCount / words.size();
    }

    /**
     *
     * @param s
     * @return
     */
    public static String splitCamelCase(String s) {
        return s.replaceAll(
                String.format("%s|%s|%s",
                        "(?<=[A-Z0-9])(?=[A-Z0-9][a-z])",
                        "(?<=[^A-Z0-9])(?=[A-Z0-9])",
                        "(?<=[A-Za-z0-9])(?=[^A-Za-z0-9])"
                ),
                " "
        ).replaceAll("_", "")
                .replaceAll("\\s{2,}", " ")
                .replaceAll(" - ", "-")
                .replaceAll(" / ", "/")
                .replaceAll(" . ", ".").trim();// TODO: change regex

    }

    public static String splitCamelCaseWithSpaceSeperate(String s) {
        return s.replaceAll(
                String.format("%s|%s|%s",
                        "(?<=[A-Z])(?=[A-Z][a-z])",
                        "(?<=[^A-Z])(?=[A-Z])",
                        "(?<=[A-Za-z])(?=[^A-Za-z])"
                ),
                " "
        );
    }

    public static <K, V extends Comparable<? super V>> Map<K, V>
            sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list
                = new LinkedList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });

        Map<K, V> result = new LinkedHashMap<K, V>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    public static String toCamelCase(String input) {
        return input.replace(" ", "");
    }

    /**
     *
     * @param src
     * @param target
     * @param dww
     * @return
     */
    public static boolean isMatchStrongWord(final String src, String qword, DomainWordWeight dww) {
        // LOGGER.info(src);
        boolean strong = false;
        Set<String> qwords = createWordSet(qword);
        Set<String> weakWordsSet = new HashSet<>();
        Set<String> swords = createWordSet(src);
        Map<String, Long> wordWeightMap = dww.getwordWeightMap();
        // LOGGER.info(qwords);
        for (String w : qwords) {
            if (swords.contains(w) && wordWeightMap.containsKey(w) && wordWeightMap.get(w) == 1) {
                LOGGER.info("Strong Word Found : " + w);
                strong = true;
                return strong;
            } else if (swords.contains(w) && wordWeightMap.containsKey(w) && wordWeightMap.get(w) == 2) {
                weakWordsSet.add(w);
            }
        }
        if (weakWordsSet.size() >= 2) {
            LOGGER.info("two weak made a strong");
            LOGGER.info(weakWordsSet);
            return true;
        }
        return false;
    }

    /**
     *
     * @param qword
     * @return
     */
    public static Set<String> createWordSet(String qword) {
        qword = qword.replaceAll("[^a-zA-Z]", " ");
        String[] sw = qword.split("\\s+");
        Set<String> swords = new HashSet();
        swords.addAll(Arrays.asList(sw));
        return swords;
    }

    public static String getCamelCaseToSpaceSeperated(String s) {
        String[] words = org.apache.commons.lang3.StringUtils.splitByCharacterTypeCamelCase(s);
        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            sb.append(word);
        }
        return sb.toString();
    }

    public static String getUnderScoreToSpaceSeperated(String s) {
        String[] words = org.apache.commons.lang3.StringUtils.splitByWholeSeparator(s, "_");
        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            sb.append(word).append(" ");
        }
        return sb.toString().trim();
    }

    public static String getSpaceSeparetedToUnderScore(String s) {
        return s.trim().replaceAll("([^A-Za-z0-9_])", " ").trim().replaceAll("\\s+", "_").trim();
    }

    public static void main(String[] args) {
        String s = "A, ,,,,,,B  C        ";
        String s1 = "Zeeman    Splitting";
        String s2 = "work   function";
        String t = "Nambiar , Sreejayan";
        String tt = "Sasidharan, T.O. ";
        System.out.println(getSpaceSeparetedToUnderScore(tt));
        System.out.println(percentageMatchOfWords("Tree diversity, distribution, history and change in urban parks: studies in Bangalore, India", " energy", 3));
        System.out.println(getSpaceSeparetedToUnderScore(t));

        System.out.println(getSpaceSeparetedToUnderScore(s));
        System.out.println(getSpaceSeparetedToUnderScore(s1));
        System.out.println(percentageMatchOfWords(s1, s2, 3));
        System.out.println(percentageMatchOfWords(s2, s1, 3));
    }
}
