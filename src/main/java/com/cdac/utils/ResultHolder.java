/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.utils;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Suhas
 */
@SuppressWarnings("serial")
public class ResultHolder implements Serializable {

    private List results;
    private Long totalResults;

    public ResultHolder(List results, Long totalResults) {
        this.results = results;
        this.totalResults = totalResults;
    }

    public List getResults() {
        return results;
    }

    public void setResults(List results) {
        this.results = results;
    }

    public Long getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(Long totalResults) {
        this.totalResults = totalResults;
    }

}
