package com.cdac.utils;

import java.sql.Timestamp;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.codec.Base64;

/**
 *
 * @author Gajraj
 */
public class Helper {

    /**
     *
     * Gets the current timestamp in SQL format.
     *
     * @return java.sql.Timestamp
     * @since 1
     */
    public static Timestamp rightNow() {
        return new Timestamp(new java.util.Date().getTime());
    }

    public static HttpHeaders getHeader(String userName, String password) {
        String credential = userName + ":" + password;
        byte[] credentialBytes = credential.getBytes();
        byte[] base64CredentialBytes = Base64.encode(credentialBytes);
        String base64Credential = new String(base64CredentialBytes);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + base64Credential);
        httpHeaders.add("User-Agent", "NVLI Internal Web Service");
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);
        return httpHeaders;
    }
}
