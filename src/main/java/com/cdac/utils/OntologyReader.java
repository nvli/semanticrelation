/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyRenameException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.RemoveAxiom;
import org.semanticweb.owlapi.vocab.OWL2Datatype;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;
import weka.core.Stopwords;

/**
 *
 * @author Gajraj
 */
public class OntologyReader {

    private final Logger LOOGER = Logger.getLogger(OntologyReader.class);

    private OWLOntology ontoogy;

    public OntologyReader() {
    }

    public OntologyReader(OWLOntology ontoogy) {
        this.ontoogy = ontoogy;
    }

    public OntologyReader(byte[] bytestream) {

        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        try {
            InputStream myInputStream = new ByteArrayInputStream(bytestream);
            ontoogy = manager.loadOntologyFromOntologyDocument(myInputStream);
        } catch (OWLOntologyCreationException ex) {
            LOOGER.error("Error in reading file", ex);
        }
    }

    public OntologyReader(File owlFile) throws OWLOntologyCreationException {
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        ontoogy = manager.loadOntologyFromOntologyDocument(owlFile);
    }

    /**
     * This will generate the missing rdfs:label annotation on the class which
     * has missing label we change the class name which is in camelCase to
     * normal representation with whitespace as separator. Capitalize the first
     * char of each word
     *
     * If label is already present then capitalize the first character of the
     * each word of the label( only label which has equal to the label created
     * from class name will be considered)
     *
     * @throws OWLOntologyRenameException
     * @throws OWLOntologyStorageException
     */
    public void generatelables() throws OWLOntologyRenameException, OWLOntologyStorageException {
        OWLDataFactory df = OWLManager.getOWLDataFactory();
        OWLAnnotationProperty rdfsLabel = df.getOWLAnnotationProperty(OWLRDFVocabulary.RDFS_LABEL.getIRI());
        Set<OWLClass> cls = ontoogy.getClassesInSignature();
        cls.forEach((cl) -> {
            // System.out.println(cl.getIRI());
            String cname = cl.getIRI().toString();
            cname = cname.substring(cname.contains("#") ? cname.indexOf("#") + 1 : cname.lastIndexOf("/") + 1);
            Set<OWLAnnotation> annoList = cl.getAnnotations(ontoogy, rdfsLabel);
            if (annoList.isEmpty()) {
                //create a new lable for the class
                OWLAnnotation labelAnnotation = df.getOWLAnnotation(df.getRDFSLabel(),
                        df.getOWLLiteral(WordUtils.capitalize(HcdcStringUtils.splitCamelCase(cname)), "en"));
                OWLAxiom ax = df.getOWLAnnotationAssertionAxiom(cl.getIRI(), labelAnnotation);
                ontoogy.getOWLOntologyManager().applyChange(new AddAxiom(ontoogy, ax));
            } else {
                //update the existing labels i.e. capitalize the first character of every word in class name
                for (OWLAnnotation oWLAnnotation : annoList) {
                    String[] cnameWords = org.apache.commons.lang3.StringUtils.splitByCharacterTypeCamelCase(cname.trim());
                    StringBuilder sb = new StringBuilder();
                    StringBuilder sb2 = new StringBuilder();
                    for (String cnameWord : cnameWords) {
                        sb.append(cnameWord.toLowerCase()).append(" ");
                        sb2.append(WordUtils.capitalize(cnameWord)).append(" ");
                    }

                    OWLAnnotation labelAnnotation = df.getOWLAnnotation(df.getRDFSLabel(),
                            df.getOWLLiteral(sb.toString().trim(), OWL2Datatype.XSD_STRING));

                    if (oWLAnnotation.equals(labelAnnotation)) {
                        OWLAxiom removeAx = df.getOWLAnnotationAssertionAxiom(cl.getIRI(), oWLAnnotation);
                        OWLAxiom addAx = df.getOWLAnnotationAssertionAxiom(cl.getIRI(), labelAnnotation);
                        if (removeAx.equals(addAx)) {
                            ontoogy.getOWLOntologyManager().applyChange(new RemoveAxiom(ontoogy, removeAx));
                            labelAnnotation = df.getOWLAnnotation(df.getRDFSLabel(),
                                    df.getOWLLiteral(sb2.toString().trim(), OWL2Datatype.XSD_STRING));
                            addAx = df.getOWLAnnotationAssertionAxiom(cl.getIRI(), labelAnnotation);
                            ontoogy.getOWLOntologyManager().applyChange(new AddAxiom(ontoogy, addAx));
                        }

                    }
                }
            }
        });
        ontoogy.getOWLOntologyManager().saveOntology(ontoogy);
    }

    public OWLOntology getOntoogy() {
        return ontoogy;
    }

    public void setOntoogy(OWLOntology ontoogy) {
        this.ontoogy = ontoogy;
    }

    /**
     *
     * @param stopwords
     * @return
     */
    public Map<String, Integer> getFrequencyMap(Object... stopwords) {
        Stopwords sw = new Stopwords();
        if (stopwords.length > 0) {
            sw = (Stopwords) stopwords[0];
        }
        Set<OWLClass> ontClasses = ontoogy.getClassesInSignature();
        Map<String, Integer> map = new HashMap<>();
        LOOGER.info("The number of classes in OWL file is : " + ontClasses.size());
        for (OWLClass oWLClass : ontClasses) {
            final String owlClassIRI = oWLClass.getIRI().toString();
            final String localClasname = owlClassIRI.substring(owlClassIRI.indexOf('#') + 1);
            final String name = HcdcStringUtils.splitCamelCase(localClasname).toLowerCase();
            LOOGER.debug(name);
            String[] words = name.split("\\s+");
            for (String word : words) {
                if (!sw.is(word)) {
                    if (map.containsKey(word)) {
                        map.put(word, map.get(word) + 1);
                    } else {
                        map.put(word, 1);
                    }
                }
            }
        }
        /*orting the map*/
        map = HcdcStringUtils.sortByValue(map);
        return map;
    }

}
