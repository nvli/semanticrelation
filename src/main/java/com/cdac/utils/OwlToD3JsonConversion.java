package com.cdac.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *  A conversion utility for converting the ontology in owl format to D3 Json Format.
 * @author Gajraj Tanwar
 */
public class OwlToD3JsonConversion {
    private File owlFile;

    /**
     * @param owlFile
     */
    public OwlToD3JsonConversion(File owlFile) {
        this.owlFile = owlFile;
    }

    /**
     *  Node Class
     */
    public static class D3Node {
        private String nodename;
        private String iri;
        private int nodeId;
        Set<D3Node> children;

        public D3Node(String nodename,String iri,int nodeId, Set<D3Node> d3Nodes) {
            this.nodename = nodename;
            this.iri=iri;
            this.nodeId=nodeId;
            children = d3Nodes;
        }

        public String getNodename() {
            return nodename;
        }

        public void setNodename(String nodename) {
            this.nodename = nodename;
        }

        public Set<D3Node> getChildren() {
            return children;
        }

        public void setChildren(Set<D3Node> children) {
            children = children;
        }

        public String getIri() {
            return iri;
        }

        public void setIri(String iri) {
            this.iri = iri;
        }

        public int getNodeId() {
            return nodeId;
        }

        public void setNodeId(int nodeId) {
            this.nodeId = nodeId;
        }

        @Override
        public String toString() {
            return "D3Node{" +
                    "nodename='" + nodename + '\'' +
                    ", children=" + children +
                    '}';
        }
    }



    /**
     * @param d3Node
     * @param owlClass
     * @param owlOntology
     */
    public  void addNode(D3Node d3Node, OWLClass owlClass, OWLOntology owlOntology,int nodeNumber) {
        if (owlClass.getSubClasses(owlOntology).isEmpty()) {
            d3Node.setChildren(new HashSet<>());
            return;
        }
        owlClass.getSubClasses(owlOntology).forEach(expression -> {
            D3Node temp = new D3Node(expression.asOWLClass().getIRI().getFragment(),expression.asOWLClass().getIRI().toString(),nodeNumber,new HashSet<>());
            addNode(temp, expression.asOWLClass(), owlOntology,nodeNumber+1);
            d3Node.getChildren().add(temp);
        });
    }

    /**
     * @param rootNodeName
     * @return
     * @throws IOException
     * @throws OWLOntologyCreationException
     */
    public String convert(String rootNodeName) throws IOException, OWLOntologyCreationException {
        OWLOntologyManager owlOntologyManager = OWLManager.createOWLOntologyManager();
        OWLOntology owlOntology = owlOntologyManager.loadOntologyFromOntologyDocument(owlFile);
        OWLDataFactory df = OWLManager.getOWLDataFactory();

        AtomicInteger nodeNumber=new AtomicInteger(0);
        D3Node d3Node = new D3Node(rootNodeName, "Things",nodeNumber.getAndIncrement(),new HashSet<>());
        String ontologyIRI=owlOntology.getOntologyID().getDefaultDocumentIRI().toString()+"#";
        owlOntology.getClassesInSignature().forEach(owlClass ->
        {
            if (owlClass.getSuperClasses(owlOntology).isEmpty()) {
                d3Node.getChildren().add(new D3Node(owlClass.getIRI().getFragment(),owlClass.getIRI().toString(),nodeNumber.getAndIncrement() ,new HashSet<>()));
            }
        });
        int childrenSize=d3Node.getChildren().size();
                d3Node.getChildren().forEach(d3Node1 ->
                addNode(d3Node1, df.getOWLClass(IRI.create(ontologyIRI+ d3Node1.getNodename())), owlOntology,nodeNumber.getAndIncrement()));

        Gson gson = new GsonBuilder().create();
        return gson.toJson(d3Node);

    }

    /**
     * @param rootNodeName
     * @param outPutFileName
     * @throws IOException
     * @throws OWLOntologyCreationException
     */
    public void convert(String rootNodeName,File outPutFileName) throws IOException, OWLOntologyCreationException {
        FileWriter fileWriter = new FileWriter(outPutFileName);
        fileWriter.write(convert(rootNodeName));
        fileWriter.flush();
        fileWriter.close();
    }
}