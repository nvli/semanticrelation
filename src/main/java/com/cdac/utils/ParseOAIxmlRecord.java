/**
 * 
 Copyright 2015 HCDC, Human Centered Design Computing
 Licensed. you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.cdac.utils;

import com.cdac.model.RecordOAI;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


/**
 * @author Gajraj Tanwar
 *
 */
public class ParseOAIxmlRecord {
	/**
	 * @param xmlF
	 * @return
	 * @throws IOException
	 */
	public static RecordOAI parse(File xmlF) throws IOException {
		RecordOAI rOAI = new RecordOAI();

		Document doc = Jsoup.parse(xmlF, "utf-8");
		return createRecordFromXML(rOAI, doc);
	}

	/**
	 * @param xmlF
	 * @return
	 * @throws IOException
	 */
	public static RecordOAI parse(String xml) throws IOException {
		RecordOAI rOAI = new RecordOAI();

		Document doc = Jsoup.parse(xml);
		return createRecordFromXML(rOAI, doc);
	}

	/**
	 * @param rOAI
	 * @param doc
	 * @return
	 */
	public static RecordOAI createRecordFromXML(RecordOAI rOAI, Document doc) {
		/*
		 * add all titles
		 */
		Elements eles = doc.getElementsByTag("dc:title");
		List<String> titleList = new ArrayList<>();
		for (Element element : eles) {
			titleList.add(element.text());
		}
		rOAI.setTitle(titleList);
		/*
		 * add all creators
		 */
		eles = doc.getElementsByTag("dc:creator");
		List<String> creatorList = new ArrayList<>();
		for (Element element : eles) {
			creatorList.add(element.text());
		}
		rOAI.setCreater(creatorList);
		/*
		 * add all subjects
		 */
		eles = doc.getElementsByTag("dc:subject");
		List<String> subjectList = new ArrayList<>();
		for (Element element : eles) {
			subjectList.add(element.text());
		}
		rOAI.setSource(subjectList);

		/*
		 * add all publisher
		 */
		eles = doc.getElementsByTag("dc:publisher");
		List<String> publisherList = new ArrayList<>();
		for (Element element : eles) {
			publisherList.add(element.text());
		}
		rOAI.setPublisher(publisherList);

		/*
		 * add all type
		 */
		eles = doc.getElementsByTag("dc:type");
		List<String> typeList = new ArrayList<>();
		for (Element element : eles) {
			typeList.add(element.text());
		}
		rOAI.setType(typeList);

		/*
		 * add all identifier
		 */
		eles = doc.getElementsByTag("dc:identifier");
		List<String> identifierList = new ArrayList<>();
		for (Element element : eles) {
			identifierList.add(element.text());
		}
		rOAI.setIdentifier(identifierList);

		/*
		 * add all contributor
		 */
		eles = doc.getElementsByTag("dc:contributor");
		List<String> contributorList = new ArrayList<>();
		for (Element element : eles) {
			contributorList.add(element.text());
		}
		rOAI.setContributor(contributorList);

		/*
		 * add all formats
		 */
		eles = doc.getElementsByTag("dc:format");
		List<String> formatList = new ArrayList<>();
		for (Element element : eles) {
			formatList.add(element.text());
		}
		rOAI.setFormat(formatList);

		/*
		 * add all source
		 */
		eles = doc.getElementsByTag("dc:source");
		List<String> sourceList = new ArrayList<>();
		for (Element element : eles) {
			sourceList.add(element.text());
		}
		rOAI.setSource(sourceList);

		/*
		 * add all language
		 */
		eles = doc.getElementsByTag("dc:language");
		List<String> languageList = new ArrayList<>();
		for (Element element : eles) {
			languageList.add(element.text());
		}
		rOAI.setLanguage(languageList);

		/*
		 * add all relation
		 */
		eles = doc.getElementsByTag("dc:relation");
		List<String> relationList = new ArrayList<>();
		for (Element element : eles) {
			relationList.add(element.text());
		}
		rOAI.setRelation(relationList);

		/*
		 * add all coverage
		 */
		eles = doc.getElementsByTag("dc:coverage");
		List<String> coverageList = new ArrayList<>();
		for (Element element : eles) {
			coverageList.add(element.text());
		}
		rOAI.setCoverage(coverageList);

		/*
		 * add all rights
		 */
		eles = doc.getElementsByTag("dc:rights");
		List<String> rightsList = new ArrayList<>();
		for (Element element : eles) {
			rightsList.add(element.text());
		}
		rOAI.setRights(rightsList);

		/*
		 * add all descriptions
		 */
		eles = doc.getElementsByTag("dc:description");
		List<String> descriptionList = new ArrayList<>();
		for (Element element : eles) {
			descriptionList.add(element.text());
		}
		rOAI.setDesceiption(descriptionList);

		/*
		 * add all dates
		 */
		eles = doc.getElementsByTag("dc:date");
		List<String> dateList = new ArrayList<>();
		for (Element element : eles) {
			dateList.add(element.text());
		}
		rOAI.setDate(dateList);

		return rOAI;
	}

	public static void main(String args[]) throws IOException {
            
            
            List<String> list1,list2;
            list1=list2=new ArrayList<>();
            list1.add("gajraj");
            for (String string : list2) {
                System.out.println(string);
            }
            

		//String xmlR = "D:\\BackUp Folder Gajraj\\NVLI Harvester\\output4.xml";
              //  String xml1="D:\\BackUp Folder Gajraj\\NVLI Harvester\\IITB Data\\oai dspace.library.iitb.ac.in 100 16";
		//System.out.println(parse(new File(xml1)));
	}

}
