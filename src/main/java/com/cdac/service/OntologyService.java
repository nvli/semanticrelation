package com.cdac.service;

import com.cdac.entity.Domain;
import com.cdac.entity.DomainOntology;
import com.cdac.entity.OntDomainClasses;
import com.cdac.entity.dao.IDomainDAO;
import com.cdac.entity.dao.IDomainOntologyDao;
import com.cdac.entity.dao.IOntDomainClassesDao;
import com.cdac.utils.HcdcStringUtils;
import com.cdac.utils.OntologyReader;
import com.cdac.utils.Status;
import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gajraj
 */
@Service
public class OntologyService {

    private static final Logger LOGGER = Logger.getLogger(OntologyService.class);
    @Autowired
    private IDomainDAO domainDAO;
    @Autowired
    private IDomainOntologyDao domainOntologyDao;
    @Autowired
    private IOntDomainClassesDao ontDomainClassesDao;
    @Value("${temp.upload.dir}")
    private String uploadDir;
    @Autowired
    private OwlFileProcessingService owlFileProcessingService;

    public List<DomainOntology> getOWLofSelectedDomain(int subDomainId) {
        return domainOntologyDao.getDomainOntologiesForDomain(subDomainId);
    }

    /**
     *
     * @param domain
     * @return
     */
    public boolean createDomain(Domain domain) {
        return domainDAO.createNew(domain);
    }

    /**
     *
     * @param d
     * @return
     */
    public boolean addDomain(Domain d) {
        return domainDAO.createNew(d);
    }

    public void updateDomain(Domain domain) {
        domainDAO.saveOrUpdate(domain);
    }

    /**
     *
     * @param dname
     * @return
     */
    public Domain getDomainByName(String dname) {
        return domainDAO.getDomainByName(dname);
    }

    /**
     *
     * @return
     */
    public List<Domain> getAllDomains() {
        return domainDAO.list();
    }

    /**
     *
     * @return
     */
    public List<String> getAllDomainsName() {
        return domainDAO.getAllDomains();
    }

    /**
     *
     * @param id
     * @return
     */
    public Domain getDomain(long id) {
        return domainDAO.get(id);
    }

    public DomainOntology getDomainOntology(Long id) {
        return domainOntologyDao.get(id);
    }

    /**
     *
     * @param domainOntology
     * @return
     */
    public boolean createDomainOntology(DomainOntology domainOntology) {
        return domainOntologyDao.createNew(domainOntology);
    }

    public DomainOntology getDomainOntology(long domOntId) {
        return domainOntologyDao.get(domOntId);
    }

    public void updateDomainOntology(DomainOntology domainOntology) {
        domainOntologyDao.saveOrUpdate(domainOntology);
    }

    /**
     *
     * @param ontDomainClasses
     * @return
     */
    public boolean createClassInDomainOntology(OntDomainClasses ontDomainClasses) {
        return ontDomainClassesDao.createNew(ontDomainClasses);
    }

    /**
     *
     * @param OntId
     * @return
     */
    public List<OntDomainClasses> getReferenceOntClasses(long OntId) {
        return ontDomainClassesDao.getAllClassesForDomOnt(OntId);
    }

    public boolean isExistOntologyWithChecksum(byte[] checksum) {
        return domainOntologyDao.isExistChecksumForOWL(checksum);

    }

    /**
     *
     * @param ontID
     * @return
     */
    public Set<String> getClassesForOnt(int ontID) {
        Set<String> classNameSet = new HashSet<>();
        List<OntDomainClasses> list = ontDomainClassesDao.getAllClassesForDomOnt(ontID);
        if (list == null) {
            LOGGER.error("ontology Id Does not exist in the system");
            return classNameSet;
        }
        list.forEach((ontDomainClasses) -> {
            classNameSet.add(ontDomainClasses.getClassname());
        });
        return classNameSet;
    }

    public DomainOntology addOntology(long domainName, byte[] bytestream, String filename, String description, String displayName) throws NoSuchAlgorithmException, IOException {

        Domain d = domainDAO.get(domainName);
        //TODO check if domain is exist get uri form Ontology
        if (d == null) {
            LOGGER.error("Invalid Domain ID in request");
            return null;
        }
        /*calculate checksum*/
        MessageDigest md = MessageDigest.getInstance("MD5");
        LOGGER.info("Size of bytestream : " + bytestream.length);
        byte[] digest = md.digest(bytestream);
        /*
        *this is a new file so adding into the system
        *write file to random named file
         */ /* now add the new file int the system*/
        OntologyReader ontologyReader = new OntologyReader(bytestream);
        OWLOntology ontology = ontologyReader.getOntoogy();

        IRI ontNamespace = ontology.getOntologyID().getDefaultDocumentIRI();
        System.out.println(">>>>>>>>>>ontNamespace>>>>>>>>>>>>>> " + ontNamespace);
        DomainOntology domainOntology = new DomainOntology();
        domainOntology.setDisplayName(displayName);
        domainOntology.setDecsription(description);
        domainOntology.setNamespaceURI(ontNamespace.toString());

        domainOntology.setByteStream(bytestream);
        domainOntology.setCheckSum(digest);
        domainOntology.setDomain(d);
        domainOntology.setProcessingStatus(Status.NEW);
        domainOntologyDao.createNew(domainOntology);
        String newFilename = domainOntology.getId() + "_" + filename;
        domainOntology.setFileName(newFilename);
        domainOntologyDao.saveOrUpdate(domainOntology);
        LOGGER.info("Domain Ontology ID of the file is : " + domainOntology.getId());
        LOGGER.info("New Ontology file has beed sucssfully added in the system");

        /*
        save file to the folder
         */
        File file = new File(System.getProperty("user.home") + File.separator + uploadDir, newFilename);
        FileUtils.writeByteArrayToFile(file, bytestream);

        owlFileProcessingService.processOntologyFile(file, domainOntology);

        // processOntologyFile(ontology, domainOntology);
        return domainOntology;
    }

    /**
     *
     * @param ontoogy
     * @param domainOntology
     */
    @Async("threadPoolTaskExecutor")
    public void processOntologyFile(final OWLOntology ontoogy, DomainOntology domainOntology) {
        /*
        * parse the owl file and get the classes
         */
        Set<OWLClass> cls = ontoogy.getClassesInSignature();
        for (OWLClass cl : cls) {
            if (!cl.getSubClasses(ontoogy).isEmpty()) {
                processChildClasses(cl, ontoogy, domainOntology);
            }
        }
    }

    /**
     *
     * @param owlc
     * @param oWLOntology
     * @param domainOntology
     */
    public void processChildClasses(OWLClass owlc, OWLOntology oWLOntology, DomainOntology domainOntology) {
        /*
        persist super class first
         */

        final String name = owlc.getIRI().toString();
        final String cname = HcdcStringUtils.splitCamelCase(name.substring(name.indexOf('#') + 1));
        OntDomainClasses odc = null;

        if (ontDomainClassesDao.isExist(domainOntology.getId(), cname)) {
            LOGGER.info("Get existing object");
            odc = ontDomainClassesDao.getClassBasedOnNameAndID(domainOntology.getId(), cname);
        } else {
            odc = new OntDomainClasses();
            odc.setClassname(cname);
            odc.setDomainOntology(domainOntology);
            odc.setQualifiedClassName(name);
            odc.setRecordCount(0L);
            odc.setIsUpdatedRecordCount(Boolean.FALSE);
            ontDomainClassesDao.createNew(odc);
            LOGGER.info("generated ID  : " + odc.getId());
        }
        /*
        get subclasses and process recusively
         */
        Set<OWLClassExpression> subs = owlc.getSubClasses(oWLOntology);
        if (!subs.isEmpty()) {
            for (OWLClassExpression sub : subs) {
                OWLClass subClass = sub.asOWLClass();
                final String tname = subClass.getIRI().toString();
                String cName = HcdcStringUtils.splitCamelCase(tname.substring(tname.indexOf('#') + 1));
                OntDomainClasses temp = new OntDomainClasses();
                temp.setClassname(cName);
                temp.setDomainOntology(domainOntology);
                temp.setParentClass(odc);
                temp.setQualifiedClassName(tname);
                LOGGER.info("Class IRI : " + tname);
                if (!ontDomainClassesDao.isExist(domainOntology.getId(), cName)) {
                    ontDomainClassesDao.createNew(temp);
                    processChildClasses(subClass, oWLOntology, domainOntology);
                }
            }
        }
    }

    public OntDomainClasses getOntClassForId(long id) {
        return ontDomainClassesDao.get(id);
    }
}
