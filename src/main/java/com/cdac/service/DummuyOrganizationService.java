/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.service;

import au.com.bytecode.opencsv.CSVReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

/**
 *
 * @author Suhas
 */
@Service("dummyOrgService")
public class DummuyOrganizationService {

    private static final Logger LOGGER = Logger.getLogger(DummuyOrganizationService.class);

    public static class Organization implements Serializable {

        private String orgName;
        private String orgCode;

        public Organization(String orgName, String orgCode) {
            this.orgName = orgName;
            this.orgCode = orgCode;
        }

        public String getOrgName() {
            return orgName;
        }

        public void setOrgName(String orgName) {
            this.orgName = orgName;
        }

        public String getOrgCode() {
            return orgCode;
        }

        public void setOrgCode(String orgCode) {
            this.orgCode = orgCode;
        }

    }

    private static Map<String, List<Organization>> getResourceType() {
        Map<String, List<Organization>> map = new HashMap<>();
        map.put("Open Repository", getOrganizationByResourceType("open_repository.csv"));
        map.put("National Programme on Technology Enhanced Learning", getOrganizationByResourceType("nptl_organizations.csv"));
        return map;
    }

    private static final Map<String, List<Organization>> map = getResourceType();

    public List<Organization> getOrgList(String resName) {
        return map.get(resName);
    }

    public Set<String> getResourcType() {
        return map.keySet();
    }

    /**
     * This method reads list of Organization Name and Organization Code from 
     * [resourceFileName].csv file which is kept in project resources directory.
     * 
     * @param resourceFileName
     * @return 
     */
    public static ArrayList<Organization> getOrganizationByResourceType(String resourceFileName) {
        ArrayList<Organization> organizationList = new ArrayList<>();
        try (CSVReader reader = new CSVReader(new FileReader(new ClassPathResource(resourceFileName).getFile()))) {

            String[] csvEntry;
            while ((csvEntry = reader.readNext()) != null) {
                if (csvEntry.length > 0) {
                    organizationList.add(new Organization(csvEntry[0], csvEntry[1]));
                }
            }
        } catch (IOException ex) {
            LOGGER.error("Error ontology process\n" + ex.getLocalizedMessage(), ex);
        }
        return organizationList;
    }
}
