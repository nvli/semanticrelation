package com.cdac.service;

import com.cdac.entity.DomainOntology;
import com.cdac.entity.OntDomainClasses;
import com.cdac.entity.dao.IDomainOntologyDao;
import com.cdac.entity.dao.IOntDomainClassesDao;
import com.cdac.utils.HcdcStringUtils;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gajraj
 */
@Service
public class OwlFileProcessingService {

    private static final Logger LOGGER = Logger.getLogger(OwlFileProcessingService.class);
    @Autowired
    private IOntDomainClassesDao ontDomainClassesDao;
    @Autowired
    private IDomainOntologyDao domainOntologyDao;
    @Autowired
    private JsonConversoinService jsonConversoinService;

    @Value("${json.directory}")
    private String jsonOutputDirectory;

    private final Logger LOOGER = Logger.getLogger(OwlFileProcessingService.class);

    /**
     *
     * @param owlFile
     * @param domainOntology
     */
    @Async
    public void processOntologyFile(File owlFile, DomainOntology domainOntology) {

        String outputJsonName = owlFile.getName();
        outputJsonName = outputJsonName.substring(0, outputJsonName.lastIndexOf('.'));
        jsonConversoinService.createTempAndGenerateJson(owlFile, outputJsonName);

        File jsonFile = new File(System.getProperty("user.home") + File.separator + jsonOutputDirectory + File.separator + outputJsonName + ".json");
        JSONParser parser = new JSONParser();
        Object obj = null;

        try {
            obj = parser.parse(new FileReader(jsonFile));
        } catch (ParseException | IOException ex) {
            LOOGER.error("Error in reading file", ex);
        }

        JSONObject jsonObject = (JSONObject) obj;
        JSONArray classList = (JSONArray) jsonObject.get("classAttribute");
        Iterator<JSONObject> iterator = classList.iterator();

        while (iterator.hasNext()) {
            JSONObject classJson = iterator.next();
            if (classJson.get("iri") != null) {
                final String classIRI = classJson.get("iri").toString();
                final long jsonClassId = Long.parseLong(classJson.get("id").toString());
                final String name = classIRI.contains("#") ? classIRI : new StringBuilder(classIRI).replace(classIRI.lastIndexOf("/"), classIRI.lastIndexOf("/") + 1, "#").toString();
                final String cname = WordUtils.capitalize(HcdcStringUtils.splitCamelCase(getClassNameFromIRI(name)));

                OntDomainClasses odc = null;
                if (ontDomainClassesDao.isExist(classIRI, domainOntology.getId())) {
                    LOGGER.info("Get existing object");
                    odc = ontDomainClassesDao.getClassBasedOnDomainIdAndClassIRI(domainOntology.getId(), classIRI);
                } else {
                    odc = new OntDomainClasses();
                    odc.setClassname(cname);
                    odc.setDomainOntology(domainOntology);
                    odc.setQualifiedClassName(classIRI);
                    odc.setJsonClassID(jsonClassId);
                    odc.setRecordCount(0L);
                    odc.setIsUpdatedRecordCount(Boolean.FALSE);
                    ontDomainClassesDao.createNew(odc);
                    LOGGER.info("generated ID  : " + odc.getId());
                }
            }
        }
        /*try {
            LOGGER.info("Current thread name =" + Thread.currentThread().getName());
            System.out.println("Current thread name =" + Thread.currentThread().getName() + "\t" + domainOntology.getFileName());

            domainOntology.setProcessingStatus(Status.RUNNING);
            domainOntologyDao.saveOrUpdate(domainOntology);

            OntologyReader ontologyReader = new OntologyReader(owlFile);
            OWLOntology ontology = ontologyReader.getOntoogy();

            //parse the owl file and get the classes
            Set<OWLClass> cls = ontology.getClassesInSignature();
            for (OWLClass cl : cls) {
            if (!cl.getSubClasses(ontology).isEmpty()) {
            processChildClasses(cl, ontology, domainOntology);
            }
            }
            
            domainOntology.setProcessingStatus(Status.COMPLETED);
            domainOntologyDao.saveOrUpdate(domainOntology);
            System.out.println("Completed =" + Thread.currentThread().getName() + "\t" + domainOntology.getFileName());
            LOGGER.info("Creating thread for json conversion for the add3d ontology");
            String outputJsonName = owlFile.getName();
            outputJsonName = outputJsonName.substring(0, outputJsonName.lastIndexOf('.'));
            jsonConversoinService.createTempAndGenerateJson(owlFile, outputJsonName);
            
            } catch (OWLOntologyCreationException ex) {
            domainOntology.setProcessingStatus(Status.INTERRUPTED);
            domainOntologyDao.saveOrUpdate(domainOntology);
            LOOGER.error("Error in reading file", ex);
            }*/
    }

    /**
     *
     * @param owlc
     * @param oWLOntology
     * @param domainOntology
     */
    /*public void processChildClasses(OWLClass owlc, OWLOntology oWLOntology, DomainOntology domainOntology) {
     
        persist super class first
       
        //final String name = owlc.getIRI().toString();
        final String name = owlc.getIRI().toString().contains("#") ? owlc.getIRI().toString() : new StringBuilder(owlc.getIRI().toString()).replace(owlc.getIRI().toString().lastIndexOf("/"), owlc.getIRI().toString().lastIndexOf("/") + 1, "#").toString();

        final String cname = WordUtils.capitalize(HcdcStringUtils.splitCamelCase(getClassNameFromIRI(name)));
        OntDomainClasses odc = null;

        if (ontDomainClassesDao.isExist(domainOntology.getId(), cname)) {
            LOGGER.info("Get existing object");
            odc = ontDomainClassesDao.getClassBasedOnNameAndID(domainOntology.getId(), cname);
        } else {
            odc = new OntDomainClasses();
            odc.setClassname(cname);
            odc.setDomainOntology(domainOntology);
            odc.setQualifiedClassName(name);
            odc.setRecordCount(0L);
            odc.setIsUpdatedRecordCount(Boolean.FALSE);
            ontDomainClassesDao.createNew(odc);
            LOGGER.info("generated ID  : " + odc.getId());
        }
        get subclasses and process recusively
        Set<OWLClassExpression> subs = owlc.getSubClasses(oWLOntology);
        if (!subs.isEmpty()) {
            for (OWLClassExpression sub : subs) {
                OWLClass subClass = sub.asOWLClass();
                //final String tname = subClass.getIRI().toString();
                final String tname = subClass.getIRI().toString().contains("#") ? subClass.getIRI().toString() : new StringBuilder(subClass.getIRI().toString()).replace(subClass.getIRI().toString().lastIndexOf("/"), subClass.getIRI().toString().lastIndexOf("/") + 1, "#").toString();

                String cName = WordUtils.capitalize(HcdcStringUtils.splitCamelCase(getClassNameFromIRI(tname)));
                OntDomainClasses temp = new OntDomainClasses();
                temp.setClassname(cName);
                temp.setDomainOntology(domainOntology);
                temp.setParentClass(odc);
                temp.setQualifiedClassName(tname);
                temp.setRecordCount(0L);
                temp.setIsUpdatedRecordCount(Boolean.FALSE);
                LOGGER.info("Class IRI : " + tname);
                if (!ontDomainClassesDao.isExist(domainOntology.getId(), cName)) {
                    ontDomainClassesDao.createNew(temp);
                    processChildClasses(subClass, oWLOntology, domainOntology);
                }
            }
        }
    }*/
    private String getClassNameFromIRI(String classname) {
        int index = classname.lastIndexOf('#');
        if (index < 0) {
            index = classname.lastIndexOf('/');
        }
        return classname.substring(index + 1);
    }
}
