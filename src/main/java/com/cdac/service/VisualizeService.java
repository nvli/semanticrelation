package com.cdac.service;

import com.cdac.entity.DomainOntology;
import com.cdac.entity.OntDomainClasses;
import com.cdac.entity.dao.IDomainOntologyDao;
import com.cdac.entity.dao.IOntDomainClassesDao;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Hemant Anjana
 */
@Service("visualizeService")
public class VisualizeService {
    
    @Value("${fuseki.serviceURI}")
    private String FUSEKI_SERVICE_URI;
    @Autowired
    private SparqlServiceFuseki sparqlServiceFuseki;
    
    @Autowired
    IDomainOntologyDao domainOntologyDao;
    
    @Autowired
    IOntDomainClassesDao ontDomainClassesDao;

    /**
     * Calls rest webservice of NVLisemantic relation for count of given
     * classIRI.
     *
     * @param classIRI
     * @return
     */
    @SuppressWarnings("unchecked")
    public Integer callRestSparqlEndPointForCount(String classIRI, String refPrefix) {
        Integer count = 0;
        /* Below code is calling NvliSemanticRalations rest services.

        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://10.208.35.183:8080/NvliSemanticRalations/sparql/getClassRecordCount?classIRI=" + classIRI;
        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<Integer> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, Integer.class);
        count = responseEntity.getBody();
         */
        count = sparqlServiceFuseki.getRecordCountForClass(FUSEKI_SERVICE_URI, classIRI, refPrefix, "#");
        if (count == 0) {
            return sparqlServiceFuseki.getRecordCountForClass(FUSEKI_SERVICE_URI, classIRI, refPrefix, "/");
        } else {
            return count;
        }
    }

    /**
     * Calls rest webservice of NVLisemantic relation for records of given
     * classIRI.
     *
     * @param classIRI
     * @param pageNum
     * @param pageWin
     * @return
     */
    @SuppressWarnings("unchecked")
    public Map<String, String> callRestSparqlEndPointForRecord(String classIRI, Integer pageNum, Integer pageWin, String refPrefix) {
        Map<String, String> recMap = null;
        /*
         Below code is calling NvliSemanticRalations rest services.

        HttpHeaders headers = new HttpHeaders();
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://10.208.35.183:8080/NvliSemanticRalations/sparql/getRecords/" + pageNum + "/" + pageWin + "?classIRI=" + classIRI;

        HttpEntity request = new HttpEntity(headers);
        ResponseEntity<List> responseEntity = restTemplate.exchange(url, HttpMethod.GET, request, List.class);

        list = responseEntity.getBody();
         */
        int offset = (pageNum - 1) * pageWin;
        recMap = sparqlServiceFuseki.getRecordForClass(FUSEKI_SERVICE_URI, classIRI, offset, pageWin, refPrefix, "#");
        if (recMap.isEmpty()) {
            return sparqlServiceFuseki.getRecordForClass(FUSEKI_SERVICE_URI, classIRI, offset, pageWin, refPrefix, "/");
        } else {
            return recMap;
        }
    }

    /**
     * Returns the list of all Domain Ontology for listing on main page.
     *
     * @return
     */
    public List<DomainOntology> getDomainOntologies() {
        return domainOntologyDao.getDomainOntologies();
    }

    /**
     * Returns the list of all Domain Ontology for listing on main page.
     *
     * @param ontId
     * @return
     */
    public DomainOntology getDomainOntologyById(long ontId) {
        return domainOntologyDao.get(ontId);
    }

    /**
     * returns count of record for given class. if
     * ontClass.getIsUpdatedRecordCount() is true then returns count from
     * database else generate from fuseki server and updates
     * ontClass.getIsUpdatedRecordCount() to true and recordCount to record
     * count.
     *
     * @param className
     * @param ontId
     * @return
     */
    public long getRecordCountForClass(String className, long ontId) {
//        OntDomainClasses ontClass = ontDomainClassesDao.getClassBasedOnNameAndID(ontId, HcdcStringUtils.splitCamelCaseWithSpaceSeperate(className).replaceAll("\\s{2,}", " ").replaceAll(HcdcStringUtils.NAME_TO_IRI, "").trim());
        OntDomainClasses ontClass = ontDomainClassesDao.getClassBasedOnNameAndID(ontId, className);
        Optional<OntDomainClasses> optionalOntClass = Optional.ofNullable(ontClass);
        if (optionalOntClass.isPresent()) {
            if (ontClass.getIsUpdatedRecordCount()) {
                //return ontDomainClassesDao.getRecordCountForClass(HcdcStringUtils.splitCamelCaseWithSpaceSeperate(className).replaceAll("\\s{2,}", " ").replaceAll(HcdcStringUtils.NAME_TO_IRI, "").trim(), ontId);
                return ontDomainClassesDao.getRecordCountForClass(className, ontId);
            } else {
                if (ontId == 1 || ontId == 3) {
                    className = className.replaceAll("\\s", "_");
                }
                long recordCount = sparqlServiceFuseki.getRecordCountForClass(FUSEKI_SERVICE_URI, className, getClassBasedOnJsonIdAndDomainOntID(ontId, className).getQualifiedClassName(), "#");
                if (recordCount == 0) {
                    recordCount = sparqlServiceFuseki.getRecordCountForClass(FUSEKI_SERVICE_URI, className, getClassBasedOnJsonIdAndDomainOntID(ontId, className).getQualifiedClassName(), "/");
                }
                ontClass.setRecordCount(recordCount);
                ontClass.setIsUpdatedRecordCount(Boolean.TRUE);
                ontDomainClassesDao.saveOrUpdate(ontClass);
                return recordCount;
            }
        } else {
            ontClass = new OntDomainClasses();
            ontClass.setClassname(className);
            ontClass.setDomainOntology(domainOntologyDao.get(ontId));
            ontClass.setIsUpdatedRecordCount(Boolean.FALSE);
            ontClass.setRecordCount(0L);
            ontClass.setJsonClassID(Long.parseLong(className));
            ontClass.setQualifiedClassName(domainOntologyDao.get(ontId).getNamespaceURI().concat("#").concat(className));
            ontDomainClassesDao.createNew(ontClass);
            getRecordCountForClass(className, ontId);
        }
        return 0;
    }
    
    public OntDomainClasses getClassBasedOnJsonIdAndDomainOntID(long domOntId, String classJSOnId) {
        return ontDomainClassesDao.getClassBasedOnJsonIdAndDomainOntID(domOntId, classJSOnId);
    }
}
