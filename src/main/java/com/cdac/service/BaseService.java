package com.cdac.service;

import com.cdac.entity.Domain;
import com.cdac.entity.dao.IDomainDAO;
import com.cdac.entity.dao.IDomainOntologyDao;
import com.cdac.utils.ResultHolder;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Suhas
 */
@Service("baseService")
public class BaseService {

    private final Logger LOGGER = Logger.getLogger(BaseService.class);

    @Autowired
    private IDomainDAO domainDao;

    @Autowired
    private IDomainOntologyDao domainOntologyDao;

    public ResultHolder getListDomains(int pageNum, int pageWin) {
        return domainDao.getListDomains(pageNum, pageWin);
    }

    public ResultHolder getListDomainOntologies(int pageNum, int pageWin) {
        return domainOntologyDao.getListDomainOntologies(pageNum, pageWin);
    }

    public List<Domain> getListBaseDomains() {
        return domainDao.getBaseDomains();
    }

    public List<Domain> getSubDomains(int parentDomainId) {
        return domainDao.getSubDomains(parentDomainId);
    }
}
