package com.cdac.service;

import com.cdac.entity.ResourceProcess;
import com.cdac.entity.dao.IOntDomainClassesDao;
import com.cdac.entity.dao.IResourceProcessDAO;
import com.cdac.model.RecordOAI;
import com.cdac.processor.LuceneBasedRecordProcessor;
import com.cdac.utils.NvliConstants;
import com.cdac.utils.Status;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gajraj
 */
@Service
public class ProcessorService {

    private final Logger LOGGER = Logger.getLogger(ProcessorService.class);

    @Autowired
    private OntologyService ontologyService;
    @Autowired
    private IOntDomainClassesDao ontDomainClassesDao;

    @Value("${fuseki.serviceURI}")
    String fusekiServiceURI;
    @Value("${nvli.base.namespace}")
    String nvliBaseURI;
    @Value("${csv.directory}")
    String csvDirectory;
    @Autowired
    SparqlServiceFuseki sparqlServiceFuseki;
    @Autowired
    private IResourceProcessDAO resourceProcessDAO;
    @Value("${temp.directory}")
    private String tempDir;
    private File tempOwlFile;
    private File dataFile;

    /*
    private static Map<String, Integer> fieldIndexMap;

    static {
        fieldIndexMap = new HashMap<>();
        fieldIndexMap.put("identifier", 0);
        fieldIndexMap.put("title", 1);
        fieldIndexMap.put("subject", 2);
        fieldIndexMap.put("creater", 3);
        fieldIndexMap.put("description", 4);
        fieldIndexMap.put("publisher", 5);
    }
     */
    /**
     * if sourceFile and temp ontology file cant be found or created then
     * process will not be start
     *
     * @param resourceProcess
     * @return
     */
    @Async
    public void init(ResourceProcess resourceProcess) {
        try {
            //check for source file first
            dataFile = new File(System.getProperty("user.home") + File.separator + csvDirectory + File.separator + resourceProcess.getResourceCode() + ".csv");
            if (!dataFile.exists()) {
                LOGGER.error("Data for given organisation code " + resourceProcess.getResourceCode() + " is not available");
                updateStatus(Status.INTERRUPTED, resourceProcess);
            } else {
                //create temp owl file
                Resource resource = new ClassPathResource(NvliConstants.RECORD_PROCESSING_ONTOLOGY_FILENAME);
                final File targetFile = resource.getFile();
                //TODO remove undersocer
                tempOwlFile = new File(System.getProperty("user.home") + File.separator + tempDir + File.separator + resourceProcess.getId() + "_" + targetFile.getName());
                FileUtils.copyFile(targetFile, tempOwlFile);
                processLucene(resourceProcess);
            }
        } catch (IOException ex) {
            LOGGER.error("System cant find the NVLI Ontology");
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
    }

    @Async
    public void processWithCSV(ResourceProcess resourceProcess) {
        /*
        try {
            Resource resource = new ClassPathResource(NvliConstants.RECORD_PROCESSING_ONTOLOGY_FILENAME);
            final File targetFile = resource.getFile();
            //TODO remove undersocer
            File tempFile = new File(System.getProperty("user.home") + File.separator + tempDir + File.separator + resourceProcess.getId() + "_" + targetFile.getName());
            FileUtils.copyFile(targetFile, tempFile);
            boolean flag = false;

            /**
             * TODO send resourceType in the resourceProcess object resourceType
             * should not contains white space use '_' as seperator.
         */
 /*   OntProcessorForCSV forCSV = new OntProcessorForCSV(tempFile, ontologyService, fieldIndexMap, resourceProcess);
            File csvFile = new File(System.getProperty("user.home") + File.separator + csvDirectory + File.separator + resourceProcess.getResourceCode() + ".csv");
            if (!csvFile.exists()) {
                LOGGER.error("Data for given organisation code " + resourceProcess.getResourceCode() + " is not available");
                return;
            }
            updateStatus(resourceProcess);
            flag = forCSV.processMetadataWithOWL(csvFile);
            if (!flag) {
                resourceProcess.setStatus(Status.INTERRUPTED);
                resourceProcessDAO.saveOrUpdate(resourceProcess);
            } else {
                try {
                    /**
                     * now upload the file to the fuseki server
                     *
                     *
                    sparqlServiceFuseki.uploadRdf(fusekiServiceURI, tempFile, nvliBaseURI);
                    resourceProcess.setStatus(Status.COMPLETED);
                    resourceProcessDAO.saveOrUpdate(resourceProcess);
                    flag = true;
                    if (tempFile.delete()) {
                        LOGGER.info("Temp file deleted successfully");
                    } else {
                        LOGGER.error("Unable to delete the processed file");
                    }
                } catch (FileNotFoundException ex) {
                    flag = false;
                    LOGGER.error("File not found");
                }
            }
            if (!flag) {
                resourceProcess.setStatus(Status.INTERRUPTED);
                resourceProcessDAO.saveOrUpdate(resourceProcess);
            }
        } catch (OWLOntologyCreationException | IOException ex) {
            resourceProcess.setStatus(Status.INTERRUPTED);
            resourceProcessDAO.saveOrUpdate(resourceProcess);
            LOGGER.error("Error ontology process\n" + ex.getLocalizedMessage(), ex);
        }
    }

    @Async
    public void processWithCSVwithStrongWeak(ResourceProcess resourceProcess) {

        try {
            Resource resource = new ClassPathResource(NvliConstants.RECORD_PROCESSING_ONTOLOGY_FILENAME);
            final File targetFile = resource.getFile();
            //TODO remove undersocer
            File tempFile = new File(System.getProperty("user.home") + File.separator + tempDir + File.separator + resourceProcess.getId() + "_" + targetFile.getName());
            FileUtils.copyFile(targetFile, tempFile);
            boolean flag = false;

            /**
             * TODO send resourceType in the resourceProcess object resourceType
             * should not contains white space use '_' as seperator.
             *
            OntProcessorForCSV forCSV = new OntProcessorForCSV(tempFile, ontologyService, fieldIndexMap, resourceProcess);
            File csvFile = new File(System.getProperty("user.home") + File.separator + csvDirectory + File.separator + resourceProcess.getResourceCode() + ".csv");
            if (!csvFile.exists()) {
                LOGGER.error("Data for given organisation code " + resourceProcess.getResourceCode() + " is not available");
                return;
            }
            updateStatus(resourceProcess);
            DomainWordWeight wfmap = WordWeightProcessor.loadFromJsonText(resourceProcess.getReferenceOnt().getWordFrequencyMap());

            flag = forCSV.processMetadataWithOWL(csvFile, wfmap);

            if (!flag) {
                resourceProcess.setStatus(Status.INTERRUPTED);
                resourceProcessDAO.saveOrUpdate(resourceProcess);
            } else {
                try {
                    /*
                now upload the file to the fuseki server

                     *
                    sparqlServiceFuseki.uploadRdf(fusekiServiceURI, tempFile, nvliBaseURI);
                    resourceProcess.setStatus(Status.COMPLETED);
                    resourceProcessDAO.saveOrUpdate(resourceProcess);
                    flag = true;
                    if (tempFile.delete()) {
                        LOGGER.info("Temp file deleted successfully");
                    } else {
                        LOGGER.error("Unable to delete the processed file");
                    }
                } catch (FileNotFoundException ex) {
                    flag = false;
                    LOGGER.error("File not found");
                }
            }
            if (!flag) {
                resourceProcess.setStatus(Status.INTERRUPTED);
                resourceProcessDAO.saveOrUpdate(resourceProcess);
            }
        } catch (OWLOntologyCreationException | IOException ex) {
            resourceProcess.setStatus(Status.INTERRUPTED);
            resourceProcessDAO.saveOrUpdate(resourceProcess);
            LOGGER.error("Error ontology process\n" + ex.getLocalizedMessage(), ex);
        }
    }

    @Async
    public void addSemanticInformation(ResourceProcess resourceProcess, BiFunction<OntProcessorForCSV, File, Boolean> biFunction) {
        try {
            Resource resource = new ClassPathResource(NvliConstants.RECORD_PROCESSING_ONTOLOGY_FILENAME);
            final File targetFile = resource.getFile();
            //TODO remove undersocer
            File tempFile = new File(System.getProperty("user.home") + File.separator + tempDir + File.separator + resourceProcess.getId() + "_" + targetFile.getName());
            FileUtils.copyFile(targetFile, tempFile);
            boolean flag = false;

            /**
             * TODO send resourceType in the resourceProcess object resourceType
             * should not contains white space use '_' as seperator.
             *
            OntProcessorForCSV forCSV = new OntProcessorForCSV(tempFile, ontologyService, fieldIndexMap, resourceProcess);
            File csvFile = new File(System.getProperty("user.home") + File.separator + csvDirectory + File.separator + resourceProcess.getResourceCode() + ".csv");
            if (!csvFile.exists()) {
                LOGGER.error("Data for given organisation code " + resourceProcess.getResourceCode() + " is not available");
                return;
            }
            updateStatus(resourceProcess);
            flag = biFunction.apply(forCSV, csvFile);
            if (!flag) {
                resourceProcess.setStatus(Status.INTERRUPTED);
                resourceProcessDAO.saveOrUpdate(resourceProcess);
            } else {
                try {
                    /**
                     * now upload the file to the fuseki server
                     *
                     *
                    sparqlServiceFuseki.uploadRdf(fusekiServiceURI, tempFile, nvliBaseURI);
                    resourceProcess.setStatus(Status.COMPLETED);
                    resourceProcessDAO.saveOrUpdate(resourceProcess);
                    flag = true;
                    if (tempFile.delete()) {
                        LOGGER.info("Temp file deleted successfully");
                    } else {
                        LOGGER.error("Unable to delete the processed file");
                    }
                } catch (FileNotFoundException ex) {
                    flag = false;
                    LOGGER.error("File not found");
                }
            }
            if (!flag) {
                resourceProcess.setStatus(Status.INTERRUPTED);
                resourceProcessDAO.saveOrUpdate(resourceProcess);
            }
        } catch (OWLOntologyCreationException | IOException ex) {
            resourceProcess.setStatus(Status.INTERRUPTED);
            resourceProcessDAO.saveOrUpdate(resourceProcess);
            LOGGER.error("Error ontology process\n" + ex.getLocalizedMessage(), ex);
        }
    }

    public static BiFunction<OntProcessorForCSV, File, Boolean> processOnlyMetadata = (x, y) -> {
        return x.processMetadataExceptRefOntProcessing(y);
    };
    public static BiFunction<OntProcessorForCSV, File, Boolean> processWithRefOnt = (x, y) -> {
        return x.processMetadataWithOWL(y);
    };
    public static BiFunction<OntProcessorForCSV, File, Boolean> processWithStrongWeak = (x, y) -> {
        return x.processMetadataWithOWL(y);
         */
    }

    ;

    /**
     *
     * @param resourceProcess
     */
    public void processLucene(ResourceProcess resourceProcess) {
        LuceneBasedRecordProcessor lbrp;
        try {
            lbrp = new LuceneBasedRecordProcessor(tempOwlFile, resourceProcess.getResourceType(), resourceProcess.getResourceCode(), ontDomainClassesDao);
        } catch (OWLOntologyCreationException ex) {
            LOGGER.error("Error in opening the ontlology file . Stoppping processing now\n" + ex.getLocalizedMessage());
            updateStatus(Status.INTERRUPTED, resourceProcess);
            return;
        }
        LOGGER.info("started lbrp");
        updateStatus(Status.RUNNING, resourceProcess);

        List<RecordOAI> records = lbrp.getRecordsFromSource(dataFile);
        LOGGER.info("Processing this number of records in file : " + records.size());

        lbrp.processRecordsFromStream(records.stream());
        if (lbrp.saveOntology()) {
            LOGGER.info("Successfully saved ontology");
            uploadOntologyToFuskeiServer(resourceProcess);
        }
    }

    /**
     * update the status of the process
     */
    private void updateStatus(Status status, ResourceProcess resourceProcess) {
        resourceProcess.setStatus(status);
        resourceProcessDAO.saveOrUpdate(resourceProcess);
    }

    /**
     * upload the final enhanced ontology file to the fuseki endpoint and set
     * the status for the resource process
     *
     * @param resourceProcess
     */
    public void uploadOntologyToFuskeiServer(ResourceProcess resourceProcess) {
        try {
            sparqlServiceFuseki.uploadRdf(fusekiServiceURI, tempOwlFile, nvliBaseURI);
            resourceProcess.setStatus(Status.COMPLETED);
            /*if (tempOwlFile.delete()) {
                resourceProcess.setStatus(Status.COMPLETED);
                LOGGER.info("Temp file deleted successfully");
            } else {
                resourceProcess.setStatus(Status.INTERRUPTED);
                LOGGER.error("Unable to delete the processed file");
            }*/
        } catch (FileNotFoundException ex) {
            LOGGER.error("Owl File not found : " + ex.getLocalizedMessage());
        } finally {
            resourceProcessDAO.saveOrUpdate(resourceProcess);
        }
    }
}
