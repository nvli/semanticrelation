package com.cdac.service;

import com.cdac.utils.HcdcStringUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import org.apache.jena.query.DatasetAccessor;
import org.apache.jena.query.DatasetAccessorFactory;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.sparql.resultset.ResultSetException;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.IRI;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gajraj
 */
@Service(value = "sparqlFusekiService")
public class SparqlServiceFuseki {

    private final Logger LOGGER = Logger.getLogger(SparqlServiceFuseki.class);

    public static final int DEFAULT_LIMIT = 20;
    public static final int DEFAULT_OFFSET = 0;

    /**
     * upload a RDF file in the triple store. this will upload the file in
     * default graph of the dataset
     *
     * @param serviceURI
     * @param rdfFile
     * @param base
     * @throws FileNotFoundException
     */
    public void uploadRdf(final String serviceURI, File rdfFile, final String base) throws FileNotFoundException {
        DatasetAccessor accessor = DatasetAccessorFactory.createHTTP(serviceURI);
        InputStream in = new FileInputStream(rdfFile);
        Model model = ModelFactory.createDefaultModel();
        model.read(in, base, "RDF/XML");
        accessor.add(model);
    }

    /**
     * upload a rdf graph in the specified graph in the dataset
     *
     * @param serviceURI
     * @param rdfFile
     * @param base
     * @param graphname
     * @throws FileNotFoundException
     */
    public void uploadRdf(String serviceURI, File rdfFile, String base, String graphname) throws FileNotFoundException {
        DatasetAccessor accessor = DatasetAccessorFactory.createHTTP(serviceURI);
        InputStream in = new FileInputStream(rdfFile);
        Model model = ModelFactory.createDefaultModel();
        String graphURI = serviceURI + "/data/" + graphname;
        model.read(in, base, "RDF/XML");
        accessor.add(model);
    }

    public int getRecordCountForClass(String serviceURI, String className, String refURI, String classSeparator) {

        String countQuery = "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>"
                + "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
                + "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>\n"
                + "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "prefix : <http://www.cdac.in/technologies/ontology/nvli#>"
                //+ "\nselect  (count(distinct ?record) as ?count)\n"
                + "\nselect  (count(distinct *) as ?count)\n"
                + "WHERE {\n"
                + "{ ?record :hasDomainClassInSubject ?pClass }"
                + "union"
                + "{ ?record :hasDomainClassInTitle ?pClass }"
                + "\nFILTER (?pClass=\"" + refURI //+ classSeparator
                //+ HcdcStringUtils.toCamelCase(className)
                + "\""
                //+ " || "
                //+ "lcase(?pClass)=lcase(\"" + refURI //+ classSeparator
                //+ HcdcStringUtils.toCamelCase(className)
                //+ "\")).}"
                + ").}"
                + "\nLIMIT 25";

        System.out.println(countQuery);

        QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI,
                countQuery);
        ResultSet results = q.execSelect();

        QuerySolution soln = results.nextSolution();
        RDFNode x = soln.getLiteral("count");
        String cnt = x.toString();

        return Integer.parseInt(cnt.substring(0, x.toString().indexOf("^^")));

    }

    /*public int getRecordCountForClass(String serviceURI, String className, String refURI, String classSeparator) {

        String countQuery = "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>"
                + "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
                + "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>\n"
                + "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "prefix : <http://www.cdac.in/technologies/ontology/nvli#>"
                //+ "\nselect  (count(distinct ?record) as ?count)\n"
                + "\nselect  (count(distinct *) as ?count)\n"
                + "WHERE {\n"
                + "{ ?record :hasDomainClassInSubject ?pClass }"
                + "union"
                + "{ ?record :hasDomainClassInTitle ?pClass }"
                + "\nFILTER (?pClass=\"" + refURI + classSeparator
                + HcdcStringUtils.toCamelCase(className)
                + "\""
                + " || "
                + "lcase(?pClass)=lcase(\"" + refURI + classSeparator
                + HcdcStringUtils.toCamelCase(className)
                + "\")).}"
                + "\nLIMIT 25";

        System.out.println(countQuery);

        QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI,
                countQuery);
        ResultSet results = q.execSelect();

        QuerySolution soln = results.nextSolution();
        RDFNode x = soln.getLiteral("count");
        String cnt = x.toString();

        return Integer.parseInt(cnt.substring(0, x.toString().indexOf("^^")));

    }*/

    public Map<String, String> getRecordForClass(String serviceURI, String className, int offset, int limit, String refURI, String classSeparator) {
        Map<String, String> recordmap = new HashMap<>(limit);
        String recordQuery = "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>"
                + "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
                + "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>\n"
                + "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "prefix : <http://www.cdac.in/technologies/ontology/nvli#>"
                + "\nselect  distinct ?record ?name\n"
                + "WHERE {\n"
                + "{ ?record :hasDomainClassInSubject ?pClass }"
                + "union"
                + "{ ?record :hasDomainClassInTitle ?pClass }"
                + "\nFILTER (?pClass=\"" + refURI //+ classSeparator
                //+ HcdcStringUtils.toCamelCase(className)
                + "\""
                //+ "||"
                //+ "  lcase(?pClass)=lcase(\"" + refURI //+ classSeparator
                //+ HcdcStringUtils.toCamelCase(className)
                //+ "\"))"
                + ")"
                + "\n?record :hasRecordName ?name.}"
                + "\noffset " + offset
                + " LIMIT " + limit;

        System.out.println(recordQuery);
        QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI, recordQuery);
        ResultSet results = q.execSelect();
        try {
            while (results.hasNext()) {
                QuerySolution soln = results.nextSolution();
                String record = soln.get("record").toString();
                recordmap.put(record.substring(record.lastIndexOf("/") + 1), soln.get("name").toString());
            }
            q.close();
        } catch (ResultSetException rse) {
            // rse.printStackTrace();
        }
        //  ResultSetFormatter.out(System.out, results);

        return recordmap;

    }

    /*public Map<String, String> getRecordForClass(String serviceURI, String className, int offset, int limit, String refURI, String classSeparator) {
        Map<String, String> recordmap = new HashMap<>(limit);
        String recordQuery = "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>"
                + "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
                + "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>\n"
                + "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "prefix : <http://www.cdac.in/technologies/ontology/nvli#>"
                + "\nselect  distinct ?record ?name\n"
                + "WHERE {\n"
                + "{ ?record :hasDomainClassInSubject ?pClass }"
                + "union"
                + "{ ?record :hasDomainClassInTitle ?pClass }"
                + "\nFILTER (?pClass=\"" + refURI + classSeparator
                + HcdcStringUtils.toCamelCase(className)
                + "\""
                + "||"
                + "  lcase(?pClass)=lcase(\"" + refURI + classSeparator
                + HcdcStringUtils.toCamelCase(className)
                + "\"))"
                + "\n?record :hasRecordName ?name.}"
                + "\noffset " + offset
                + " LIMIT " + limit;

        System.out.println(recordQuery);
        QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI, recordQuery);
        ResultSet results = q.execSelect();
        try {
            while (results.hasNext()) {
                QuerySolution soln = results.nextSolution();
                String record = soln.get("record").toString();
                recordmap.put(record.substring(record.lastIndexOf("/") + 1), soln.get("name").toString());
            }
            q.close();
        } catch (ResultSetException rse) {
            // rse.printStackTrace();
        }
        //  ResultSetFormatter.out(System.out, results);

        return recordmap;

    }*/
    /**
     * get the authors of the given record
     *
     * @param serviceURI
     * @param recordIdentifier
     * @return
     */
    public Set<String> getAuthorsForRecord(String serviceURI, String recordIdentifier) {
        Set<String> authorList = new HashSet<>();

        recordIdentifier = new StringBuilder("<http://www.cdac.in/technologies/ontology/").append(recordIdentifier).append(">").toString();

        String authorsOfRecordQuery = "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>"
                + "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
                + "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>\n"
                + "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "prefix : <http://www.cdac.in/technologies/ontology/nvli#>"
                + "\nselect  distinct ?author\n"
                + "WHERE {\n"
                + recordIdentifier + " :hasAuthor ?author }"
                + " LIMIT 25";

        QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI, authorsOfRecordQuery);
        ResultSet results = q.execSelect();

        try {
            while (results.hasNext()) {
                QuerySolution soln = results.nextSolution();
                String author = soln.get("author").toString();
                author = author.substring(author.lastIndexOf('/') + 1);
                authorList.add(HcdcStringUtils.getUnderScoreToSpaceSeperated(author));
            }
            q.close();
        } catch (ResultSetException rse) {
            rse.printStackTrace();
        }
        return authorList;
    }

    /**
     * get authors which have worked with given author
     *
     * @param serviceURI
     * @param author
     * @param offset
     * @param winSize
     * @return
     */
    public static List<String> getCoAuthors(String serviceURI, String author, int offset, int winSize) {
        System.out.println(author);
        String originalStr = author;
        List<String> coAuthors = new ArrayList<>();
        author = new StringBuilder("<http://www.cdac.in/technologies/ontology/").append(HcdcStringUtils.getSpaceSeparetedToUnderScore(author)).append(">").toString();
        String coAuthordQuery = "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>"
                + "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
                + "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>\n"
                + "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "prefix : <http://www.cdac.in/technologies/ontology/nvli#>"
                + "\nselect distinct ?author\n"
                + "WHERE {\n"
                + "?record :hasAuthor " + author + "."
                + "\n?record :hasAuthor ?author }"
                + "\noffset " + offset
                + "\nLIMIT " + winSize;
        System.out.println(coAuthordQuery);
        QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI, coAuthordQuery);
        ResultSet results = q.execSelect();

        try {
            while (results.hasNext()) {
                QuerySolution soln = results.nextSolution();
                String coAuthor = soln.get("author").toString();
                coAuthor = coAuthor.substring(coAuthor.lastIndexOf('/') + 1);
                coAuthors.add(HcdcStringUtils.getUnderScoreToSpaceSeperated(coAuthor));
            }
            Collections.swap(coAuthors, coAuthors.indexOf(originalStr), 0);
            if (offset == 1) {
                Collections.swap(coAuthors, coAuthors.indexOf(originalStr), 0);
            }
            q.close();
        } catch (ResultSetException rse) {
            rse.printStackTrace();
        }
        return coAuthors;
    }

    /**
     * get authors which have worked with given author
     *
     * @param serviceURI
     * @param author
     * @param offset
     * @param winSize
     * @return
     */
    public Map<String, HashSet<String>> getRecordsAndCoAuthors(String serviceURI, String author, int offset, int winSize) {
        System.out.println(author);
        Map<String, HashSet<String>> coAuthor = new HashMap<>();
        author = new StringBuilder("<http://www.cdac.in/technologies/ontology/").append(HcdcStringUtils.getSpaceSeparetedToUnderScore(author)).append(">").toString();
        String coAuthordQuery = "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>"
                + "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
                + "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>\n"
                + "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "prefix : <http://www.cdac.in/technologies/ontology/nvli#>"
                + "\nselect distinct ?record ?author\n"
                + "WHERE {\n"
                + "?record :hasAuthor " + author + "."
                + "\n?record :hasAuthor ?author }"
                + "\noffset " + offset
                + "\nLIMIT " + winSize;
        System.out.println(coAuthordQuery);
        QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI, coAuthordQuery);
        ResultSet results = q.execSelect();

        try {
            while (results.hasNext()) {
                QuerySolution soln = results.nextSolution();
                String record = soln.get("record").toString();
                String tauthor = soln.get("author").toString();
                record = record.substring(record.lastIndexOf('/') + 1).trim();
                final String author1 = HcdcStringUtils.getUnderScoreToSpaceSeperated(tauthor.substring(tauthor.lastIndexOf('/') + 1).trim());

                coAuthor.computeIfPresent(record, (x, y) -> {
                    y.add(author1);
                    return y;
                });
                coAuthor.computeIfAbsent(record, (x) -> {
                    HashSet<String> g = new HashSet<>();
                    g.add(author1);
                    return g;
                });
            }
            q.close();
        } catch (ResultSetException rse) {
            LOGGER.error(rse.getLocalizedMessage());
        }

        return coAuthor;

    }

    /**
     * get the records for given authors and organization
     *
     * @param authorsIRIs
     * @param organizationIRI
     * @param offset
     * @param limit
     * @param serviceURI
     * @return
     */
    public static Map<String, String> getRecordsForAuthors(List<IRI> authorsIRIs, IRI organizationIRI, int offset, int limit, String serviceURI) {
        StringBuilder sb = new StringBuilder(getPrefixImports());
        sb.append("select distinct ?record ?name \n where {\n");
        Optional.ofNullable(organizationIRI).ifPresent(org -> sb.append("?record :isInstanceOf ").append(org.toQuotedString()).append(".\n"));
        MessageFormat messageFormat = new MessageFormat("?record :hasAuthor {0}.\n");
        authorsIRIs.forEach(author -> {
            Object[] args = {author.toQuotedString()};
            sb.append(messageFormat.format(args));
        });
        sb.append("?record :hasRecordName ?name");
        sb.append("}\n offset ").append(offset).append(" limit ").append(limit);
        System.out.println(sb.toString());
        QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI, sb.toString());
        ResultSet results = q.execSelect();
        Map<String, String> identifiersMap = new HashMap<>();
        results.forEachRemaining(result -> {
            identifiersMap.put(result.getResource("record").getLocalName(), result.get("name").toString());
        });
        return identifiersMap;
    }

    /**
     * get the records for given authors and organization
     *
     * @param authorsIRIs
     * @param organizationIRI
     * @param offset
     * @param limit
     * @param serviceURI
     * @return
     */
    public Integer getCountOfRecordsForAuthors(List<IRI> authorsIRIs, IRI organizationIRI, String serviceURI) {
        StringBuilder sb = new StringBuilder(getPrefixImports());
        sb.append("select (count(distinct ?record)as ?count) \n where {\n");
        Optional.ofNullable(organizationIRI).ifPresent(org -> sb.append("?record :isInstanceOf ").append(org.toQuotedString()).append(".\n"));
        MessageFormat messageFormat = new MessageFormat("?record :hasAuthor {0}.\n");
        authorsIRIs.forEach(author -> {
            Object[] args = {author.toQuotedString()};
            sb.append(messageFormat.format(args));
        });
        sb.append("}\n");
        System.out.println(sb.toString());

        QueryExecution q = QueryExecutionFactory.sparqlService(serviceURI, sb.toString());
        ResultSet results = q.execSelect();
        QuerySolution soln = results.nextSolution();
        RDFNode x = soln.getLiteral("count");
        String cnt = x.toString();
        return Integer.parseInt(cnt.substring(0, x.toString().indexOf("^^")));
    }

    /**
     * add the default prefix for the query
     *
     * @return
     */
    public static String getPrefixImports() {
        return "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>\n"
                + "PREFIX owl: <http://www.w3.org/2002/07/owl#>\n"
                + "prefix nvli: <http://www.cdac.in/technologies/ontology/nvli#>\n"
                + "prefix xsd: <http://www.w3.org/2001/XMLSchema#>\n"
                + "prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\n"
                + "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
                + "prefix : <http://www.cdac.in/technologies/ontology/nvli#>\n"
                + "BASE   <http://www.cdac.in/technologies/ontology/nvli>\n";
    }

    /**
     * get the all authors in the knowledge base. if organization is passed then
     * authors of the particular organization will be returned else all authors
     * in the system.
     *
     * @param resourceType
     * @param organisation
     * @param offset
     * @param limit
     * @param serviceURI
     * @return
     */
    public Set<String> getAllAuthors(String resourceType, IRI organisation, IRI publishers, String strAlfa, IRI srchAuthors, int offset, int limit, String serviceURI) {
        StringBuilder sb = new StringBuilder(getPrefixImports());
        sb.append("select distinct ?author\n where \n{");
        Optional.ofNullable(organisation).ifPresent(org -> sb.append("?record :isInstanceOf ").append(org.toQuotedString()).append(".\n"));
        Optional.ofNullable(resourceType).ifPresent(resType -> sb.append(" ?org rdf:type nvli:").append(resType).append(".\n"));
        Optional.ofNullable(resourceType).ifPresent(resType -> sb.append(" ?record :isInstanceOf ?org.\n"));
        Optional.ofNullable(srchAuthors).ifPresent(author -> sb.append("?record :hasAuthor ").append(author.toQuotedString()).append(".\n"));
        Optional.ofNullable(publishers).ifPresent(publisher -> sb.append("?record :hasPublisher ").append(publisher.toQuotedString()).append(".\n"));
        sb.append("?record :hasAuthor ?author\n");
        if (strAlfa != null) {
            sb.append("FILTER regex(str(?author),\"http://www.cdac.in/technologies/ontology/").append(strAlfa).append("\",\"i\").\n");
        }
        sb.append("}");
        sb.append("order by ?author");
        System.out.println(sb);
        sb.append(getoffsetAndLimit(offset, limit));
        QueryExecution qe = QueryExecutionFactory.sparqlService(serviceURI, sb.toString());
        ResultSet resuiltSet = qe.execSelect();
        Set<String> authorSet = new HashSet<>();
        resuiltSet.forEachRemaining(result -> {
            authorSet.add(HcdcStringUtils.getUnderScoreToSpaceSeperated(result.getResource("author").getLocalName()));
        });
        return authorSet;
    }

    /**
     * ofNullable
     *
     * @param organisation
     * @param serviceURI
     * @return
     */
    public Set<String> getAllAuthors(String resType, IRI organisation, IRI publishers, String strAlfa, IRI srchAuthors, String serviceURI) {
        return getAllAuthors(resType, organisation, publishers, strAlfa, srchAuthors, DEFAULT_OFFSET, DEFAULT_LIMIT, serviceURI);
    }

    public Integer getAllAuthorCount(String resourceType, IRI organisation, IRI publishers, IRI authors, String strAlfa, String serviceURI) {
        StringBuilder sb = new StringBuilder(getPrefixImports());//select  (count(distinct ?record) as ?count)\n"
        sb.append("select (count(distinct ?author)as ?count)\n where \n{");
        Optional.ofNullable(organisation).ifPresent(org -> sb.append("?record :isInstanceOf ").append(org.toQuotedString()).append(".\n"));
        Optional.ofNullable(resourceType).ifPresent(resType -> sb.append(" ?org rdf:type nvli:").append(resType).append(".\n"));
        Optional.ofNullable(resourceType).ifPresent(resType -> sb.append(" ?record :isInstanceOf ?org.\n"));
        Optional.ofNullable(publishers).ifPresent(publisher -> sb.append("?record :hasPublisher ").append(publisher.toQuotedString()).append(".\n"));
        Optional.ofNullable(authors).ifPresent(author -> sb.append("?record :hasAuthor ").append(author.toQuotedString()).append(".\n"));
        sb.append("?record :hasAuthor ?author\n");
        if (strAlfa != null) {
            sb.append("FILTER regex(str(?author),\"http://www.cdac.in/technologies/ontology/").append(strAlfa).append("\",\"i\").\n");
        }
        sb.append("}");
        sb.append("order by ?author");
        System.out.println(sb);
        QueryExecution qe = QueryExecutionFactory.sparqlService(serviceURI, sb.toString());
        ResultSet resuiltSet = qe.execSelect();
        QuerySolution soln = resuiltSet.nextSolution();
        RDFNode x = soln.getLiteral("count");
        String cnt = x.toString();

        return Integer.parseInt(cnt.substring(0, x.toString().indexOf("^^")));
    }

    /**
     *
     * @param offset
     * @param limit
     * @return
     */
    private String getoffsetAndLimit(Optional<Integer> offset, Optional<Integer> limit) {
        StringBuilder sb = new StringBuilder();
        sb.append("\noffset ").append(Optional.ofNullable(offset).orElse(Optional.of(0)).get());
        sb.append("\nlimit ").append(Optional.ofNullable(limit).orElse(Optional.of(20)).get());
        return sb.toString();
    }

    private String getoffsetAndLimit(int offset, int limit) {
        StringBuilder sb = new StringBuilder();
        sb.append("\noffset ").append(offset);
        sb.append("\nlimit ").append(limit);
        return sb.toString();
    }

    /**
     * get the all authors for a resourceType
     *
     * @param serviceURI
     * @param resourceType
     * @param offset
     * @param limit
     * @return
     */
    public Set<String> getAuthorsForResourceType(String serviceURI, String resourceType, int offset, int limit) {
        StringBuilder sb = new StringBuilder(getPrefixImports());
        sb.append("select distinct ?author \n where \n{");
        sb.append(" ?org rdf:type nvli:").append(resourceType).append(".\n");
        sb.append("?record :isInstanceOf ?org.\n").append("  ?record :hasAuthor ?author.");
        sb.append("\n}\n");
        sb.append("order by ?author");
        sb.append(getoffsetAndLimit(offset, limit));
        System.out.println(sb.toString());
        QueryExecution qe = QueryExecutionFactory.sparqlService(serviceURI, sb.toString());
        ResultSet rs = qe.execSelect();
        Set<String> authorList = new HashSet<>();

        rs.forEachRemaining(result -> authorList.add(HcdcStringUtils.getUnderScoreToSpaceSeperated(result.getResource("author").getLocalName())));
        return authorList;
    }

    public Integer getCountOfAuthorsForResourceType(String serviceURI, String resourceType) {
        StringBuilder sb = new StringBuilder(getPrefixImports());
        sb.append("select (count(distinct ?author)as ?count)\n where \n{");
        sb.append(" ?org rdf:type nvli:").append(resourceType).append(".\n");
        sb.append("?record :isInstanceOf ?org.\n");
        sb.append("  ?record :hasAuthor ?author.");
        sb.append("\n}\n");
        sb.append("order by ?author");

        QueryExecution qe = QueryExecutionFactory.sparqlService(serviceURI, sb.toString());
        ResultSet resuiltSet = qe.execSelect();
        QuerySolution soln = resuiltSet.nextSolution();
        RDFNode x = soln.getLiteral("count");
        String cnt = x.toString();
        return Integer.parseInt(cnt.substring(0, x.toString().indexOf("^^")));
    }

    public Set<String> getPublishers(String serviceURI, String subStr, int length) {
        StringBuilder sb = new StringBuilder(getPrefixImports());
        sb.append("select distinct ?publisher\n where \n{");
        sb.append(" ?record :hasPublisher ?publisher .\n");
        if (subStr != null) {
            sb.append(" bind(strafter(str(?publisher),\"http://www.cdac.in/technologies/ontology/\") as ?temp)");
            sb.append("bind(lcase(?temp) as ?name).");
            sb.append("filter(contains(str(?name),\"" + subStr.replaceAll("\\s+", "_") + "\"))");
        }
        sb.append("\n}\n");
        sb.append("order by ?publisher");
        sb.append(getoffsetAndLimit(0, length));
        System.out.println(sb.toString());
        QueryExecution qe = QueryExecutionFactory.sparqlService(serviceURI, sb.toString());
        ResultSet rs = qe.execSelect();
        Set<String> authorList = new TreeSet<>();

        rs.forEachRemaining(
                result -> authorList.add(HcdcStringUtils.getUnderScoreToSpaceSeperated(result.getResource("publisher").getLocalName()))
        );
        return authorList;
    }

    public Set<String> getAuthorsForPublisher(String serviceURI, IRI publishers, int offset, int limit) {
        StringBuilder sb = new StringBuilder(getPrefixImports());
        sb.append("select distinct ?author \n where \n{");
        Optional.ofNullable(publishers).ifPresent(publisher -> sb.append("?record :hasPublisher ").append(publisher.toQuotedString()).append(".\n"));
        sb.append(" ?record :hasAuthor ?author.");
        sb.append("\n}\n");
        sb.append("order by ?author");
        sb.append(getoffsetAndLimit(offset, limit));
        System.out.println(sb.toString());
        QueryExecution qe = QueryExecutionFactory.sparqlService(serviceURI, sb.toString());
        ResultSet rs = qe.execSelect();
        Set<String> authorList = new HashSet<>();

        rs.forEachRemaining(result -> authorList.add(HcdcStringUtils.getUnderScoreToSpaceSeperated(result.getResource("author").getLocalName())));
        return authorList;
    }

    public Set<String> getSuggestions(String serviceURI, String subStr, int length) {
        StringBuilder sb = new StringBuilder(getPrefixImports());
        sb.append("select distinct ?author \n where \n{");
        sb.append("\n?record :hasAuthor ?author.\n");
        if (subStr != null) {
            sb.append(" bind(strafter(str(?author),\"http://www.cdac.in/technologies/ontology/\") as ?temp)");
            sb.append("bind(lcase(?temp) as ?name).");
            // sb.append("bind(lcase(strafter(str(?author),\"http://www.cdac.in/technologies/ontology/\")) as ?name).\n");
            // sb.append("filter(contains(str(?name),\"").append(subStr).append("\"))");
            sb.append("filter(contains(str(?name),\"" + subStr.replaceAll("\\s+", "_") + "\"))");
        }

        sb.append("\n}\n");
        sb.append("order by ?author");
        sb.append(getoffsetAndLimit(0, length));
        System.out.println(sb.toString());
        QueryExecution qe = QueryExecutionFactory.sparqlService(serviceURI, sb.toString());
        ResultSet rs = qe.execSelect();
        Set<String> authorList = new HashSet<>();

        rs.forEachRemaining(result -> authorList.add(HcdcStringUtils.getUnderScoreToSpaceSeperated(result.getResource("author").getLocalName())));
        return authorList;
    }

    public static void main(String[] args) {
        SparqlServiceFuseki fuseki = new SparqlServiceFuseki();

        //IRI org = IRI.create("7th_Convention_PLANNER_2010");
        String serviceURI = "http://10.208.26.13:3030/NVLI_FINAL";
        //System.out.println(getSuggestions(serviceURI, "suh++", 100));
        System.out.println(getCoAuthors(serviceURI, "AHMED GIASUDDIN", DEFAULT_OFFSET, DEFAULT_LIMIT));
        //System.out.println(getAuthorsForPublisher(serviceURI, org, 0, 20));
        //System.out.println(getPublishers(serviceURI));
        //System.out.println(getAllAuthors(null, null, null, "T", 1, 20, serviceURI));
        ////  Set<String> authors1 = fuseki.getAllAuthors(org, 0, 20, serviceURI);
        //  System.out.println(authors1);
        //  authors1 = fuseki.getAllAuthors(null, 20, 20, serviceURI);
        // System.out.println(authors1);
        //System.out.println(iri11.toQuotedString());
        //  List<IRI> authors = new ArrayList<>();
        //  authors.add(IRI.create("http://www.cdac.in/technologies/ontology/", "a"));
        // IRI iri = IRI.create("http://www.cdac.in/technologies/ontology/Mondal_S_K");
        // IRI iriOrg = IRI.create("http://www.cdac.in/technologies/ontology/OPRE14");
        //  getAllAuthors(null, 0, 10);
        // System.out.println(iri.getNamespace());
        //authors.add(iri);
        //serviceURI = "http://nvli-demo1:3030/NVLI_FINAL";
        //Map<String, String> records = getRecordsForAuthors(authors, IRI.create("http://www.cdac.in/technologies/ontology/OPRE14"), 0, 20, serviceURI);
        //System.out.println(records.toString());
        //  SparqlServiceFuseki fuseki = new SparqlServiceFuseki();
        //  String serviceURI = "http://localhost:3030/NVLI_Test";
        //  String refURI = "http://www.astro.umd.edu/~eshaya/astro-onto/owl/chemistry.owl";
        // System.out.println(fuseki.getRecordForClass(serviceURI, "ammonia", 1, 5, refURI));
        //  System.out.println(fuseki.getRecordCountForClass(serviceURI, "ammonia", refURI));
        //  System.out.println(fuseki.getCoAuthors(serviceURI, "Ghate Rucha", 0, 5));
    }

}
