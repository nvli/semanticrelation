/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.service;

import com.cdac.utils.OntologyReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Set;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLImportsDeclaration;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.RemoveImport;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author Gajraj
 */
@Service
public class JsonConversoinService {

    private static final Logger LOGGER = Logger.getLogger(OntologyService.class);

    @Value("${owl2vowl.libs}")
    private String owl2vowlDir;

    @Value("${json.directory}")
    private String jsonOutputDirectory;

    @Value("${temp.directory}")
    private String tempDir;

    public JsonConversoinService() {
        //  owl2vowlDir = ".nvli_semantic_relations\\owl2vowl";
        //  jsonOutputDirectory = ".nvli_semantic_relations\\json";
        //   tempDir = ".nvli_semantic_relations/temp";
    }

    /**
     * remove the import statements from the ontology file
     *
     * @param owlFile
     * @throws OWLOntologyCreationException
     * @throws OWLOntologyStorageException
     */
    public void removeImportFromOntology(File owlFile) throws OWLOntologyCreationException, OWLOntologyStorageException {
        OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = manager.loadOntologyFromOntologyDocument(owlFile);
        Set<OWLOntology> importClosure = ontology.getImportsClosure();
        importClosure.forEach(owl -> System.out.println(owl.getOntologyID()));
        Set<OWLImportsDeclaration> importDeclaretions = ontology.getImportsDeclarations();
        importDeclaretions.forEach(oi -> manager.applyChange(new RemoveImport(ontology, oi)));
        manager.saveOntology(ontology);
    }

    /**
     * generate required json file for owl viewer vowl
     *
     * @param owlFile
     * @param outputJsonName
     * @throws IOException
     * @throws InterruptedException
     */
    public void generateJsonForOWL(final File owlFile, String outputJsonName) throws IOException, InterruptedException {
        String userhome = System.getProperty("user.home");
        String process = "java -jar " + userhome + File.separator + owl2vowlDir
                + File.separator + "owl2vowl.jar -file " + owlFile.getAbsolutePath() + " -output "
                + userhome + File.separator + jsonOutputDirectory + File.separator + outputJsonName + ".json";
        System.out.println(process);
        Process p = Runtime.getRuntime().exec(process);
        p.waitFor();
        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
        reader.lines().forEach(s -> System.out.println(s));
    }

    /**
     * first create the temporary file by copying the input file to temp
     * directory. and then generate the json
     *
     * @param owlFile
     * @param outputJsonName
     * @throws IOException
     * @throws OWLOntologyCreationException
     * @throws InterruptedException
     * @throws OWLOntologyStorageException
     */
    //@Async
    public void createTempAndGenerateJson(File owlFile, String outputJsonName) {
        try {
            File tempFile = new File(System.getProperty("user.home") + File.separator + tempDir + File.separator + owlFile.getName());
            FileUtils.copyFile(owlFile, tempFile);

            //to remove imports
            removeImportFromOntology(tempFile);

            // to generate and make first latter to Uppercase.
            new OntologyReader(tempFile).generatelables();
            generateJsonForOWL(tempFile, outputJsonName);
            if (!tempFile.delete()) {
                LOGGER.error("Error in deleteing file " + tempFile.getAbsolutePath());
            }
        } catch (IOException | OWLOntologyCreationException | InterruptedException | OWLOntologyStorageException ex) {
            LOGGER.error(ex.getClass() + ex.getMessage());
        }

    }

    public static void main(String[] args) throws IOException, OWLOntologyCreationException, InterruptedException, OWLOntologyCreationException, OWLOntologyStorageException {
        JsonConversoinService conversoinService = new JsonConversoinService();
        File file = new File("E:\\ontology\\p1.xml");
        conversoinService.createTempAndGenerateJson(file, "test");
        // conversoinService.removeImportFromOntology(file);
        // conversoinService.generateJsonForOWL(file, "test");

    }

}
