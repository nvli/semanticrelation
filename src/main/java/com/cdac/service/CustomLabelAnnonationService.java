/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.service;

import com.cdac.beans.QueryParameter;
import com.cdac.beans.Record;
import com.cdac.entity.UserCustomBookmarkedRecord;
import com.cdac.entity.UserDefineClassLabel;
import com.cdac.entity.dao.IUserCustomBookmarkedRecordDao;
import com.cdac.entity.dao.IUserDefineClassLabelDao;
import com.cdac.form.UserBookmarkDto;
import com.cdac.form.UserDefinedCustomLabelDto;
import com.cdac.utils.Helper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * TODO use model mapper for convert from object to dto and viceversa
 *
 * @author Gajraj
 */
@Service
public class CustomLabelAnnonationService {

    @Autowired
    private IUserDefineClassLabelDao userDefinedClassLabelDao;
    @Autowired
    private IUserCustomBookmarkedRecordDao userCustomBookmarkedRecordDao;

    private final Logger LOGGER = Logger.getLogger(CustomLabelAnnonationService.class);

    //private final String METADATAINTEGRATOR_WEBSERVICE = "http://10.208.26.200/MetadataIntegratorWebService/webservice/";
    @Value("${nvli.metedataservice.URL}")
    String METADATAINTEGRATOR_WEBSERVICE;
    @Value("${nvli.metadata.integrater.user}")
    String metedataIntegraterUser;
    @Value("${nvli.metadata.integrater.password}")
    String metedataIntegraterPassword;

    /**
     *
     * @return
     */
    public List<UserDefinedCustomLabelDto> getAllUDCL() {
        return userDefinedClassLabelDao.listDto();
    }

    public List<UserDefinedCustomLabelDto> getLabelsForRefOntId(long refOntId) {
        return userDefinedClassLabelDao.getAllLabelDtoForClass(refOntId);
    }

    public void AddCustomeLabelForClassId(UserDefinedCustomLabelDto udclDto) {
        userDefinedClassLabelDao.AddNewLabel(udclDto);
    }

    public void tagRecordWithCustomAnnonation(UserBookmarkDto ubd) {
        userCustomBookmarkedRecordDao.addBookmark(ubd);
    }

    public List<UserDefinedCustomLabelDto> getCustomTagForUsers(long userId) {
        return userDefinedClassLabelDao.getAllCustomLabelDtoForUser(userId);
    }

    public List<UserDefinedCustomLabelDto> getCustomTagForUsers(long userId, long domOntId, String classname) {
        return userDefinedClassLabelDao.getAllLabelDtoForClassByUser(domOntId, classname, userId);
    }

    public List<UserDefinedCustomLabelDto> getCustomTagForRefClassByUser(long refClassId, long userId) {
        return userDefinedClassLabelDao.getAllLabelDtoForClassByUser(refClassId, userId);
    }

    public List<UserDefinedCustomLabelDto> getCustomLabelForRecord(String recordId) {
        List<UserCustomBookmarkedRecord> blist = userCustomBookmarkedRecordDao.getCustomeLablesForRecord(recordId);
        List<UserDefineClassLabel> udclList = new ArrayList<>();
        blist.forEach(x -> udclList.add(userDefinedClassLabelDao.get(x.getCustomClassId())));
        return udclList.stream().map(UserDefineClassLabel::convertToDto).collect(Collectors.toList());
    }

    public List<UserCustomBookmarkedRecord> getBookmarks() {
        return userCustomBookmarkedRecordDao.list();
    }

    public List<UserCustomBookmarkedRecord> getBookmarsForUser(long userId) {
        return userCustomBookmarkedRecordDao.getAllCustomLabeledRecordForUser(userId);
    }

    public List<UserCustomBookmarkedRecord> getBookmarsForRecord(String recordIdentifier) {
        return userCustomBookmarkedRecordDao.getCustomeLablesForRecord(recordIdentifier);
    }

    public List<UserCustomBookmarkedRecord> getBookmarsForRecordByUser(String recordIdentifier, long userId) {
        return userCustomBookmarkedRecordDao.getBookmarksForRecordByUser(recordIdentifier, userId);
    }

    public void addCustomeLabel(UserDefineClassLabel udcl) {
        userDefinedClassLabelDao.saveOrUpdate(udcl);
    }

    public boolean addCustomeLabel(UserDefinedCustomLabelDto udclDto) {
        if (!userDefinedClassLabelDao.isExist(udclDto)) {
            return userDefinedClassLabelDao.AddNewLabel(udclDto);
        }
        return false;
    }

    /**
     * add given record with given custom labels
     *
     * @param ubd
     */
    public void addBookmark(UserBookmarkDto ubd) {
        userCustomBookmarkedRecordDao.addBookmark(ubd);
    }

    public List<UserCustomBookmarkedRecord> getRecordForCutomClassIdAndUser(Long userId, List<Long> lstCustLblIds, int pageNo, int limit) {
        return userCustomBookmarkedRecordDao.getRecordForCutomClassIdAndUser(userId, lstCustLblIds, pageNo, limit);
    }

    public Long getTaggedRecordCountForCustLbl(Long userId, List<Long> lstCustLblIds) {
        return userCustomBookmarkedRecordDao.getTaggedRecordCountForCustLbl(userId, lstCustLblIds);
    }

    public Set<String> getMyTaggedClassList(Long userID, Long domainId) {
        Set<String> myTaggedClasses = new HashSet();
        userDefinedClassLabelDao.getAllCustomLabelForUser(userID, domainId).forEach(userDefineClassLabel -> {
            if (userDefineClassLabel.getOntDomainClass() != null) {
                myTaggedClasses.add(userDefineClassLabel.getOntDomainClass().getClassname());
            }
        });
        return myTaggedClasses;
    }

    public boolean deleteCustomLbl(Long userId, Long customLblId) {
        //delete from UserCustomBookmarkedRecord class.
        userCustomBookmarkedRecordDao.getRecordForCutomClassIdAndUser(userId, customLblId).forEach(
                userCustomBookmarkedRecord -> userCustomBookmarkedRecordDao.delete(userCustomBookmarkedRecord)
        );

        //delete from UserDefineClassLabel class.
        UserDefineClassLabel classLabel = new UserDefineClassLabel();
        classLabel.setId(customLblId);
        userDefinedClassLabelDao.delete(classLabel);
        return true;
    }

    public List<Record> getRecordsRecordID(QueryParameter parameter) {
        List<Record> records = null;
        HttpHeaders headers = Helper.getHeader(metedataIntegraterUser, metedataIntegraterPassword);
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity httpEntity = new HttpEntity(parameter, headers);
        String url = METADATAINTEGRATOR_WEBSERVICE + "request/recordinfo";
        ObjectMapper mapper = new ObjectMapper();
        records = restTemplate.postForObject(url, httpEntity, List.class);
        try {
            return mapper.readValue(mapper.writeValueAsString(records), new TypeReference<List<Record>>() {
            });
        } catch (IOException ex) {
            LOGGER.error(ex);
        }
        return records;
    }
}
