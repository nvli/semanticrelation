/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.model;

/**
 *
 * @author Gajraj
 */
public class WordWeight {

    private String word;
    private long strength;// lowest value means highest strenth

    public WordWeight() {
    }

    public WordWeight(String word, int strength) {
        this.word = word;
        this.strength = strength;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public long getStrength() {
        return strength;
    }

    public void setStrength(long strength) {
        this.strength = strength;
    }

    @Override
    public String toString() {
        return "DomainWeightOfClasses{" + "word=" + word + ", strength=" + strength + '}';
    }

}
