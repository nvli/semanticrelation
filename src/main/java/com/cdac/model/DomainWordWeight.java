/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.model;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

/**
 *
 * @author Gajraj
 */
public class DomainWordWeight {

    private int ontologyID;
    private Map<String, Long> wordWeightMap;

    public DomainWordWeight() {
    }

    public DomainWordWeight(int ontologyID, Map wordWeightMap) {
        this.ontologyID = ontologyID;
        this.wordWeightMap = wordWeightMap;
    }

    public int getOntologyID() {
        return ontologyID;
    }

    public void setOntologyID(int ontologyID) {
        this.ontologyID = ontologyID;
    }

    public Map<String, Long> getwordWeightMap() {
        return wordWeightMap;
    }

    public void setwordWeightMap(Map<String, Long> wordWeightMap) {
        this.wordWeightMap = wordWeightMap;
    }

    @Override
    public String toString() {
        return "DomainWordWeight{" + "ontologyID=" + ontologyID + ", wordWeightMap=" + wordWeightMap + '}';
    }

    public static void main(String[] args) {
        StandardDeviation sd = new StandardDeviation();
        double[] values = {1, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 14, 12, 1, 2, 2, 2, 2, 2};
        System.out.println(sd.evaluate(values));
        String s = "hi this is this is hi or bhai";
        List<String> list = Arrays.asList(s.split(" "));
        Map<String, Long> collect = list.stream().collect(groupingBy(Function.identity(), counting()));
        System.out.println(collect);

    }

}
