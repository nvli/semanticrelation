/**
 *
 * Copyright 2015 HCDC, Human Centered Design Computing
 * Licensed. you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.cdac.model;

import java.util.List;

/**
 * @author Gajraj Tanwar
 *
 */
public class RecordOAI {

    /**
     *
     */
    private List<String> title;

    private List<String> creator;

    private List<String> publisher;

    private List<String> date;

    private List<String> type;

    private List<String> identifier;

    private List<String> subject;

    private List<String> desceiption;

    private List<String> contributor;

    private List<String> format;

    private List<String> source;

    private List<String> language;

    private List<String> relation;

    private List<String> coverage;

    private List<String> rights;

    /**
     *
     */
    public RecordOAI() {
        super();
    }

    /**
     * @return the title
     */
    public List<String> getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(List<String> title) {
        this.title = title;
    }

    /**
     * @return the creater
     */
    public List<String> getCreater() {
        return creator;
    }

    /**
     * @param creater the creater to set
     */
    public void setCreater(List<String> creater) {
        this.creator = creater;
    }

    /**
     * @return the publisher
     */
    public List<String> getPublisher() {
        return publisher;
    }

    /**
     * @param publisher the publisher to set
     */
    public void setPublisher(List<String> publisher) {
        this.publisher = publisher;
    }

    /**
     * @return the date
     */
    public List<String> getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(List<String> date) {
        this.date = date;
    }

    /**
     * @return the type
     */
    public List<String> getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(List<String> type) {
        this.type = type;
    }

    /**
     * @return the identifier
     */
    public List<String> getIdentifier() {
        return identifier;
    }

    /**
     * @param identifier the identifier to set
     */
    public void setIdentifier(List<String> identifier) {
        this.identifier = identifier;
    }

    /**
     * @return the subject
     */
    public List<String> getSubject() {
        return subject;
    }

    /**
     * @param subject the subject to set
     */
    public void setSubject(List<String> subject) {
        this.subject = subject;
    }

    /**
     * @return the desceiption
     */
    public List<String> getDesceiption() {
        return desceiption;
    }

    /**
     * @param desceiption the desceiption to set
     */
    public void setDesceiption(List<String> desceiption) {
        this.desceiption = desceiption;
    }

    /**
     * @return the contributor
     */
    public List<String> getContributor() {
        return contributor;
    }

    /**
     * @param contributor the contributor to set
     */
    public void setContributor(List<String> contributor) {
        this.contributor = contributor;
    }

    /**
     * @return the format
     */
    public List<String> getFormat() {
        return format;
    }

    /**
     * @param format the format to set
     */
    public void setFormat(List<String> format) {
        this.format = format;
    }

    /**
     * @return the source
     */
    public List<String> getSource() {
        return source;
    }

    /**
     * @param source the source to set
     */
    public void setSource(List<String> source) {
        this.source = source;
    }

    /**
     * @return the language
     */
    public List<String> getLanguage() {
        return language;
    }

    /**
     * @param language the language to set
     */
    public void setLanguage(List<String> language) {
        this.language = language;
    }

    /**
     * @return the relation
     */
    public List<String> getRelation() {
        return relation;
    }

    /**
     * @param relation the relation to set
     */
    public void setRelation(List<String> relation) {
        this.relation = relation;
    }

    /**
     * @return the coverage
     */
    public List<String> getCoverage() {
        return coverage;
    }

    /**
     * @param coverage the coverage to set
     */
    public void setCoverage(List<String> coverage) {
        this.coverage = coverage;
    }

    /**
     * @return the rights
     */
    public List<String> getRights() {
        return rights;
    }

    /**
     * @param rights the rights to set
     */
    public void setRights(List<String> rights) {
        this.rights = rights;
    }

    /*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "RecorOAI [title=" + title + ", creater=" + creator
                + ", publisher=" + publisher + ", date=" + date + ", type="
                + type + ", identifier=" + identifier + ", subject=" + subject
                + ", desceiption=" + desceiption + ", contributor="
                + contributor + ", format=" + format + ", source=" + source
                + ", language=" + language + ", relation=" + relation
                + ", coverage=" + coverage + ", rights=" + rights + "]";
    }

    /**
     *
     * @return
     */
    public String getRecordName() {
        if (!title.isEmpty()) {
            return title.get(0);
        }
        if (!subject.isEmpty()) {
            return subject.get(0);
        }
        if (!publisher.isEmpty()) {
            return publisher.get(0);
        }
        return "Undefined No name in the metadata";
    }

    public static void main(String[] args) {
        String s = "Dr.Babu, C A&|&Hamza, Varikoden&|&Samah, A A";
        System.out.println(s.split("&|&").length);
        System.out.println(s.split("&\\|&").length);
    }
}
