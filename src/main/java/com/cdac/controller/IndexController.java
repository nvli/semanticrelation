package com.cdac.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Base controller
 *
 * @author Gajraj Tanwar
 * @since 1
 * @version 1
 */
@Controller
public class IndexController {

    /**
     * Handles request identified by "/"
     *
     * @return ModelAndView and view is index.
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView getIndex() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        return mav;
    }

    /**
     * Handles request identified by "/home"
     *
     * @return ModelAndView and view is home.
     */
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView getHome() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("home");
        return mav;
    }
}
