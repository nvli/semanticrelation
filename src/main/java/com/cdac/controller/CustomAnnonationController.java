/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.controller;

import com.cdac.beans.QueryParameter;
import com.cdac.entity.UserCustomBookmarkedRecord;
import com.cdac.form.MultipleBookmarkForm;
import com.cdac.form.UserBookmarkDto;
import com.cdac.form.UserDefinedCustomLabelDto;
import com.cdac.service.CustomLabelAnnonationService;
import com.cdac.service.OntologyService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Gajraj
 */
@RestController
@RequestMapping("/custom")
public class CustomAnnonationController {

    @Autowired
    private OntologyService ontologyService;

    @Autowired
    private CustomLabelAnnonationService annonationService;

    /**
     * get all custom labels
     *
     * @return
     */
    @RequestMapping(value = "/labels", method = RequestMethod.GET)
    public ResponseEntity<List<UserDefinedCustomLabelDto>> getAllCustomLabels() {
        List<UserDefinedCustomLabelDto> list = annonationService.getAllUDCL();
        return new ResponseEntity<>(list, HttpStatus.OK);

    }

    /**
     * get all custom labels
     *
     * @param udcl
     * @return
     */
    //@ResponseBody
    @RequestMapping(value = "/labels", method = RequestMethod.POST)
    public ResponseEntity<List<UserDefinedCustomLabelDto>> getAllCustomLabels(@RequestBody List<UserDefinedCustomLabelDto> udcl) {
        udcl.forEach(annonationService::addCustomeLabel);
        List<UserDefinedCustomLabelDto> list = annonationService.getCustomTagForUsers(udcl.get(0).getUserId(), udcl.get(0).getOntologyFileId(), udcl.get(0).getClassName());
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    /**
     * get all custom labels for given userid
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/labels/users1/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<UserDefinedCustomLabelDto>> getAllCustomLabelsByUser(@PathVariable(value = "userId") long userId) {
        List<UserDefinedCustomLabelDto> list = annonationService.getCustomTagForUsers(userId);
        return new ResponseEntity<>(list, HttpStatus.OK);

    }

    /**
     * get all custom labels for given userid
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/labels/users/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<UserDefinedCustomLabelDto>> getAllCustomLabelsByUserForRefOnt(@PathVariable(value = "userId") long userId, @RequestParam long domOntId, @RequestParam String classname) {
        List<UserDefinedCustomLabelDto> list = annonationService.getCustomTagForUsers(userId, domOntId, classname);
        return new ResponseEntity<>(list, HttpStatus.OK);

    }

    /**
     * get the custom label for the ref ontology class by all users
     *
     * @param refOntClassId
     * @return
     */
    @RequestMapping(value = "/labels/{refOntClassId}", method = RequestMethod.GET)
    public ResponseEntity<List<UserDefinedCustomLabelDto>> getCustomLabelForRefClass(@PathVariable(value = "refOntClassId") long refOntClassId) {
        List<UserDefinedCustomLabelDto> udclList = annonationService.getLabelsForRefOntId(refOntClassId);
        return new ResponseEntity<>(udclList, HttpStatus.OK);
    }

    /**
     * get the custom label for the ref ontology class by given users
     *
     * @param refOntClassId
     * @param userId
     * @return
     */
    @RequestMapping(value = "/labels/{refOntClassId}/{userId}", method = RequestMethod.GET)
    public ResponseEntity<List<UserDefinedCustomLabelDto>> getCustomLabelForRefClassByUser(@PathVariable(value = "refOntClassId") long refOntClassId, @PathVariable(value = "userId") long userId) {
        List<UserDefinedCustomLabelDto> udclList = annonationService.getCustomTagForRefClassByUser(refOntClassId, userId);
        return new ResponseEntity<>(udclList, HttpStatus.OK);

    }

    /**
     * get all bookmarks
     *
     * @return
     */
    @RequestMapping(value = "/bookmarks")
    public ResponseEntity<List<UserCustomBookmarkedRecord>> getAllBookmarks() {
        List<UserCustomBookmarkedRecord> blist = annonationService.getBookmarks();
        return new ResponseEntity<>(blist, HttpStatus.OK);
    }

    /**
     * get all bookmarks for this record
     *
     * @param recordIdentifier
     * @return
     */
    @RequestMapping(value = "/bookmarks/{recordIdentifier}")
    public ResponseEntity<List<UserCustomBookmarkedRecord>> getBookmarksForRecord(@PathVariable(value = "recordIdentifier") String recordIdentifier) {
        List<UserCustomBookmarkedRecord> userBookmarks = annonationService.getBookmarsForRecord(recordIdentifier);
        return new ResponseEntity<>(userBookmarks, HttpStatus.OK);
    }

    /**
     * get all bookmarks for the user
     *
     * @param userId
     * @return
     */
    @RequestMapping(value = "/bookmarks/users/{userId}")
    public ResponseEntity<List<UserCustomBookmarkedRecord>> getBookmarksByUser(@PathVariable(value = "userId") long userId) {

        List<UserCustomBookmarkedRecord> blist = annonationService.getBookmarsForUser(userId);
        return new ResponseEntity<>(blist, HttpStatus.OK);
    }

    /**
     * get bookmark for this record by this user
     *
     * @param recordIdentifier
     * @param userId
     * @return
     */
    @RequestMapping(value = "/bookmarks/{recordIdentifier}/{userId}")
    public ResponseEntity<List<UserCustomBookmarkedRecord>> getBookmarksForRecordByUser(@PathVariable(value = "recordIdentifier") String recordIdentifier, @PathVariable(value = "userId") long userId) {
        List<UserCustomBookmarkedRecord> userBookmark = annonationService.getBookmarsForRecordByUser(recordIdentifier, userId);
        return new ResponseEntity<>(userBookmark, HttpStatus.OK);
    }

    /**
     * add new custom label for a ref ontology class
     *
     * @param udclDto
     */
    @RequestMapping(value = "/labels1", method = {RequestMethod.POST})
    public void addCustomLabel(@RequestBody UserDefinedCustomLabelDto udclDto) {
        //@RequestBody UserDefinedCustomLabelDto udclDto
        System.out.println(udclDto.toString());
        annonationService.addCustomeLabel(udclDto);
    }

    /**
     * add new custom label for a ref ontology class
     *
     * @param ubd
     */
    @RequestMapping(value = "/bookmarks", method = {RequestMethod.POST})
    public void addBookmark(@RequestBody UserBookmarkDto ubd) {
        //@RequestBody UserDefinedCustomLabelDto udclDto
        System.out.println(ubd.toString());
        annonationService.addBookmark(ubd);
    }

    /**
     * add new custom label for a ref ontology class.
     *
     * @param mbf
     * @return
     */
    @RequestMapping(value = "/bookmarks/multi", method = {RequestMethod.POST})
    public ResponseEntity addBookmark(@RequestBody MultipleBookmarkForm mbf) {
        mbf.getCustomLabelId().forEach(customLableId -> {
            mbf.getRecordIdentifier().forEach(recordIdentifier -> {
                annonationService.addBookmark(new UserBookmarkDto(mbf.getUserId(), customLableId, recordIdentifier));
            });
        });
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * returns number of record count for selected custom label and for logged
     * in user.
     *
     * @param userID
     * @param lblIds
     * @return
     */
    @RequestMapping(value = "/getTaggedRecordCount", method = RequestMethod.POST)
    public Long getTaggedRecordCountForCustLbl(@RequestParam Long userID, @RequestBody List<Long> lblIds) {
        //int offset = (pageNum - 1) * pageWindowSize;
        return annonationService.getTaggedRecordCountForCustLbl(userID, lblIds);
    }

    /**
     * returns records for selected custom label(tag) for logged in user.
     *
     *
     * @param pageNum
     * @param pageWindowSize
     * @param userID
     * @param custLblIds
     * @return
     */
    @RequestMapping(value = "/getTaggedRecords/{pageNum}/{pageWin}", method = RequestMethod.POST)
    public Map<String, String> getRecordsForTaggedLbl(@PathVariable("pageNum") int pageNum, @PathVariable("pageWin") int pageWindowSize,
            @RequestParam Long userID, @RequestBody List<Long> custLblIds) {
        Map<String, String> records = new HashMap();
        QueryParameter parameter = new QueryParameter();

        List<String> listDC = new ArrayList<>();
        listDC.add("title");
        List<String> recordID = new ArrayList<>();
        annonationService.getRecordForCutomClassIdAndUser(userID, custLblIds, pageNum, pageWindowSize).forEach(userBookmarRecord -> recordID.add(userBookmarRecord.getRecordIdentifier()));

        parameter.setRecordIdentifiers(recordID);
        parameter.setDcElements(listDC);
        annonationService.getRecordsRecordID(parameter).forEach(record -> {
            records.put(record.getRecordIdentifier(), record.getTitle().get(0));
        });
        return records;
    }

    /**
     * returns list of custom labels for logged in user.
     *
     * @param userId
     * @param domainId
     * @return
     */
    @RequestMapping(value = "/myTaggedClassList/{userId}/{domainId}", method = RequestMethod.GET)
    public Set<String> getMyTaggedClassList(@PathVariable("userId") Long userId, @PathVariable("domainId") Long domainId) {
        return annonationService.getMyTaggedClassList(userId, domainId);
    }

    /**
     *
     * to delete logged users Custom Lbl
     *
     * @param userId
     * @param tagID
     * @return
     */
    @RequestMapping(value = "/deleteMyCustomTag/{userId}/{domainId}", method = RequestMethod.DELETE)
    public boolean deleteMyCustomTag(@PathVariable Long userId, @PathVariable Long domainId) {
        return annonationService.deleteCustomLbl(userId, domainId);
    }
}
