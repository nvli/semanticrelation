package com.cdac.controller;

import com.cdac.entity.DomainOntology;
import com.cdac.service.DummuyOrganizationService;
import com.cdac.service.SparqlServiceFuseki;
import com.cdac.service.VisualizeService;
import com.cdac.utils.HcdcStringUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.log4j.Logger;
import org.semanticweb.owlapi.model.IRI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Handles all requests identified by '/sparql'. Controller act as rest
 * endpoint.
 *
 * @author Gajraj
 * @since 1
 * @version 1
 */
@RestController
@RequestMapping("/sparql")
public class SemanticController {

    /**
     * Logger Reference
     */
    private final Logger LOGGER = Logger.getLogger(SemanticController.class);
    /**
     * URI of Apache jena fuseki service. It value will populated from Property
     * file. (@see WEB-INF/etc/conf/dispatcher-servlet.xml)
     */
    @Value("${fuseki.serviceURI}")
    private String FUSEKI_SERVICE_URI;
    /**
     * SparqlServiceFuseki Reference
     */
    @Autowired
    private SparqlServiceFuseki sparqlServiceFuseki;
    /**
     * VisualizeService reference.
     */
    @Autowired
    private VisualizeService visualizeService;

    /**
     * DummuyOrganizationService Reference
     */
    @Autowired
    private DummuyOrganizationService dummyOrgService;

    /**
     *
     * @param pageNum
     * @param pageWindowSize
     * @param classIRI
     * @param refOntId
     * @return
     */
    @RequestMapping(value = "/getRecords/{pageNum}/{pageWin}", method = RequestMethod.GET)
    public Map<String, String> getRecordsForClass(@PathVariable("pageNum") int pageNum, @PathVariable("pageWin") int pageWindowSize, @RequestParam("classIRI") String classIRI, @RequestParam("refOntId") Long refOntId) {
        if (refOntId == 1 || refOntId == 3) {
            classIRI = classIRI.replaceAll("\\s", "_");
        }
        System.out.println("Offset " + pageNum);
        System.out.println("ClassIRI " + classIRI);
        Map<String, String> records = sparqlServiceFuseki.getRecordForClass(FUSEKI_SERVICE_URI, classIRI, (pageWindowSize * pageNum) - pageWindowSize, pageWindowSize, visualizeService.getClassBasedOnJsonIdAndDomainOntID(refOntId, classIRI).getQualifiedClassName(), "#");
        if (records.isEmpty()) {
            return sparqlServiceFuseki.getRecordForClass(FUSEKI_SERVICE_URI, classIRI, (pageWindowSize * pageNum) - pageWindowSize, pageWindowSize, visualizeService.getClassBasedOnJsonIdAndDomainOntID(refOntId, classIRI).getQualifiedClassName(), "/");
        } else {
            return records;
        }
    }

    /**
     *
     * @param classIRI
     * @param refOntId
     * @return
     *
     * Returns count of records for given @param classIRI and @param refOntId.
     */
    @RequestMapping(value = "/getClassRecordCount", method = RequestMethod.GET)
    public long getRecordCountForClass(@RequestParam("classIRI") String classIRI, @RequestParam("refOntId") Long refOntId) {
        //int recordCount = sparqlServiceFuseki.getRecordCountForClass(FUSEKI_SERVICE_URI, classIRI, visualizeService.getDomainOntologyById(refOntId).getNamespaceURI(), "#");
        //if (recordCount == 0) {
        //    return sparqlServiceFuseki.getRecordCountForClass(FUSEKI_SERVICE_URI, classIRI, visualizeService.getDomainOntologyById(refOntId).getNamespaceURI(), "/");
        //}
        //System.out.println(">>>>>>>>>>>>>>>>>>> " + refOntId);
        return visualizeService.getRecordCountForClass(classIRI, refOntId);
        //return recordCount;
    }

    /**
     * find the authors of a particular record
     *
     * @param recordIdentifier
     * @return
     */
    @RequestMapping(value = "/getAuthors/{recordIdentifier}", method = RequestMethod.GET)
    public Set<String> getAuthorsForRecord(@PathVariable("recordIdentifier") String recordIdentifier) {
        return sparqlServiceFuseki.getAuthorsForRecord(FUSEKI_SERVICE_URI, recordIdentifier);
    }

    /**
     * get authors respect to organization
     *
     * @param resType
     * @param organization
     * @param publisher
     * @param organisation
     * @param pageNumber
     * @param pageWin
     * @return
     */
    @RequestMapping(value = "/getAllAuthors/{pageNumber}/{pageWin}", method = RequestMethod.GET)
    public Set<String> getAllAuthors(@RequestParam String resType, @RequestParam String organization, @RequestParam String publisher, @RequestParam String strAlfa, @RequestParam String srchAuthor, @PathVariable("pageNumber") Integer pageNumber, @PathVariable("pageWin") Integer pageWin) {
        return sparqlServiceFuseki.getAllAuthors(resType.equalsIgnoreCase("all") ? null : HcdcStringUtils.getSpaceSeparetedToUnderScore(resType),
                organization.equalsIgnoreCase("all") ? null : IRI.create(HcdcStringUtils.getSpaceSeparetedToUnderScore(organization)),
                publisher.equalsIgnoreCase("all") ? null : IRI.create(HcdcStringUtils.getSpaceSeparetedToUnderScore(publisher)),
                strAlfa.equalsIgnoreCase("all") ? null : HcdcStringUtils.getSpaceSeparetedToUnderScore(strAlfa),
                srchAuthor.equalsIgnoreCase("all") ? null : IRI.create(HcdcStringUtils.getSpaceSeparetedToUnderScore(srchAuthor)),
                pageNumber, pageWin, FUSEKI_SERVICE_URI);
    }

    /**
     * find the count of authors
     *
     * @param resType
     * @param resourceType
     * @param publisher
     * @param author
     * @param organisation
     * @param strAlfa
     * @param organization
     * @param pageNumber
     * @param pageWin
     * @return
     */
    @RequestMapping(value = "/getAllAuthorCount", method = RequestMethod.GET)
    public Integer getAllAuthorsCount(@RequestParam String resType, @RequestParam String organization, @RequestParam String publisher, @RequestParam String author, @RequestParam String strAlfa) {
        return sparqlServiceFuseki.getAllAuthorCount(resType.equalsIgnoreCase("all") ? null : HcdcStringUtils.getSpaceSeparetedToUnderScore(resType),
                organization.equalsIgnoreCase("all") ? null : IRI.create(HcdcStringUtils.getSpaceSeparetedToUnderScore(organization)),
                publisher.equalsIgnoreCase("all") ? null : IRI.create(HcdcStringUtils.getSpaceSeparetedToUnderScore(publisher)),
                author.equalsIgnoreCase("all") ? null : IRI.create(HcdcStringUtils.getSpaceSeparetedToUnderScore(author)),
                strAlfa.equalsIgnoreCase("all") ? null : HcdcStringUtils.getSpaceSeparetedToUnderScore(strAlfa), FUSEKI_SERVICE_URI);
    }

    /**
     * Handles request identified by
     * <u>/getCoAuthors/{pageNumber}/{windowSize}</u>
     * find the Co-authors as well as record for which given author has worked
     * in collaboration
     *
     * @param authorIRI author name.
     * @param pageNumber page number for pagination.
     * @param windowSize window size for pagination.
     * @return Map have key as String and value as Set of String
     */
    @RequestMapping(value = "/getCoAuthors/{pageNumber}/{windowSize}", method = RequestMethod.GET)
    public List<String> getCoAuthors(@RequestParam("authorIRI") String authorIRI, @PathVariable int pageNumber, @PathVariable int windowSize) {
        //int offset = (pageNumber - 1) * windowSize;
        return sparqlServiceFuseki.getCoAuthors(FUSEKI_SERVICE_URI, authorIRI, pageNumber, windowSize);
    }

    @RequestMapping(value = "/getCountOfRecordForAuthor", method = RequestMethod.GET)
    public int getCountOfRecordsForAuthors(@RequestParam ArrayList<String> authorIRI, @RequestParam("organizationIRI") String organizationIRI) {
        List<IRI> listIRI = new ArrayList<>();
        authorIRI.forEach(author -> listIRI.add(IRI.create(HcdcStringUtils.getSpaceSeparetedToUnderScore(author))));
        listIRI.remove(0);
        return sparqlServiceFuseki.getCountOfRecordsForAuthors(listIRI, null, FUSEKI_SERVICE_URI);
    }

    @RequestMapping(value = "/getRecordsForAuthor/{pageNumber}/{windowSize}", method = RequestMethod.GET)
    public Map<String, String> getRecordsForAuthors(@RequestParam ArrayList<String> authorIRI, @PathVariable int pageNumber, @PathVariable int windowSize) {
        //int offset = (pageNumber - 1) * windowSize;
        List<IRI> listIRI = new ArrayList<>();
        authorIRI.forEach(author -> listIRI.add(IRI.create(HcdcStringUtils.getSpaceSeparetedToUnderScore(author))));
        listIRI.remove(0);
        return sparqlServiceFuseki.getRecordsForAuthors(listIRI, null, pageNumber, windowSize, FUSEKI_SERVICE_URI);
    }

    /**
     * Handles requests identified by <u>/getjsonforDomainOnts</u>
     * Returns the list of all Domain Ontology for listing on main page.
     *
     * @return
     */
    @RequestMapping(value = "/getDomainOnts", method = RequestMethod.GET)
    @ResponseBody
    public List<DomainOntology> getDomainOnts() {
        return visualizeService.getDomainOntologies();
    }

    @RequestMapping(value = "/getResourceType", method = RequestMethod.GET)
    @ResponseBody
    public Set<String> getResourceType() {
        Set<String> resourceType = new HashSet<>();
        resourceType = dummyOrgService.getResourcType();
        return resourceType;
    }

    @ResponseBody
    @RequestMapping(value = "/getOrgForResourceType/{resType}", method = {RequestMethod.GET, RequestMethod.POST})
    public List<DummuyOrganizationService.Organization> getOrgList(@PathVariable String resType) {
        return dummyOrgService.getOrgList(resType);
    }

    @RequestMapping(value = "/getPublishers/{length}", method = RequestMethod.GET)
    @ResponseBody
    public Set<String> getPublishers(@RequestParam String subStr, @PathVariable int length) {
        return sparqlServiceFuseki.getPublishers(FUSEKI_SERVICE_URI, subStr, length);
    }

    @RequestMapping(value = "/getAuthorsForPublisher/{pageNumber}/{windowSize}", method = RequestMethod.GET)
    @ResponseBody
    public Set<String> getAuthorsForPublisher(@RequestParam String publisher, @PathVariable int pageNumber, @PathVariable int windowSize) {
        return sparqlServiceFuseki.getAuthorsForPublisher(FUSEKI_SERVICE_URI, IRI.create(HcdcStringUtils.getSpaceSeparetedToUnderScore(publisher)), 0, 0);
    }

    @RequestMapping(value = "/getSuggestions/{length}", method = RequestMethod.GET)
    @ResponseBody
    public Set<String> getSuggestions(@RequestParam String subStr, @PathVariable("length") int length) {
        return sparqlServiceFuseki.getSuggestions(FUSEKI_SERVICE_URI, subStr, length);
    }
    /*@RequestMapping(value = "/getCountOfAuthorsForResourceType", method = RequestMethod.GET)
    public int getCountOfAuthorsForResourceType(@RequestParam String resType) {
        return sparqlServiceFuseki.getCountOfAuthorsForResourceType(FUSEKI_SERVICE_URI, HcdcStringUtils.getSpaceSeparetedToUnderScore(resType));
    }

    @RequestMapping(value = "/getAuthorsForResourceType/{pageNumber}/{windowSize}", method = RequestMethod.GET)
    public Set<String> getAuthorsForResourceType(@RequestParam String resType, @PathVariable("pageNumber") Integer pageNumber, @PathVariable("pageWin") Integer pageWin) {
        return sparqlServiceFuseki.getAuthorsForResourceType(FUSEKI_SERVICE_URI, HcdcStringUtils.getSpaceSeparetedToUnderScore(resType), pageNumber, pageWin);
    }*/
//    private String getRefPrefix(String pf) {
//        String phyPrefix = "http://www.astro.umd.edu/~eshaya/astro-onto/owl/physics.owl#";
//        String chemPrefix = "http://www.astro.umd.edu/~eshaya/astro-onto/owl/chemistry.owl#";
//        String phyExercises = "http://www.semanticweb.org/ontologies/2013/2/OPE.owl#";
//
//        String prefix = phyExercises;
//        if (pf.equalsIgnoreCase("1")) {
//            prefix = phyPrefix;
//        } else if (pf.equalsIgnoreCase("2")) {
//            prefix = chemPrefix;
//        }
//        return prefix;
//    }
}
