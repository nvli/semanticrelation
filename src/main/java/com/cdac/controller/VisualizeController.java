package com.cdac.controller;

import com.cdac.entity.DomainOntology;
import com.cdac.service.VisualizeService;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller handles all requests identified by '/visualize'
 *
 * @author Hemant Anjana
 */
@Controller
@RequestMapping("/visualize")
public class VisualizeController {

    /**
     * VisualizeService reference.
     */
    @Autowired
    private VisualizeService visualizeService;

    /**
     * Handles requests identified by <u>/owl</u>
     *
     * @return ModelAndView for viewing owl page.
     */
    @RequestMapping(value = "/owl/{ontId}", method = RequestMethod.GET)
    public ModelAndView getOWL(@PathVariable String ontId) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("selectedOnt", ontId);
        mav.addObject("domainOnts", visualizeService.getDomainOntologies());
        mav.setViewName("owlVisualize");
        return mav;
    }

    /**
     * Handles requests identified by <u>/getjsonforjs/{pageNum}/{pageWin}</u>
     * and takes 'q' as request param. Gives list of record that matches given
     * term.
     *
     * @param term search term
     * @param pageNum page number for pagination.
     * @param pageWin page window for pagination.
     * @param ontId
     * @return Map with key as record identifier and value as record name.
     */
    @RequestMapping(value = "/getjsonforjs/{pageNum}/{pageWin}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getReponseFormJs(@RequestParam("q") String term, @PathVariable Integer pageNum, @PathVariable Integer pageWin, @RequestParam("refOntId") long ontId) {
        System.out.println(">>>>>>>>>>>>>>>>>>> "+ontId);
        return visualizeService.callRestSparqlEndPointForRecord(term.toLowerCase(), pageNum, pageWin, visualizeService.getDomainOntologyById(ontId).getNamespaceURI());
    }

    /**
     * Handles requests identified by <u>/getjsonforjsCount</u>
     * and takes 'q' as request param. Gives count of record that matches given
     * term.
     *
     * @param term search term
     * @param ontId
     * @return Integer
     */
    @RequestMapping(value = "/getjsonforjsCount", method = RequestMethod.GET)
    @ResponseBody
    public Integer getReponseFormJsCount(@RequestParam("q") String term, @RequestParam("refOntId") long ontId) {
        System.out.println(">>>>>>>>>>>>>>>>>>> "+ontId);
        return visualizeService.callRestSparqlEndPointForCount(term.toLowerCase(), visualizeService.getDomainOntologyById(ontId).getNamespaceURI());
    }

//    private String getRefPrefix(String pf) {
//        String phyPrefix = "http://www.astro.umd.edu/~eshaya/astro-onto/owl/physics.owl#";
//        String chemPrefix = "http://www.astro.umd.edu/~eshaya/astro-onto/owl/chemistry.owl#";
//        String phyExercises = "http://www.semanticweb.org/ontologies/2013/2/OPE.owl#";
//
//        String prefix = phyExercises;
//        if (pf.equalsIgnoreCase("1")) {
//            prefix = phyPrefix;
//        } else if (pf.equalsIgnoreCase("2")) {
//            prefix = chemPrefix;
//        }
//        return prefix;
//    }

    /**
     * Handles requests identified by <u>/getjsonforDomainOnts</u>
     * Returns the list of all Domain Ontology for listing on main page.
     *
     * @return
     */
    @RequestMapping(value = "/getjsonforDomainOnts", method = RequestMethod.GET)
    @ResponseBody
    public List<DomainOntology> getDomainOnts() {
        return visualizeService.getDomainOntologies();
    }
}
