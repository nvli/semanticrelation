package com.cdac.controller;

import com.cdac.entity.Domain;
import com.cdac.entity.DomainOntology;
import com.cdac.entity.dao.IDomainOntologyDao;
import com.cdac.form.DomainOntologyForm;
import com.cdac.form.validator.DomainOntologyFormValidator;
import com.cdac.model.DomainWordWeight;
import com.cdac.processor.WordWeightProcessor;
import com.cdac.service.BaseService;
import com.cdac.service.OntologyService;
import com.cdac.utils.ResultHolder;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller for handling request for domain-ontology.
 * <u>Handles request /domain-ontology</u>
 *
 * @author Gajraj
 * @since 1
 * @version 1
 */
@Controller
@RequestMapping("/domain-ontology")
public class DomainOntologyController {

    /**
     * Logger Reference
     */
    private final Logger LOGGER = Logger.getLogger(DomainOntologyController.class);
    /**
     * DomainOntologyFormValidator reference
     */
    @Autowired
    private DomainOntologyFormValidator domainOntologyFormValidator;
    /**
     * OntologyService reference
     */
    @Autowired
    private OntologyService ontologyService;
    /**
     * default page window size. It value will populated from Property file.
     * (@see WEB-INF/etc/conf/dispatcher-servlet.xml)
     */
    @Value("${default.pagewindow}")
    private Integer defaultPageWin;
    /**
     * IDomainOntologyDao reference
     */
    @Autowired
    private IDomainOntologyDao domainOntologyDao;
    /**
     * BaseService reference
     */
    @Autowired
    private BaseService baseService;

    /**
     * method add Attribute 'domainList' to model for the all request coming
     * this controller. Spring MVC will automatically call this method.
     *
     * @param model {@link Model}
     */
    @ModelAttribute
    public void addCommonObj(Model model) {
        List<Domain> domainList = ontologyService.getAllDomains();
        model.addAttribute("domainList", domainList);
    }

    /**
     * Method handles request (GET and Post) identified by /show. Model bind
     * empty {@link DomainOntologyForm} and add view name
     * 'showDomainOntologyForm'. It is for viewing domain ontology form page.
     *
     * @param model {@link Model}
     * @return Model and view with view name and bind {@link DomainOntologyForm}
     * in model.
     */
    @RequestMapping(value = "/show", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView populateDomainOntologyForm(Model model) {
        //List<Domain> domainList = ontologyService.getAllDomains();
        //model.addAttribute("domainList", domainList);
        model.addAttribute("domainOntologyForm", new DomainOntologyForm());
        ModelAndView mav = new ModelAndView();
        mav.setViewName("showDomainOntologyForm");
        return mav;
    }

    /**
     * Method handles request (GET and Post) identified by /create. Method store
     * given domainOntologyForm in database by calling
     * {@link OntologyService.addOntology}. Before storing , it also check
     * whether ontology file is already available.
     *
     * @param domainOntologyForm {@link  DomainOntologyForm}
     * @param result {@link BindingResult}
     * @param model {@link Model}
     * @return ModelandView and redirect to listing of domain-ontology.
     */
    @RequestMapping(value = "/create", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView processDomainOntologyForm(@ModelAttribute("domainOntologyForm") DomainOntologyForm domainOntologyForm, BindingResult result, Model model) {
        ModelAndView mav = new ModelAndView();
        domainOntologyFormValidator.validate(domainOntologyForm, result);
        if (result.hasErrors()) {
            List domainList = ontologyService.getAllDomains();
            model.addAttribute("domainList", domainList);
            mav.setViewName("showDomainOntologyForm");
            return mav;
        }
        try {
            MultipartFile multipartFile = domainOntologyForm.getFile();
            InputStream is = multipartFile.getInputStream();
            byte[] byteStream = IOUtils.toByteArray(is);
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digest = md.digest(byteStream);
            boolean iSExist = domainOntologyDao.isExistChecksumForOWL(digest);
            if (iSExist) {
                LOGGER.error("File alreadt exist in the system with same checksum");
                mav.setView(new RedirectView("/domain-ontology/list/1/" + defaultPageWin, true, true, true));
                return mav;
            }
            /**
             * this is new file. Add and process it
             */
            ontologyService.addOntology(Long.parseLong(domainOntologyForm.getDomain()), byteStream, multipartFile.getOriginalFilename(), domainOntologyForm.getDescription(), domainOntologyForm.getDisplayName());
        } catch (IOException | NoSuchAlgorithmException | NumberFormatException ex) {
            LOGGER.error(ex, ex);
        }

        mav.setView(new RedirectView("/domain-ontology/list/1/" + defaultPageWin, true, true, true));
        return mav;
    }

    /**
     * Method handles request (GET and Post) identified by
     * <u>/list/{pageNum}/{pageWin}</u>
     * Gives list of domain-ontology.
     *
     * @param pageNum page number for pagination.
     * @param pageWin page window for pagination.
     * @param model {@link Model}
     * @return ModelandView to view listing page of domain-ontology
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/list/{pageNum}/{pageWin}", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView listDomainOntologies(@PathVariable("pageNum") int pageNum, @PathVariable("pageWin") int pageWin, Model model) {
        ModelAndView mav = new ModelAndView();
        ResultHolder resultHolder = baseService.getListDomainOntologies(pageNum, pageWin);
        List<DomainOntology> list = resultHolder.getResults();
        Long resultCount = resultHolder.getTotalResults();
        int pageperrecords = pageWin;
        int totalpages = (resultCount.intValue() / pageperrecords);
        if (resultCount.intValue() % pageperrecords > 0) {
            totalpages++;
        }
        if (resultCount < pageperrecords) {
            totalpages = 0;
        }
        model.addAttribute("results", list);
        model.addAttribute("totalpages", totalpages);
        model.addAttribute("resultCount", resultCount);
        model.addAttribute("pageNum", pageNum);
        model.addAttribute("pageWin", pageWin);

        mav.setViewName("domainOntologyList");
        return mav;
    }

    /**
     * Method handles request identified by
     * <u>/wfmap/{domOntId}</u>. Method generate weightage map(Strong word map
     * and weak word map ) of classes names of given domOntId and store in
     * database using {@link OntologyService]
     *
     * @param domOntId id of domain ontology.
     * @return ModelandView to index page.
     */
    @RequestMapping(value = "/wfmap/{domOntId}", method = {RequestMethod.GET})
    public ModelAndView buildStrongWeakForDomainOnt(@PathVariable("domOntId") int domOntId) {
        System.out.println("Started processing");
        try {
            float thresholdWordLength = 3;
            WordWeightProcessor wwp = new WordWeightProcessor(ontologyService, domOntId, thresholdWordLength);
            DomainWordWeight wmap = wwp.buildDomainWordWeightMap();
            Function<Map.Entry<String, Long>, Long> weakStrongF = map -> map.getValue() <= 2l ? map.getValue() : 3l;
            Map<String, Long> weakStrongMap = wmap.getwordWeightMap().entrySet().parallelStream()
                    .collect(Collectors.toMap(Map.Entry::getKey, weakStrongF));
            wmap.setwordWeightMap(weakStrongMap);
            DomainOntology domainOnt = ontologyService.getDomainOntology(domOntId);
            ObjectMapper mapper = new ObjectMapper();
            String ouputJsonText = mapper.writeValueAsString(wmap);
            domainOnt.setWordFrequencyMap(ouputJsonText);
            ontologyService.updateDomainOntology(domainOnt);
            LOGGER.info("Map Created and saved successfully");
        } catch (Exception ex) {
            LOGGER.error("Something bad happened", ex);
        }
        return new ModelAndView("index");
    }

}
