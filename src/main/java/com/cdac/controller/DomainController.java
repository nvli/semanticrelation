package com.cdac.controller;

import com.cdac.entity.Domain;
import com.cdac.form.DomainForm;
import com.cdac.form.validator.DomainFormValidator;
import com.cdac.service.BaseService;
import com.cdac.service.OntologyService;
import com.cdac.utils.ResultHolder;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 ** Controller for handling request for domain.
 * <u>Handles request /domain</u>
 *
 * @author Gajraj
 * @since 1
 * @version 1
 */
@RestController
@RequestMapping("/domain")
public class DomainController {

    /**
     * Logger Reference
     */
    private final Logger LOGGER = Logger.getLogger(DomainController.class);
    /**
     * DomainFormValidator reference
     */
    @Autowired
    private DomainFormValidator domainFormValidator;
    /**
     * OntologyService reference
     */
    @Autowired
    private OntologyService ontologyService;
    /**
     * Integer reference
     */
    @Value("${default.pagewindow}")
    private Integer defaultPageWin;
    /**
     * BaseService reference
     */
    @Autowired
    private BaseService baseService;

    /**
     * method add Attribute 'domainList' to model for the all request coming
     * this controller. Spring MVC will automatically call this method.
     *
     * @param model {@link Model}
     */
    @ModelAttribute
    public void addCommonObj(Model model) {
        List<Domain> domainList = ontologyService.getAllDomains();
        model.addAttribute("domainList", domainList);
    }

    /**
     * Method handles request (GET and Post) identified by /show. Model bind
     * empty {@link DomainForm} and add view name 'showDomainForm'. It is for
     * viewing domain form page.
     *
     * @param model {@link Model}
     * @return Model and view with view name and bind {@link DomainForm} in
     * model.
     */
    @RequestMapping(value = "/show", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView populateDomainForm(Model model) {
        //List<Domain> domainList = ontologyService.getAllDomains();
        model.addAttribute("domainForm", new DomainForm());
        //model.addAttribute("domainList", domainList);
        ModelAndView mav = new ModelAndView();
        mav.setViewName("showDomainForm");
        return mav;
    }

    /**
     * Method handles request (GET and Post) identified by /create. Method store
     * given domainForm in database by calling
     * {@link OntologyService.createDomain}. Before storing , it also validates
     * domainForm.
     *
     *
     * @param domainForm {@link  DomainForm}
     * @param result {@link BindingResult}
     * @param model {@link Model}
     * @return ModelandView and redirect to listing of domain.
     */
    @RequestMapping(value = "/create", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView processDomainForm(@ModelAttribute("domainForm") DomainForm domainForm, BindingResult result, Model model) {
        ModelAndView mav = new ModelAndView();
        domainFormValidator.validate(domainForm, result);
        if (result.hasErrors()) {
            //List<Domain> domainList = ontologyService.getAllDomains();
            model.addAttribute("domainForm", domainForm);
            //model.addAttribute("domainList", domainList);
            mav.setViewName("showDomainForm");
            return mav;
        } else if (domainForm.getParentDomain().equalsIgnoreCase("select")) {
            Domain d = new Domain();
            d.setDomainName(domainForm.getDomainName());
            d.setDescription(domainForm.getDescription());
            ontologyService.createDomain(d);
            mav.setView(new RedirectView("/domain/list/1/" + defaultPageWin, true, true, true));
            return mav;
        } else {
            Domain bd = ontologyService.getDomainByName(domainForm.getParentDomain());
            if (bd == null) {
                LOGGER.error("Parent domain name is invalid");
                mav.setViewName("showDomainForm");
                return mav;
            } else {
                Domain d = new Domain();
                d.setDomainName(domainForm.getDomainName());
                d.setDescription(domainForm.getDescription());
                d.setBroaderDomain(bd);
                ontologyService.createDomain(d);
                mav.setView(new RedirectView("/domain/list/1/" + defaultPageWin, true, true, true));
                return mav;
            }
        }
    }

    /**
     * Method handles request (GET and Post) identified by
     * <u>/list/{pageNum}/{pageWin}</u>
     * Gives list of domains.
     *
     * @param pageNum page number for pagination.
     * @param pageWin page window for pagination.
     * @param model {@link Model}
     * @return ModelandView to view listing page of domain
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/list/{pageNum}/{pageWin}", method = {RequestMethod.POST, RequestMethod.GET})
    public ModelAndView listDomains(@PathVariable("pageNum") int pageNum, @PathVariable("pageWin") int pageWin, Model model) {
        ModelAndView mav = new ModelAndView();
        ResultHolder resultHolder = baseService.getListDomains(pageNum, pageWin);
        List<Domain> list = resultHolder.getResults();
        Long resultCount = resultHolder.getTotalResults();
        int pageperrecords = pageWin;
        int totalpages = (resultCount.intValue() / pageperrecords);
        if (resultCount.intValue() % pageperrecords > 0) {
            totalpages++;
        }
        if (resultCount < pageperrecords) {
            totalpages = 0;
        }
        model.addAttribute("results", list);
        model.addAttribute("totalpages", totalpages);
        model.addAttribute("resultCount", resultCount);
        model.addAttribute("pageNum", pageNum);
        model.addAttribute("pageWin", pageWin);

        mav.setViewName("domainList");
        return mav;
    }
}
