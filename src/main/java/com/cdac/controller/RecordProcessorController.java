package com.cdac.controller;

import com.cdac.entity.Domain;
import com.cdac.entity.DomainOntology;
import com.cdac.entity.ResourceProcess;
import com.cdac.entity.dao.IDomainOntologyDao;
import com.cdac.entity.dao.IResourceProcessDAO;
import com.cdac.form.DomainOntologyProcessForm;
import com.cdac.form.validator.DomainOntologyProcessFormValidator;
import com.cdac.service.BaseService;
import com.cdac.service.DummuyOrganizationService;
import com.cdac.service.OntologyService;
import com.cdac.service.ProcessorService;
import com.cdac.utils.Helper;
import com.cdac.utils.ResultHolder;
import com.cdac.utils.Status;
import java.util.List;
import java.util.Set;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

/**
 * Controller handles all requests identified by '/ont'
 *
 * @author Gajraj
 * @since 1
 * @version 1
 */
@Controller
@RequestMapping("/ont")
public class RecordProcessorController {

    /**
     * Logger Reference
     */
    private final Logger LOGGER = Logger.getLogger(RecordProcessorController.class);
    /**
     * ProcessorService Reference
     */
    @Autowired
    private ProcessorService processorService;
    /**
     * OntologyService Reference
     */
    @Autowired
    private OntologyService ontologyService;
    /**
     * DummuyOrganizationService Reference
     */
    @Autowired
    private DummuyOrganizationService dummyOrgService;
    /**
     * BaseService Reference
     */
    @Autowired
    private BaseService baseService;
    /**
     * DomainOntologyProcessFormValidator Reference
     */
    @Autowired
    private DomainOntologyProcessFormValidator domainOntologyFormValidator;
    /**
     * default page window size. It value will populated from Property file.
     * (@see WEB-INF/etc/conf/dispatcher-servlet.xml)
     */
    @Value("${default.pagewindow}")
    private Integer defaultPageWin;
    /**
     * IResourceProcessDAO Reference
     */
    @Autowired
    private IResourceProcessDAO resourceProcessDAO;
    /**
     * IDomainOntologyDao Reference
     */
    @Autowired
    private IDomainOntologyDao domainOntologyDao;

    /**
     * Handles request identified by <u>/process</u>. Process Ontology on the
     * basis of domainOntProcessForm .
     *
     * @param domainOntProcessForm {@link DomainOntologyProcessForm}
     * @param result {@link BindingResult}
     * @return Model and view with view 'showOntProcessForm'
     */
    @RequestMapping(value = "/process", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView processCSV(@ModelAttribute("ontProcessForm") DomainOntologyProcessForm domainOntProcessForm, BindingResult result) {
        domainOntologyFormValidator.validate(domainOntProcessForm, result);
        ModelAndView mav = new ModelAndView();
        if (result.hasErrors()) {
            Set<String> resourceType = dummyOrgService.getResourcType();
            List<Domain> baseDomainList = baseService.getListBaseDomains();
            mav.addObject("resourceType", resourceType);
            mav.addObject("baseDomainList", baseDomainList);
            mav.setViewName("showOntProcessForm");
            return mav;
        }

        if (domainOntProcessForm.getResourceCode().equalsIgnoreCase("All")) {
            String useOrgCsv = "";
            if (domainOntProcessForm.getResourceType().equals("Open Repository")) {
                useOrgCsv = "open_repository.csv";
            } else if (domainOntProcessForm.getResourceType().equals("National Programme on Technology Enhanced Learning")) {
                useOrgCsv = "nptl_organizations.csv";
            }

            DummuyOrganizationService.getOrganizationByResourceType(useOrgCsv).forEach(
                    organization -> processResource(domainOntProcessForm.getResourceType(), domainOntProcessForm.getOwlFile(), organization.getOrgCode(), organization.getOrgName())
            );
        } else {
            processResource(domainOntProcessForm.getResourceType(), domainOntProcessForm.getOwlFile(), domainOntProcessForm.getResourceCode(), domainOntProcessForm.getResourceName());
        }

        /**
         * Insert DomainOntologyProcess to Table
         */
        /*ResourceProcess resourceProcess = new ResourceProcess();
        resourceProcess.setRecordProcessCount(0l);
        resourceProcess.setCreatedOn(Helper.rightNow());
        resourceProcess.setResourceCode(domainOntProcessForm.getResourceCode());
        resourceProcess.setResourceType(domainOntProcessForm.getResourceType());
        resourceProcess.setResourceName(domainOntProcessForm.getResourceName());
        //TODO set user id who initiainted the process  in next version it will be implemented
        resourceProcess.setCreatedBy(0l);
        resourceProcess.setStatus(Status.NEW);//
        DomainOntology domainOntology = domainOntologyDao.get(Long.parseLong(domainOntProcessForm.getOwlFile()));
        resourceProcess.setReferenceOnt(domainOntology);
        resourceProcessDAO.createNew(resourceProcess);
        //TODO change to strong weak impl
        // processorService.processWithCSV(resourceProcess);
        // processorService.addSemanticInformation(resourceProcess, ProcessorService.processOnlyMetadata);
        // processorService.addSemanticInformation(resourceProcess, ProcessorService.processWithRefOnt);
        if (processorService.init(resourceProcess)) {
            processorService.processLucene(resourceProcess);
        }
        LOGGER.info("Processig task started");*/
        mav.setView(new RedirectView("/ont/process/list/1/" + defaultPageWin, true, true, true));
        return mav;
    }

    /**
     * Handles request identified by <u>/processForm</u>. Show Domain Ontology
     * Processing Form
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/processForm", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView populateDomainOntologyProcessForm() {
        Set<String> resourceType = dummyOrgService.getResourcType();
        List<Domain> baseDomainList = baseService.getListBaseDomains();
        ModelAndView mav = new ModelAndView();
        mav.addObject("resourceType", resourceType);
        mav.addObject("baseDomainList", baseDomainList);
        mav.addObject("ontProcessForm", new DomainOntologyProcessForm());
        mav.setViewName("showOntProcessForm");
        return mav;
    }

    /**
     * Handles request identified by <u>/orglist/{resType}<u>. Gives list of
     * {@link DummuyOrganizationService.Organization}.
     *
     * @param resType resource Code of organization
     * @return List of {@link DummuyOrganizationService.Organization}
     */
    @ResponseBody
    @RequestMapping(value = "/orglist/{resType}", method = {RequestMethod.GET, RequestMethod.POST})
    public List<DummuyOrganizationService.Organization> getOrgList(@PathVariable("resType") String resType) {
        return dummyOrgService.getOrgList(resType);
    }

    /**
     * Handles request identified by <u>/process/base/{baseDomainId}<u>. Give
     * list of {@link Domains} (sub-domains) of given domain id.
     *
     * @param baseDomainId id of base domain
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/process/base/{baseDomainId}", method = {RequestMethod.GET, RequestMethod.POST})
    public List<Domain> getSubDomainsOfSelectedBaseDomain(@PathVariable("baseDomainId") int baseDomainId) {
        List<Domain> subDomainList = baseService.getSubDomains(baseDomainId);
        return subDomainList;
    }

    /**
     * Handles request identified by <u>/process/owlfiles/{subDomainId}<u>. Give
     * list of {@link DomainOntology (sub-domains) of given subDomainId.
     *
     * @param subDomainId if of sub domain
     * @param model
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/process/owlfiles/{subDomainId}", method = {RequestMethod.GET, RequestMethod.POST})
    public List<DomainOntology> getOWLofSelectedBaseDomain(@PathVariable("subDomainId") int subDomainId) {
        List<DomainOntology> owlFileList = ontologyService.getOWLofSelectedDomain(subDomainId);
        return owlFileList;
    }

    /**
     * Handles request identified by <u>/process/list/{pageNum}/{pageWin}<u>.
     * Give list of process available in database.
     *
     * @param pageNum page number for pagination.
     * @param pageWin page window for pagination.
     * @return ModelAndView with view 'ontProcessList' and list of process
     */
    @RequestMapping(value = "/process/list/{pageNum}/{pageWin}", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView domainOntologyProcessList(@PathVariable("pageNum") int pageNum, @PathVariable("pageWin") int pageWin) {
        ModelAndView mav = new ModelAndView();

        ResultHolder resultHolder = resourceProcessDAO.getListOntProcess(pageNum, pageWin);
        List<ResourceProcess> listResourceProcess = resultHolder.getResults();
        Long resultCount = resultHolder.getTotalResults();
        int pageperrecords = pageWin;
        int totalpages = (resultCount.intValue() / pageperrecords);
        if (resultCount.intValue() % pageperrecords > 0) {
            totalpages++;
        }
        if (resultCount < pageperrecords) {
            totalpages = 0;
        }
        mav.addObject("listResourceProcess", listResourceProcess);
        mav.addObject("totalpages", totalpages);
        mav.addObject("resultCount", resultCount);
        mav.addObject("pageNum", pageNum);
        mav.addObject("pageWin", pageWin);
        mav.setViewName("ontProcessList");

        return mav;
    }

    public void processResource(String resType, String owlFile, String orgCode, String orgName) {
        /**
         * Insert DomainOntologyProcess to Table
         */
        ResourceProcess resourceProcess = new ResourceProcess();
        resourceProcess.setRecordProcessCount(0l);
        resourceProcess.setCreatedOn(Helper.rightNow());
        resourceProcess.setResourceCode(orgCode);
        resourceProcess.setResourceType(resType);
        resourceProcess.setResourceName(orgName);
        //TODO set user id who initiainted the process  in next version it will be implemented
        resourceProcess.setCreatedBy(0l);
        resourceProcess.setStatus(Status.NEW);//
        DomainOntology domainOntology = domainOntologyDao.get(Long.parseLong(owlFile));
        resourceProcess.setReferenceOnt(domainOntology);
        resourceProcessDAO.createNew(resourceProcess);
        //TODO change to strong weak impl
        // processorService.processWithCSV(resourceProcess);
        // processorService.addSemanticInformation(resourceProcess, ProcessorService.processOnlyMetadata);
        // processorService.addSemanticInformation(resourceProcess, ProcessorService.processWithRefOnt);
        processorService.init(resourceProcess);
        LOGGER.info("Processig task started");
    }

}
