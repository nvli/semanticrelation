/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.beans;

import java.util.List;

/**
 *
 * @author Suhas
 */
public class QueryParameter {

    /**
     * Names of recordIdentifiers
     */
    public List<String> recordIdentifiers;
    /**
     * Dublin core elements
     */
    public List<String> dcElements;

    public List<String> getRecordIdentifiers() {
        return recordIdentifiers;
    }

    public void setRecordIdentifiers(List<String> recordIdentifiers) {
        this.recordIdentifiers = recordIdentifiers;
    }

    public List<String> getDcElements() {
        return dcElements;
    }

    public void setDcElements(List<String> dcElements) {
        this.dcElements = dcElements;
    }

}
