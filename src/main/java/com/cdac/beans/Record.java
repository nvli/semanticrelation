package com.cdac.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Class is used to show details.
 * <p>
 * Hash code of object is calculated on recordIdentifier property. Equality of
 * object is checked on recordIdentifier property.</p>
 *
 * @author Hemant Anjana
 */
public class Record implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * record identifier
     */
    private String recordIdentifier;
    /**
     * meta-data type
     */
    private String metadataType;
    /**
     * original file name
     */
    @JsonIgnore
    private List<String> lectureList;

    private String originalFileName;
    /**
     * title of record.
     */
    private List<String> title;
    /**
     * description of record.
     */
    private List<String> description;
    /**
     * rights of record.
     */
    private List<String> rights;
    /**
     * date of record.
     */
    private List<String> date;
    /**
     * format of record.
     */
    private List<String> format;
    /**
     * relation of record.
     */
    private List<String> relation;
    /**
     * coverage of record.
     */
    private List<String> coverage;
    /**
     * identifier details.
     */
    private List<String> identifier;
    /**
     * subject of record.
     */
    private List<String> subject;
    /**
     * creator of record.
     */
    private List<String> creator;
    /**
     * publisher of record.
     */
    private List<String> publisher;
    /**
     * source of record.
     */
    private List<String> source;
    /**
     * language of record.
     */
    private List<String> language;
    /**
     * type of record.
     */
    private List<String> type;
    /**
     * file location
     */
    private String location;
    /**
     * like count
     */
    private Long recordLikeCount;
    /**
     * view count
     */
    private Long recordViewCount;
    /**
     * no. of downloads
     */
    private Long recordNoOfDownloads;
    /**
     * rating
     */
    private long ratingCount;
    /**
     * shares count
     */
    private long recordNoOfShares;
    /**
     * review count
     */
    private long recordNoOfReview;
    /**
     * Udc notation
     */
    private List<String> udcNotation;
    /**
     * custom notation
     */
    private List<String> customNotation;
    /**
     * contributor of record.(realated to dc)
     */
    private List<String> contributor;
    /**
     * audience (related to marc21)
     */
    private List<String> audience;
    /**
     * provenance (related to marc21)
     */
    private List<String> provenance;
    /**
     * isbn (related to marc21)
     */
    private List<String> isbn;
    /**
     * lccn (related to marc21)
     */
    private List<String> lccn;
    /**
     * edition (related to marc21)
     */
    private List<String> edition;
    /**
     * issn (related to marc21)
     */
    private List<String> issn;
    /**
     * notes (related to marc21)
     */
    private List<String> notes;
    private List<String> paths;
    /**
     * crowd Source Flag
     */
    private boolean crowdSourceFlag;
    /**
     * crowd Source stagge Flag
     */
    private boolean crowdSourceStageFlag;
    /**
     * for all other metadata
     */
    private List<OtherMetadata> others;
    /**
     * MARC view JSON object
     */
    private String jsonView;
    /**
     * record lable
     */
    private String nameToView;
    /**
     * Number of count linked with article
     */
    private long linkedWithArticleCount;
    /**
     * final rating score
     */
    private float ratingScore;
    /**
     * final score
     */
    private float finalScore;
    private String subResourceName;
    /**
     * mets XML path
     */
    private String metsXmlPath;
    /**
     * ORE XML Path
     */
    private String oreXmlPath;
    /**
     * VIRM 3D images Path
     */
    private List<String> threeDimensionalImagesPath;
//<editor-fold defaultstate="collapsed">

    public List<String> getThreeDimensionalImagesPath() {
        return threeDimensionalImagesPath;
    }

    public void setThreeDimensionalImagesPath(List<String> threeDimensionalImagesPath) {
        this.threeDimensionalImagesPath = threeDimensionalImagesPath;
    }

    public String getMetsXmlPath() {
        return metsXmlPath;
    }

    public void setMetsXmlPath(String metsXmlPath) {
        this.metsXmlPath = metsXmlPath;
    }

    public String getOreXmlPath() {
        return oreXmlPath;
    }

    public void setOreXmlPath(String oreXmlPath) {
        this.oreXmlPath = oreXmlPath;
    }

    public long getLinkedWithArticleCount() {
        return linkedWithArticleCount;
    }

    public void setLinkedWithArticleCount(long linkedWithArticleCount) {
        this.linkedWithArticleCount = linkedWithArticleCount;
    }

    public float getRatingScore() {
        return ratingScore;
    }

    public void setRatingScore(float ratingScore) {
        this.ratingScore = ratingScore;
    }

    public float getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(float finalScore) {
        this.finalScore = finalScore;
    }

    public long getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(long ratingCount) {
        this.ratingCount = ratingCount;
    }

    public long getRecordNoOfReview() {
        return recordNoOfReview;
    }

    public void setRecordNoOfReview(long recordNoOfReview) {
        this.recordNoOfReview = recordNoOfReview;
    }

    public long getRecordNoOfShares() {
        return recordNoOfShares;
    }

    public void setRecordNoOfShares(long recordNoOfShares) {
        this.recordNoOfShares = recordNoOfShares;
    }

    public String getNameToView() {
        return nameToView;
    }

    public void setNameToView(String nameToView) {
        this.nameToView = nameToView;
    }

    public String getJsonView() {
        return jsonView;
    }

    public void setJsonView(String jsonView) {
        this.jsonView = jsonView;
    }

    public List<OtherMetadata> getOthers() {
        return others;
    }

    public void setOthers(List<OtherMetadata> others) {
        this.others = others;
    }

    public boolean isCrowdSourceStageFlag() {
        return crowdSourceStageFlag;
    }

    public void setCrowdSourceStageFlag(boolean crowdSourceStageFlag) {
        this.crowdSourceStageFlag = crowdSourceStageFlag;
    }

    public boolean isCrowdSourceFlag() {
        return crowdSourceFlag;
    }

    public void setCrowdSourceFlag(boolean crowdSourceFlag) {
        this.crowdSourceFlag = crowdSourceFlag;
    }

    public List<String> getPaths() {
        return paths;
    }

    public void setPaths(List<String> paths) {
        this.paths = paths;
    }

    public String getRecordIdentifier() {
        return recordIdentifier;
    }

    public void setRecordIdentifier(String recordIdentifier) {
        this.recordIdentifier = recordIdentifier;
    }

    public String getMetadataType() {
        return metadataType;
    }

    public void setMetadataType(String metadataType) {
        this.metadataType = metadataType;
    }

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public List<String> getTitle() {
        return title;
    }

    public void setTitle(List<String> title) {
        this.title = title;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(List<String> description) {
        this.description = description;
    }

    public List<String> getRights() {
        return rights;
    }

    public void setRights(List<String> rights) {
        this.rights = rights;
    }

    public List<String> getDate() {
        return date;
    }

    public void setDate(List<String> date) {
        this.date = date;
    }

    public List<String> getFormat() {
        return format;
    }

    public void setFormat(List<String> format) {
        this.format = format;
    }

    public List<String> getRelation() {
        return relation;
    }

    public void setRelation(List<String> relation) {
        this.relation = relation;
    }

    public List<String> getCoverage() {
        return coverage;
    }

    public void setCoverage(List<String> coverage) {
        this.coverage = coverage;
    }

    public List<String> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(List<String> identifier) {
        this.identifier = identifier;
    }

    public List<String> getSubject() {
        return subject;
    }

    public void setSubject(List<String> subject) {
        this.subject = subject;
    }

    public List<String> getCreator() {
        return creator;
    }

    public void setCreator(List<String> creator) {
        this.creator = creator;
    }

    public List<String> getPublisher() {
        return publisher;
    }

    public void setPublisher(List<String> publisher) {
        this.publisher = publisher;
    }

    public List<String> getSource() {
        return source;
    }

    public void setSource(List<String> source) {
        this.source = source;
    }

    public List<String> getLanguage() {
        return language;
    }

    public void setLanguage(List<String> language) {
        this.language = language;
    }

    public List<String> getType() {
        return type;
    }

    public void setType(List<String> type) {
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Long getRecordLikeCount() {
        return recordLikeCount;
    }

    public void setRecordLikeCount(Long recordLikeCount) {
        this.recordLikeCount = recordLikeCount;
    }

    public Long getRecordViewCount() {
        return recordViewCount;
    }

    public void setRecordViewCount(Long recordViewCount) {
        this.recordViewCount = recordViewCount;
    }

    public Long getRecordNoOfDownloads() {
        return recordNoOfDownloads;
    }

    public void setRecordNoOfDownloads(Long recordNoOfDownloads) {
        this.recordNoOfDownloads = recordNoOfDownloads;
    }

    public List<String> getUdcNotation() {
        return udcNotation;
    }

    public void setUdcNotation(List<String> udcNotation) {
        this.udcNotation = udcNotation;
    }

    public List<String> getCustomNotation() {
        return customNotation;
    }

    public void setCustomNotation(List<String> customNotation) {
        this.customNotation = customNotation;
    }

    public List<String> getContributor() {
        return contributor;
    }

    public void setContributor(List<String> contributor) {
        this.contributor = contributor;
    }

    public List<String> getAudience() {
        return audience;
    }

    public void setAudience(List<String> audience) {
        this.audience = audience;
    }

    public List<String> getProvenance() {
        return provenance;
    }

    public void setProvenance(List<String> provenance) {
        this.provenance = provenance;
    }

    public List<String> getIsbn() {
        return isbn;
    }

    public void setIsbn(List<String> isbn) {
        this.isbn = isbn;
    }

    public List<String> getLccn() {
        return lccn;
    }

    public void setLccn(List<String> lccn) {
        this.lccn = lccn;
    }

    public List<String> getEdition() {
        return edition;
    }

    public void setEdition(List<String> edition) {
        this.edition = edition;
    }

    public List<String> getIssn() {
        return issn;
    }

    public void setIssn(List<String> issn) {
        this.issn = issn;
    }

    public List<String> getNotes() {
        return notes;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }

    public String getSubResourceName() {
        return subResourceName;
    }

    public void setSubResourceName(String subResourceName) {
        this.subResourceName = subResourceName;
    }

    public List<String> getLectureList() {
        return lectureList;
    }

    public void setLectureList(List<String> lectureList) {
        this.lectureList = lectureList;
    }

//</editor-fold>
    /**
     * hash code of object is calculated on recordIdentifier property.
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.recordIdentifier);
        return hash;
    }

    /**
     * Equality of object is checked on recordIdentifier property.
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Record other = (Record) obj;
        if (!Objects.equals(this.recordIdentifier, other.recordIdentifier)) {
            return false;
        }
        return true;
    }
}
