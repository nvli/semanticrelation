<%--
    Document   : domainOntologyList
    Created on : Apr 12, 2017, 11:16:41 AM
    Author     : Suhas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>

<script>
    $(document).ready(function () {
        for (var i = 0; i < ${totalpages}; i++)
        {
            $("ul").append("<li><a href='${context}/domain-ontology/list/" + (i + 1) + "/${pageWin}'>" + (i + 1) + "</a></li>");
        }
    });

</script>
<style>
    tr {display: block; }
    th, td { width: 373px; }
    tbody { display: block;  overflow: auto;}
</style>
<div class="container">
    <span class="pull-left"><spring:message code="domain.ontology.list"/></span>
    <span class="pull-right"><a href="${context}/domain-ontology/show"><spring:message code="domain.ontology.creation.anch"/></a></span>
    <div class="clearfix"></div>
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="80%" height="70%">
        <thead>
            <tr>
                <th>Sr. No.</th>
                <th>Domain Name</th>
                <th>Parent Domain</th>
                <th>Processing Status</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${results}" var="result" varStatus="loop">
                <tr>
                    <td>${loop.index+1}</td>
                    <td>${result.domain.domainName}</td>
                    <td>${result.namespaceURI}</td>
                    <td>${result.processingStatus}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <ul class="pagination" >

    </ul>
</div>
