<%-- 
    Document   : showDomainOntologyForm
    Created on : Apr 5, 2017, 4:58:07 PM
    Author     : Hemant Anjana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="container">
    <span class="pull-left"><spring:message code="domain.ontology.creation"/></span>
    <div class="clearfix"></div> 
    <form:form id="registerform" name="registerform" action="${context}/domain-ontology/create" commandName="domainOntologyForm"  method="post" enctype="multipart/form-data" class="form-horizontal">
        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="forDisplyName"><spring:message code="domain.ontology.label.displayName.required"/><em> * </em></label>
            <div class="col-lg-3 col-sm-2 col-md-2 col-xs-2">  
                <span><form:input path="displayName" class="form-control" /></span>
                <span><form:errors path="displayName" class="help-inline"/></span>   
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="forDescription"><spring:message code="domain.ontology.label.description"/><em> * </em></label>
            <div class="col-lg-3 col-sm-2 col-md-2 col-xs-2">  
                <span><form:input path="description" class="form-control" /></span>
                <span><form:errors path="description" class="help-inline"/></span>   
            </div>
        </div>
        <div class="clearfix"></div>    

        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="selectDomainName"><spring:message code="domain.ontology.label.domain"/><em> * </em></label>
<!--                <span><form:input path="domain" /></span>-->
            <div class="col-lg-3 col-sm-2 col-md-2 col-xs-2">  
                <form:select path="domain" class="form-control input-md">
                    <form:option value="select">--- Select ---</form:option>
                    <c:forEach items="${domainList}" var="domain">     
                        <form:option value="${domain.id}">${domain.domainName}</form:option>
                    </c:forEach>
                </form:select>
                <span><form:errors path="domain" class="help-inline"/></span>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="forFileupload"><spring:message code="domain.ontology.label.file"/><em> * </em></label>
            <div class="col-lg-3 col-sm-2 col-md-2 col-xs-2">  
                <span><input type="file" name="file" class="form-control"/></span>
                <span><form:errors path="file" class="help-inline"/></span>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">        
            <div class="col-lg-offset-2 col-lg-10">    
                <input type="submit" class="btn btn-default" value='<spring:message code="domain.ontology.label.submit"/>' />
            </div>
        </div>
    </form:form>
</div>
