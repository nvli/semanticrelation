<%--
    Document   : default-header
    Created on : Apr 19, 2017, 11:36:53 AM
    Author     : Hemant Anjana
--%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="context" value="${pageContext.request.contextPath}" scope="request" />
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<div class="container">
    <div class="jumbotron">
        <h1><spring:message code="welcome"/></h1>
    </div>
    <p><span class="pull-left"><a href="${context}">Home</a></span></p>
</div>

