<%--
    Document   : ontProcess
    Created on : Apr 17, 2017, 3:44:36 PM
    Author     : Suhas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<c:set var="context" value="${pageContext.request.contextPath}" />

<script>
    $(document).ready(function () {
        var orgN = "${ontProcessForm.resourceCode}";
        var subDomainId = "${ontProcessForm.subDomain}";
        var owlFileId = "${ontProcessForm.owlFile}";
        populateOrgName();
        populateSubDomain();
        if (subDomainId !== "" && subDomainId !== "select") {
            populateOwlFiles("subDomain");
        } else {
            populateOwlFiles("baseDomain");
        }

        if (orgN !== "" && orgN !== "select") {
            $("#resourceCode option[value='${ontProcessForm.resourceCode}']").attr('selected', 'selected');
        }
        if (subDomainId !== "" && subDomainId !== "select") {
            $("#subDomain option[value='${ontProcessForm.subDomain}']").attr('selected', 'selected');
        }
        if (owlFileId !== "" && owlFileId !== "select") {
            $("#owlFile option[value='${ontProcessForm.owlFile}']").attr('selected', 'selected');
        }

        $("#resourceType").change(function () {
            populateOrgName();
        });
        $("#baseDomain").change(function () {
            populateSubDomain();
            populateOwlFiles("baseDomain");
        });
        $("#subDomain").change(function () {
            populateOwlFiles("subDomain");
        });
        $("#resourceCode").change(function () {
            $("#resourceName").val("");
            if ($('#resourceType :selected').text() !== "<spring:message code="default.select"/>") {
                $("#resourceName").val($('#resourceCode :selected').text());
            }
        });

    });
    var populateOrgName = function () {
        if ($('#resourceType :selected').text() !== "<spring:message code="default.select"/>")
        {
            $.ajax({
                url: "${context}/ont/orglist/" + $('#resourceType :selected').attr('value'),
                success: function (result) {
                    $('#resourceCode').find('option').remove().end();
                    $('#resourceCode').append("<option value='select'><spring:message code="default.select"/></option>");
                    $('#resourceCode').append("<option value='all'>Select All</option>");
                    result.forEach(function (item, index) {
                        console.log(item);
                        $('#resourceCode').append("<option value=" + item.orgCode + ">" + item.orgName + "</option>");
                    });
                }});
        } else
        {
            $('#resourceCode').find('option').remove().end();
            $('#resourceCode').append("<option value='select'><spring:message code="default.select"/></option>");
        }
    }

    var populateSubDomain = function () {
        if ($('#baseDomain :selected').text() !== "<spring:message code="default.select"/>")
        {
            $.ajax({
                url: "${context}/ont/process/base/" + $('#baseDomain :selected').attr('value'),
                success: function (result) {
                    $('#subDomain').find('option').remove().end();
                    $('#subDomain').append("<option value='select'><spring:message code="default.select"/></option>");
                    result.forEach(function (item, index) {
                        $('#subDomain').append("<option value=" + item[0] + ">" + item[1] + "</option>");
                    });
                }});
        } else
        {
            $('#subDomain').find('option').remove().end();
            $('#subDomain').append("<option value='select'><spring:message code="default.select"/></option>");
        }
    }

    var populateOwlFiles = function (domain) {
        if ($('#' + domain + ' :selected').text() !== "<spring:message code="default.select"/>")
        {
            console.log("==" + domain + "==" + $('#' + domain + ' :selected').attr('value'));
            $.ajax({
                url: "${context}/ont/process/owlfiles/" + $('#' + domain + ' :selected').attr('value'),
                success: function (result) {
                    console.log("data " + result);
                    $('#owlFile').find('option').remove().end();
                    $('#owlFile').append("<option value='select'><spring:message code="default.select"/></option>");
                    $.each(result, function (key, value) {
                        $('#owlFile').append("<option value=" + value.id + ">" + value.fileName + "</option>");
                    });
                }, error: function (err) {
                    console.log(err);
                }});
        } else
        {
            $('#owlFile').find('option').remove().end();
            $('#owlFile').append("<option value='select'><spring:message code="default.select"/></option>");
        }
    }
</script>
<br/>
<div class="container">
    <form:form id="ontprocessform" name="ontprocessform" action='${context}/ont/process' commandName="ontProcessForm"  method="post" class="form-horizontal">

        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="selectResourceType"><spring:message code="domain.ontology.label.domainontology.resourcetype"/><em> * </em></label>
            <div class="col-lg-4 col-sm-8 col-md-6 col-xs-12">
                <form:select id="resourceType" path="resourceType" class="form-control input-md">
                    <form:option value="select"><spring:message code="default.select"/></form:option>
                    <c:forEach items="${resourceType}" var="resourceT">
                        <form:option value="${resourceT}">${resourceT}</form:option>
                    </c:forEach>
                </form:select>
                <span><form:errors path="resourceType" class="help-inline"/></span>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="selectOrganizationName"><spring:message code="domain.ontology.label.domainontology.orgname"/><em> * </em></label>
            <div class="col-lg-4 col-sm-8 col-md-6 col-xs-12">
                <form:select id="resourceCode" path="resourceCode" class="form-control input-md">
                    <form:option value="select"><spring:message code="default.select"/></form:option>
                </form:select>
                <span><form:errors path="resourceCode" class="help-inline"/></span>
            </div>
            <form:hidden path="resourceName" id="resourceName"/>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="selectBaseDomain"><spring:message code="domain.ontology.label.domainontology.basedomain"/><em> * </em></label>
            <div class="col-lg-4 col-sm-8 col-md-6 col-xs-12">
                <form:select id="baseDomain" path="baseDomain" class="form-control input-md">
                    <form:option value="select"><spring:message code="default.select"/></form:option>
                    <c:forEach items="${baseDomainList}" var="baseDomain">
                        <form:option value="${baseDomain.id}">${baseDomain.domainName}</form:option>
                    </c:forEach>
                </form:select>
                <span><form:errors path="baseDomain" class="help-inline"/></span>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="selectSubDomain"><spring:message code="domain.ontology.label.domainontology.subdomain"/><em> * </em></label>
            <div class="col-lg-4 col-sm-8 col-md-6 col-xs-12">
                <form:select id="subDomain" path="subDomain" class="form-control input-md">
                    <form:option value="select"><spring:message code="default.select"/></form:option>
                </form:select>
                <span><form:errors path="subDomain" class="help-inline"/></span>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="selectOwlFile"><spring:message code="domain.ontology.label.domainontology.owl"/><em> * </em></label>
            <div class="col-lg-4 col-sm-8 col-md-6 col-xs-12">
                <form:select id="owlFile" path="owlFile" class="form-control input-md">
                    <form:option value="select"><spring:message code="default.select"/></form:option>
                </form:select>
                <span><form:errors path="owlFile" class="help-inline"/></span>
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
                <input type="submit" id="button" class="btn btn-default" value='<spring:message code="domain.ontology.label.domainontology.submit"/>' />
            </div>
        </div>
    </form:form>
</div>