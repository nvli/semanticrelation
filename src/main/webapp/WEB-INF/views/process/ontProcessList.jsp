<%--
    Document   : showDomainOntProcessList
    Created on : Apr 21, 2017, 10:44:14 AM
    Author     : Suhas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<c:set var="context" value="${pageContext.request.contextPath}" />

<script>

    $(document).ready(function () {
        for (var i = 0; i < ${totalpages}; i++)
        {
            $("ul").append("<li><a href='${context}/ont/process/list/" + (i + 1) + "/${pageWin}'>" + (i + 1) + "</a></li>");
        }
    });

</script>
<style>
    tr {display: block; }
    th, td { width: 373px; }
    tbody { display: block; overflow: auto;}
</style>
<div class="container">
    <span class="pull-left"><spring:message code="domain.ontology.process.list"/></span>
    <span class="pull-right"><a href="${context}/ont/processForm"><spring:message code="domain.ontology.process"/></a></span>
    <div class="clearfix"></div>
    <table id="example" class="table table-striped table-bordered" cellspacing="0" width="80%" height="70%">
        <tr>
            <th>Sr. No.</th>
            <th>Resource Name</th>
            <th>Resource Type</th>
            <th>Resource Code</th>
            <th>OWL File</th>
            <th>Created On</th>
            <th>Status</th>
        </tr>
        <c:forEach items="${listResourceProcess}" var="result" varStatus="loop">
            <tr>
                <td>${loop.index+1}</td>
                <td>${result.resourceName}</td>
                <td>${result.resourceType}</td>
                <td>${result.resourceCode}</td>
                <td>${result.referenceOnt.fileName}</td>
                <td>${result.createdOn}</td>
                <td>${result.status}</td>
            </tr>
        </c:forEach>
    </table>

    <ul class="pagination" >

    </ul>
</div>
