<%--
    Document   : owl
    Created on : Mar 22, 2017, 10:22:42 AM
    Author     : Hemant Anjana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="context" value="${pageContext.request.contextPath}" />
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="${context}/themes/webowl/css/webvowl.css" />
        <link rel="stylesheet" type="text/css" href="${context}/themes/webowl/css/webvowl.app.css" />
        <title>WebVOWL</title>
    </head>
    <body>
        <script type="text/javascript" src="${context}/themes/scripts/jquery.min.js"></script>
        <script type="text/javascript" src="${context}/themes/scripts/underscore-min.js"></script>
        <style>
            .intro {
                /*white-space: nowrap;*/ 
                width: 25em; 
                overflow: hidden;
                text-overflow: ellipsis;
                display:inline-block;
                font-size: 1.17em;
            }
            .btm_border{
                /*                border-bottom: dotted #e7eaec;
                                border-width: 1px;*/
                /*                padding-top: 2px;*/
                padding-bottom: 10px;
            }
        </style>
        <script>
            //please set this variable ..its is used in js of webvowl
            var myJSONDATAURL = "${context}/themes/webowl/data/";
            $(document).ready(function () {

                $('body').on('click', 'circle', function () {
                    $('#recordCountPId').text("");
                    $('#recordPreviewPId').text("");
                    var term = $(this).children("title").text();
                    var ontId = $(".selected-ontology").children("a").attr("id");
                    console.log(term);
                    fetchCount(term, ontId);
                    //console.log("clicked " + $(this).children("title").text());
                    fetchRecords(term, ontId);
                });
                function fetchRecords(term, ontId) {
                    console.log(">>>>>>>>>>>>>>ontId " + ontId);
                    $.ajax({
                        type: 'GET',
                        data: {q: term, refOntId: ontId},
                        url: "${context}/visualize/getjsonforjs/1/25",
                        success: function (jsonObj) {
                            _.each(jsonObj, function (key, value) {
                                var valuea = "<a href='http://test1.nvli.in/nvliPortal/search/preview/" + value + "' target='_blank' class='intro btm_border' title='" + key + "'>" + key + "</a>";
                                $('#recordPreviewPId').append(valuea);
                            });
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }
                function fetchCount(term, ontId) {

                    $.ajax({
                        type: 'GET',
                        data: {q: term, refOntId: ontId},
                        url: "${context}/visualize/getjsonforjsCount",
                        success: function (response) {
                            $('#recordCountPId').text(response);
                        },
                        error: function (err) {
                            console.log(err);
                        }
                    });
                }
            });

        </script>
        <main>
            <section id="canvasArea">
                <div id="loading-info">
                    <div id="loading-error" class="hidden">
                        <div id="error-info"></div>
                        <div id="error-description-button" class="hidden">Show error details</div>
                        <div id="error-description-container" class="hidden">
                            <pre id="error-description"></pre>
                        </div>
                    </div>
                    <div id="loading-progress" class="hidden">
                        <span>Loading ontology... </span>
                        <div class="spin">&#8635;</div><br>
                        <div id="myProgress">
                            <div id="myBar"></div>
                        </div>
                    </div>
                </div>
                <div id="graph" ></div>
            </section>
            <aside id="detailsArea">
                <section id="generalDetails">
                    <!--<div id="ontology-metadata" class="accordion-container"></div>-->
                    <h3 class="accordion-trigger">Statistics</h3>
                    <div class="accordion-container">
                        <p class="statisticDetails">Classes: <span id="classCount"></span></p>
                        <p class="statisticDetails">Object prop.: <span id="objectPropertyCount"></span></p>
                        <p class="statisticDetails">Datatype prop.: <span id="datatypePropertyCount"></span></p>
                        <div class="small-whitespace-separator"></div>
                        <p class="statisticDetails">Individuals: <span id="individualCount"></span></p>
                        <div class="small-whitespace-separator"></div>
                        <p class="statisticDetails">Nodes: <span id="nodeCount"></span></p>
                        <p class="statisticDetails">Edges: <span id="edgeCount"></span></p>
                    </div>
                    <h3 class="accordion-trigger" id="selection-details-trigger">Selection Details</h3>
                    <div class="accordion-container" id="selection-details">
                        <div id="classSelectionInformation" class="hidden">
                            <p class="propDetails">Name: <span id="name"></span></p>
                            <p class="propDetails">Type: <span id="typeNode"></span></p>
                            <p class="propDetails">Equiv.: <span id="classEquivUri"></span></p>
                            <p class="propDetails">Disjoint: <span id="disjointNodes"></span></p>
                            <p class="propDetails">Charac.: <span id="classAttributes"></span></p>
                            <p class="propDetails">Description: <span id="nodeDescription"></span></p>
                            <p class="propDetails">Comment: <span id="nodeComment"></span></p>
                            <p class="propDetails">Individuals: <span id="individuals"></span></p>
                        </div>
                        <%--<!--                        <div id="propertySelectionInformation" class="hidden">
                                                    <p class="propDetails">Name: <span id="propname"></span></p>
                                                    <p class="propDetails">Type: <span id="typeProp"></span></p>
                                                    <p id="inverse" class="propDetails">Inverse: <span></span></p>
                                                    <p class="propDetails">Domain: <span id="domain"></span></p>
                                                    <p class="propDetails">Range: <span id="range"></span></p>
                                                    <p class="propDetails">Subprop.: <span id="subproperties"></span></p>
                                                    <p class="propDetails">Superprop.: <span id="superproperties"></span></p>
                                                    <p class="propDetails">Equiv.: <span id="propEquivUri"></span></p>
                                                    <p id="infoCardinality" class="propDetails">Cardinality: <span></span></p>
                                                    <p id="minCardinality" class="propDetails">Min. cardinality: <span></span></p>
                                                    <p id="maxCardinality" class="propDetails">Max. cardinality: <span></span></p>
                                                    <p class="propDetails">Charac.: <span id="propAttributes"></span></p>
                                                    <p class="propDetails">Description: <span id="propDescription"></span></p>
                                                    <p class="propDetails">Comment: <span id="propComment"></span></p>
                                                </div>-->--%>
                        <div id="noSelectionInformation">
                            <p><span>Select an element in the visualization.</span></p>
                        </div>

                        <div id="recordPreviewDivId" >
                            <p class="propDetails">Total Count: <span id="recordCountPId"></span></p>
                            <p class="">Records: <br/><span id="recordPreviewPId"></span></p>
                        </div>
                    </div>
                </section>
            </aside>


            <nav id="optionsArea">
                <ul id="optionsMenu">
                    <li id="pauseOption"><a id="pause-button" href="#">Pause</a></li>
                    <li id="resetOption"><a id="reset-button" href="#" type="reset">Reset</a></li>
                    <li id="moduleOption"><a href="#">Modes</a>
                        <ul class="toolTipMenu module">
                            <li class="toggleOption" id="pickAndPinOption"></li>
                            <li class="toggleOption" id="nodeScalingOption"></li>
                            <li class="toggleOption" id="compactNotationOption"></li>
                            <li class="toggleOption" id="colorExternalsOption"></li>
                        </ul>
                    </li>
                    <li id="filterOption"><a href="#">Filter</a>
                        <ul class="toolTipMenu filter">
                            <li class="toggleOption" id="datatypeFilteringOption"></li>
                            <li class="toggleOption" id="objectPropertyFilteringOption"></li>
                            <li class="toggleOption" id="subclassFilteringOption"></li>
                            <li class="toggleOption" id="disjointFilteringOption"></li>
                            <li class="toggleOption" id="setOperatorFilteringOption"></li>
                            <li class="slideOption" id="nodeDegreeFilteringOption"></li>
                        </ul>
                    </li>
                    <li id="gravityOption"><a href="#">Gravity</a>
                        <ul class="toolTipMenu gravity">
                            <li class="slideOption" id="classSliderOption"></li>
                            <li class="slideOption" id="datatypeSliderOption"></li>
                        </ul>
                    </li>
                    <%--<!--                <li id="export"><a href="#">Export</a> 
                                        <ul class="toolTipMenu export">
                                            <li><a href="#" download id="exportJson">Export as JSON</a></li>
                                            <li><a href="#" download id="exportSvg">Export as SVG</a></li>
                                        </ul>
                                    </li>-->--%>
                    <li id="select" style="display: block;" class="${selectedOnt}"><a href="#">Ontology</a>
                        <ul class="toolTipMenu select " id="uid">
                            <c:forEach items="${domainOnts}" var="domainOnt">
                                <li id="${domainOnt.id}">
                                    <c:set var = "xmpFile" value = "${domainOnt.fileName}"/>
                                    <c:set var = "ontName" value = "${domainOnt.displayName}"/>
                                    <a href="#${fn:replace(xmpFile,'.xml','.json')}" id="${domainOnt.id}">${fn:toUpperCase(ontName)}</a>
                                </li>
                            </c:forEach>
                        </ul>
                    </li>
                    <li class="searchMenu" id="searchMenuId">
                        <input class="searchInputText" type="text" id="search-input-text" placeholder="Search">
                        <ul class="searchMenuEntry" id="searchEntryContainer">
                        </ul>
                    </li>
                    <li id="li_right" style="float:left">
                        <a href="#" id="RightButton"></a>
                    </li>
                    <li id="li_left" style="float:left">
                        <a href="#" id="LeftButton"></a>
                    </li>

                    </li>
                </ul>
            </nav>
        </main>
        <script src="${context}/themes/webowl/js/d3.min.js"></script>
        <script src="${context}/themes/webowl/js/webvowl.js"></script>
        <script src="${context}/themes/webowl/js/webvowl.app.js"></script>

        <script>
            /*$.ajax({
             type: 'GET',
             url: "${context}/visualize/getjsonforDomainOnts",
             async: false,
             success: function (jsonObjData) {
             _.each(jsonObjData, function (key) {
             var varl = "<li><a href='#"+key['fileName'].replace("xml", "json")+"' id='" + key['namespaceURI'] + "#'>"+key['displayName']+"</a></li>"
             $('#uid').append(varl);
             });
             },
             error: function (err) {
             console.log(err);
             }
             });*/
            window.onload = webvowl.app().initialize;
        </script>
    </body>
</html>
