<%-- 
    Document   : showDomainForm
    Created on : Apr 5, 2017, 4:57:51 PM
    Author     : Hemant Anjana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="container">
    <span class="pull-left"><spring:message code="domain.creation"/></span>
    <div class="clearfix"></div> 
    <form:form id="registerform" name="registerform" action="${context}/domain/create" commandName="domainForm"  method="post"  class="form-horizontal">
        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="pwd"><spring:message code="domain.label.name"/><em> * </em></label>
            <div class="col-lg-3 col-sm-2 col-md-2 col-xs-2">          
                <form:input path='domainName' class="form-control" placeholder="Enter domain name"/>
                <span class="help-inline"><form:errors path="domainName" /></span>
            </div>
        </div>
        <div class="clearfix"></div>    

        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="pwd"><spring:message code="domain.ontology.label.description"/><em> * </em></label>
            <div class="col-lg-3 col-sm-2 col-md-2 col-xs-2">          
                <form:textarea path='description' class="form-control" placeholder="Enter description name"/>
                <span class="help-inline"><form:errors path="description" /></span>
            </div>
        </div>
        <div class="clearfix"></div>    

        <div class="form-group">
            <label class="col-lg-2 control-label col-sm-2 col-md-2 col-xs-2" for="pwd"><spring:message code="domain.label.parentDomain"/><em> * </em> </label>
            <div class="col-lg-3 col-sm-2 col-md-2 col-xs-2">
                <form:select path="parentDomain" class="form-control input-md">
                    <form:option value="select">--- Select ---</form:option>
                    <c:forEach items="${domainList}" var="domain">     
                        <form:option value="${domain.domainName}">${domain.domainName}</form:option>
                    </c:forEach>
                </form:select>
            </div>
            <span class="help-inline"><form:errors path="parentDomain" /></span>
        </div>
        <div class="clearfix"></div>    

        <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-default"  value='<spring:message code="domain.label.submit"/>' />
            </div>
        </div>
    </form:form>
</div>
