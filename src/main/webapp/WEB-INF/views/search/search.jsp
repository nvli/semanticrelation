<%--
    Document   : search
    Created on : Apr 12, 2017, 10:11:00 AM
    Author     : Suhas
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div>
    <form:form id="searchform" name="searchform" action="${context}/domain/create" commandName="searchform"  method="post"  class="form-horizontal">
        <div class="form-group">
            <div class="col-lg-3 col-sm-2 col-md-2 col-xs-2">
                <form:input path='searchStr' class="form-control" placeholder="Enter search key"/>
                <span class="help-inline"><form:errors path="searchStr" /></span>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-default"  value='<spring:message code="domain.label.search"/>' />
            </div>
        </div>
        <div class="clearfix"></div>

    </form:form>
</div>