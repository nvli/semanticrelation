<%--
    Document   : index
    Created on : Dec 22, 2016, 3:59:36 PM
    Author     : Hemant Anjana
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<c:set var="context" value="${pageContext.request.contextPath}" />

<div class="container clearfix">
    <div class="clearfix">
        <a href="${context}/domain/show"><spring:message code="domain.creation.anch"/></a><br/>
        <a href="${context}/domain-ontology/show"><spring:message code="domain.ontology.creation.anch"/></a><br/>
        <a href="${context}/domain/list/1/${properties['default.pagewindow']}"><spring:message code="domain.list"/></a><br/>
        <a href="${context}/domain-ontology/list/1/${properties['default.pagewindow']}"><spring:message code="domain.ontology.list"/></a><br/>
        <a href="${context}/ont/processForm"><spring:message code="domain.ontology.process"/></a><br/>
        <a href="${context}/ont/process/list/1/${properties['default.pagewindow']}"><spring:message code="domain.ontology.process.list"/></a><br/>
        <a href="${context}/visualize/owl/4">OWL Visualization</a><br/>
    </div>
</div>
