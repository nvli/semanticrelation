<%--
    Document   : default-home-layout
    Created on : April 19, 2017, 9:49:16 AM
    Author     : Hemant Anjana
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:set var="context" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><tiles:insertAttribute name="title"/></title>
        <link rel="stylesheet" href="${context}/themes/css/bootstrap.min.css"/>
        <script src="${context}/themes/scripts/jquery.min.js"></script>
        <script src="${context}/themes/scripts/bootstrap.min.js"></script>
    </head>
    <body class="home_bg">
        <div class="container-fluid">
            <tiles:insertAttribute name="header" ignore="true"/>
            <tiles:insertAttribute name="body" ignore="true"/>
        </div>
        <tiles:insertAttribute name="footer"  ignore="true"/>
    </body>
</html>

