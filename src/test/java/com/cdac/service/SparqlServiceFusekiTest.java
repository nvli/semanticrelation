/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cdac.service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author Gajraj
 */
@Ignore
public class SparqlServiceFusekiTest {

    String serviceURI;
    String className;
    int offset;
    int limit;
    String refURI;

    public SparqlServiceFusekiTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        serviceURI = "http://10.208.26.13:3030/NVLI_FINAL";
        className = "energyDistribution";
        offset = 0;
        limit = 25;
        refURI = "http://www.astro.umd.edu/~eshaya/astro-onto/owl/physics.owl#";
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of uploadRdf method, of class SparqlServiceFuseki.
     */
    @Test
    @Ignore
    public void testUploadRdf_3args() throws Exception {
        System.out.println("uploadRdf");
        String serviceURI = "";
        File rdfFile = null;
        String base = "";
        SparqlServiceFuseki instance = new SparqlServiceFuseki();
        instance.uploadRdf(serviceURI, rdfFile, base);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of uploadRdf method, of class SparqlServiceFuseki.
     */
    @Test
    @Ignore
    public void testUploadRdf_4args() throws Exception {
        System.out.println("uploadRdf");
        String serviceURI = "";
        File rdfFile = null;
        String base = "";
        String graphname = "";
        SparqlServiceFuseki instance = new SparqlServiceFuseki();
        instance.uploadRdf(serviceURI, rdfFile, base, graphname);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of getRecordCountForClass method, of class SparqlServiceFuseki.
     */
    @Ignore
    @Test
    public void testGetRecordCountForClass() {
        System.out.println("getRecordCountForClass");
        //  String serviceURI = "";
        //String className = "";
        SparqlServiceFuseki instance = new SparqlServiceFuseki();
        int expResult = 2116;
        int result = instance.getRecordCountForClass(serviceURI, className, refURI, "#");
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of getRecordForClass method, of class SparqlServiceFuseki.
     */
    @Ignore
    @Test
    public void testGetRecordForClass() {
        System.out.println("getRecordForClass");
        String serviceURI = "";
        String className = "";
        int offset = 0;
        int limit = 0;
        SparqlServiceFuseki instance = new SparqlServiceFuseki();
        Map<String, String> expResult = null;
        Map<String, String> result = instance.getRecordForClass(serviceURI, className, offset, limit, refURI, "#");
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of getAuthorsForRecord method, of class SparqlServiceFuseki.
     */
    @Ignore
    @Test
    public void testGetAuthorsForRecord() {
        System.out.println("getAuthorsForRecord");
        // String serviceURI = "";
        String recordIdentifier = "OPRE_OPRE106_283694";
        SparqlServiceFuseki instance = new SparqlServiceFuseki();
        List<String> expResult = new ArrayList<>();
        expResult.add("Ghate Rucha");
        expResult.add("Nagendra Harini");
        expResult.add("Pareeth Sajid");
//        List<String> result = instance.getAuthorsForRecord(serviceURI, recordIdentifier);
        // assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of getCoAuthors method, of class SparqlServiceFuseki.
     */
    @Ignore
    @Test
    public void testGetCoAuthors() {
        System.out.println("getCoAuthors");
        // String serviceURI = "";
        String author = "Ghate Rucha";
        int offset = 0;
        int winSize = 25;
        SparqlServiceFuseki instance = new SparqlServiceFuseki();

        Map<String, HashSet<String>> expResult = new HashMap<>();
        HashSet<String> set1 = new HashSet<>();
        set1.addAll(Arrays.asList("Nagendra Harini", "Ghate Rucha", "Rocchini Duccio"));
        HashSet<String> set2 = new HashSet<>();
        set2.addAll(Arrays.asList("Pareeth Sajid", "Nagendra Harini", "Ghate Rucha"));
        expResult.put("OPRE_OPRE106_283694", set2);
        expResult.put("OPRE_OPRE106_283692", set1);

        //  Map<String, HashSet<String>> result = instance.getCoAuthors(serviceURI, author, offset, winSize);
        //  assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

    /**
     * Test of main method, of class SparqlServiceFuseki.
     */
    @Ignore
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        SparqlServiceFuseki.main(args);
        // TODO review the generated test code and remove the default call to fail.
        // fail("The test case is a prototype.");
    }

}
