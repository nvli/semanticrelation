package com.cdac.utils;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;

import static org.testng.Assert.*;

public class OwlToD3JsonConversionTest {
    File owlFile;
    OwlToD3JsonConversion owlToD3JsonConversion;
    @BeforeMethod
    public void setUp() throws Exception {
        owlFile=new File("src/test/resources/physics.owl.xml");
        owlToD3JsonConversion=new OwlToD3JsonConversion(owlFile);
    }

    @Test
    public void testConvert() throws Exception {
        System.out.println(owlToD3JsonConversion.convert("Things"));
    }

    @Test
    public void testConvert1() throws Exception {

        owlToD3JsonConversion.convert("thing",new File("src/test/resources/physics_D3_J.json"));
    }

}