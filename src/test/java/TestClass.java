/* IMPORTANT: Multiple classes and nested static classes are supported */

 /*
 * uncomment this if you want to read input.
//imports for BufferedReader
import java.io.BufferedReader;
import java.io.InputStreamReader;
 */
//import for Scanner and other utility classes
import static java.lang.Math.sqrt;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

class TestClass {

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int T = s.nextInt();
        long sum = 0;
        int t;
        Stack<Integer> stack = new Stack<>();

        while (T > 0) {
            t = s.nextInt();
            if (t != 0) {
                stack.push(t);
            } else {
                if (!stack.isEmpty()) {
                    stack.pop();
                }
            }
            T--;
        }
        for (Integer integer : stack) {
            sum += integer;
        }
        System.out.println(sum);
    }

    public static void main2(String[] args) {

        Scanner s = new Scanner(System.in);
        int T = s.nextInt();
        s.nextLine();
        while (T > 0) {

            long N = s.nextLong();
            long x = N - 1;
            long maxValue1 = Long.highestOneBit(x);
            // System.out.println("***" + maxValue1);

            long maxValue = (maxValue1 << 1) - 1;

            // System.out.println("***" + maxValue);
            long count = (x - maxValue1 + 1) * 2;
            if (N == 1) {
                System.out.println(0 + " " + 1);
            } else {
                System.out.println(maxValue + " " + count);
            }
            T--;
        }
    }

    public static void main1(String args[]) throws Exception {
        //  XorCombinations(8);
        getXORterms(8);
        getXORterms(2);
        getXORterms(3);
        getXORterms(4);

//Scanner
        Scanner s = new Scanner(System.in);
        long l1 = s.nextLong();
        long l2 = s.nextLong();
        long gcd = gcdStein(l1, l2);
        //  System.out.println(gcd);
        long[] pf = primeFactors(gcd);
        if (pf.length == 0 && gcd > 1) {
            System.out.println(2);
        } else if (pf.length == 0 && gcd == 1) {
            System.out.println(1);
        } else {
            List<Long> list = LongStream.of(pf).boxed().collect(Collectors.toList());
            long sum = 1;
            for (Long long1 : list) {
                sum *= (long1 + 1);
            }
            System.out.println(sum);
        }

    }

    public static long gcd(long a, long b) {
        if (b == 0 || a == b) {
            return a;
        }
        if (a > b) {
            return gcd(a - b, b);
        } else {
            return gcd(a, b - a);
        }

    }

    public static long gcdStein(long p, long q) {
        if (q == 0) {
            return p;
        }
        if (p == 0) {
            return q;
        }

        // p and q even
        if ((p & 1) == 0 && (q & 1) == 0) {
            return gcdStein(p >> 1, q >> 1) << 1;
        } // p is even, q is odd
        else if ((p & 1) == 0) {
            return gcdStein(p >> 1, q);
        } // p is odd, q is even
        else if ((q & 1) == 0) {
            return gcdStein(p, q >> 1);
        } // p and q odd, p >= q
        else if (p >= q) {
            return gcdStein((p - q) >> 1, q);
        } // p and q odd, p < q
        else {
            return gcdStein(p, (q - p) >> 1);
        }
    }

    public static long gcditerative(long a, long b) {
        while (a != b && b != 0) {
            if (a > b) {
                a = a - b;
            } else {
                b = b - a;
            }
        }
        return a;
    }

    public static long gcdBinary(long a, long b) {
        long min = Math.max(a, b);
        long lo = 1;
        long hi = min;
        long mid = (lo + hi) / 2;
        while (a % mid != 0 && b % mid != 0) {
            long t = mid;
            hi = mid;

        }
        return mid;
    }

    public static long[] primeFactors(long n) {
        List<Long> list = new ArrayList<>();
        long count = 0;

        while (n % 2 == 0) {
            count++;
            n /= 2;
        }
        if (count > 0) {
            list.add(count);
        }
        count = 0;
        long sqrt = (long) sqrt(n);

        for (long i = 3; i <= sqrt; i += 2) {

            count = 0;
            while (n % i == 0) {
                count++;
                n /= i;
            }
            if (count > 0) {
                list.add(count);
            }
        }

        return list.stream().mapToLong(i -> i).toArray();
    }

    public static long getCombinatinsOfPF(long set[]) {
        long n = set.length;
        long sum = 0;
        // Run a loop for prlonging all 2^n
        // subsets one by obe
        for (long i = 0; i < (1 << n); i++) {
            // Prlong current subset
            long t = 1;
            for (int j = 0; j < n; j++) // (1<<j) is a number with jth bit 1
            // so when we 'and' them with the
            // subset number we get which numbers
            // are present in the subset and which
            // are not
            {
                if ((i & (1 << j)) > 0) {
                    t *= set[j];
                }
            }
            sum += t;
        }
        return sum;
    }

    public static List<List<Long>> powerSet(List<Long> originalSet) {

        List<Long> list = new ArrayList<>(originalSet);
        int n = list.size();

        List<List<Long>> powerSet = new ArrayList<>();

        for (long i = 0; i < (1 << n); i++) {
            List<Long> element = new ArrayList<>();
            for (int j = 0; j < n; j++) {
                if ((i >> j) % 2 == 1) {
                    element.add(list.get(j));
                }
            }
            powerSet.add(element);
        }

        return powerSet;
    }

    public static long getXORterms(long x) {

        Long maxValue = Long.highestOneBit(x);
        if (maxValue == x) {
            maxValue -= 1;
        }
        long y = x / 2 + 1;
        int count = 0;
        while (y <= x) {
            count++;
            y++;
        }
        System.out.println(maxValue + " " + count);
        return 0;
    }

    public static long XorCombinations(long x) {
        long y = x / 2 + 1;
        int count = 0;
        while (y <= x) {
            count++;
            y++;
        }
        return count;
    }

    public static long countOfsortedSubString(String s) {
        int start = 0;
        long sum = 0;
        int count = 1;
        while (start < s.length()) {
            if (start < s.length() - 1 && s.charAt(start + 1) >= s.charAt(start)) {
                start++;
                count++;
            } else {
                sum += ((count * (count + 1)) / 2);
                count = 1;
                start = start + 1;
            }
        }
        return sum;
    }
}
