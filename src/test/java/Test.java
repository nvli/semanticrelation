
import com.cdac.processor.ProcessMetadataInOWL;
import com.cdac.processor.WordWeightProcessor;
import com.cdac.utils.OntologyReader;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import weka.core.Stopwords;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Hemant Anjana
 */
public class Test {

    public static void main(String[] args) throws IOException, OWLOntologyCreationException, OWLOntologyStorageException {
//        main1(args);
//        main2(args);

        String pathname = "C:\\Users\\Suhas\\Desktop\\onto\\chemistry.owl.xml";
        File owlFile = new File(pathname);
        System.out.println(owlFile.exists());
        OntologyReader or = new OntologyReader(owlFile);
        or.generatelables();
    }
    
    public static void main1(String[] args) throws IOException, OWLOntologyCreationException {
        Stopwords stopwords = new Stopwords();
        String pathname = "E:\\ontology\\physics.rdf.xml";
        File owlFile = new File(pathname);
        OntologyReader or = new OntologyReader(owlFile);
        // WordWeightProcessor wwp = new WordWeightProcessor(or, 0.1f);
        WordWeightProcessor wwp = new WordWeightProcessor(null, 0, 0);

        //  wwp.buildDomainWordWeightMap();
        //  wwp.saveWeightedWord(new File("E:\\ww.json"));
        File jsonFile = new File("E:\\ww.json");
        wwp.loadFromJson(jsonFile);
        Map<String, Long> wwmap = wwp.getDomainWordWeight().getwordWeightMap();
        System.out.println(wwmap.containsKey("conservation"));

    }

    public static void main2(String[] args) throws OWLOntologyCreationException, IOException, OWLOntologyStorageException {

        // File csvFile = new File("E:\\Projects\\Office\\mit_dc_metadata_information.csv");
        File csvFile = new File("E:\\ontology\\data\\OR116.csv");
        // processInputDCfile(file);


        /*For physics Ontology*/
        File targetOntFilePhysics = new File("E:\\ontology\\NVLI_Final 1.0 - Copy.owl");
        File refOntFilePhysics = new File("E:\\ontology\\physics.rdf.xml");

        ProcessMetadataInOWL processMetadataInOWLPhy = new ProcessMetadataInOWL(targetOntFilePhysics, refOntFilePhysics);
        processMetadataInOWLPhy.processRecordsFromCSVWithWeight(csvFile, "OR116", new File("json"));
    }
}
