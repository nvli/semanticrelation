
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import org.apache.jena.query.DatasetAccessor;
import org.apache.jena.query.DatasetAccessorFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Suhas
 */
public class NewClass {

    public static void main(String[] args) throws FileNotFoundException {
        String s = "Invariant Gaussian ensembles of Hermitian matrices: eigenvalues-distribution of 2  2 Wigner-Dyson ensembles";
        System.out.println("--" + s);
        uploadRdf("http://localhost:3030/MY_NVLI", new File("C:\\Users\\Suhas\\.nvli_semantic_relations\\temp\\354_NVLI_Final 1.0.owl"), "http://www.cdac.in/technologies/ontology/nvli#");
        //s=stripNonValidXMLCharacters(s);
        System.out.println("--" + s);
    }

    public static void uploadRdf(final String serviceURI, File rdfFile, final String base) throws FileNotFoundException {
        DatasetAccessor accessor = DatasetAccessorFactory.createHTTP(serviceURI);
        InputStream in = new FileInputStream(rdfFile);
        Model model = ModelFactory.createDefaultModel();
        model.read(in, base, "RDF/XML");
        accessor.add(model);
    }

    public static String stripNonValidXMLCharacters(String in) {
        StringBuffer out = new StringBuffer(); // Used to hold the output.
        char current; // Used to reference the current character.

        if (in == null || ("".equals(in))) {
            return ""; // vacancy test.
        }
        for (int i = 0; i < in.length(); i++) {
            current = in.charAt(i); // NOTE: No IndexOutOfBoundsException caught here; it should not happen.
            if ((current == 0x9)
                    || (current == 0xA)
                    || (current == 0xD)
                    || ((current >= 0x20) && (current <= 0xD7FF))
                    || ((current >= 0xE000) && (current <= 0xFFFD))
                    || ((current >= 0x10000) && (current <= 0x10FFFF))) {
                out.append(current);
            }
        }
        return out.toString();
    }
}
