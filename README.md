# Readme

* When You Clone the project, you will get entire code base of project.
Along with it, you will also find property file.
Name of property file is 'admin-settings.properties'.

* To Run the project, you need to create directory(folder) and name it as '.nvli_semantic_relations/conf' in 'user.home'.
'user.home' is user directory. 

 i.e your user directory should look a like:- 

    'user.home'
        '.nvli_semantic_relations'
                'conf'
                    'admin-settings.properties' (this is file which will be copied by you from code base)

# eg: C:\Users\admin\.nvli_semantic_relations\conf\admin-settings.properties


* Once you done with above settings, we can change any configuration in 'admin-settings.properties'.
* By default, we uses Mysql database, and database name is 'ontologydb'. And change password accordingly.

